#ifndef _COORDINATOR_H
#define _COORDINATOR_H

#include <functional>
#include <memory>
#include <unordered_map>
#include <vector>
#include "card.h"
#include "cardpack.h"
#include "play_structures.h"

class Engine;
typedef std::pair<Player*, Card*> playercard;
typedef std::list<playercard> playercards;

class Coordinator
{
public:
	Coordinator() :_engine(nullptr) { }
	virtual Coordinator& operator=(const Coordinator&) = delete;

	// setup
	uint8_t retrieve_players(std::vector<std::shared_ptr<Player>>& ps, Playfield *f) {
		uint8_t nump = _retrieve_players(ps, f);
		std::vector<Player*>& unconst_ps = const_cast<std::vector<Player*>&>(_ps);
		uint8_t i = 0;
		for (auto& p : ps) {
			p->index(i++);
			unconst_ps.push_back(p.get());
		}
		return nump;
	}

	// informational
	virtual void turn_start(const Player *curp) = 0;

	virtual void new_hands(const std::list<Player*>& players) = 0;
	virtual void drew_card(const Player *p, const Card *c) = 0;
	virtual void card_moved(const Player *from, const Player *to, const Card *cfrom, const Card *cto, Cardloc where) = 0;
	virtual void card_played(const Player *from, const Player *to, const Card *c) = 0;
	virtual void card_revealed(const Card *c, Cardloc where) = 0;
	virtual void card_discarded(const Event& ev) = 0;
	virtual void mundane_discard(const Card *c) = 0;

	virtual void attempt(const Event& ev) = 0;

	virtual void player_hit(const Player *p, int16_t hit) = 0;
	virtual void player_healed(const Player *p, int16_t hit) = 0;
	virtual void player_dead(const Player *p) = 0;

	template<typename T> class future;

	// (non-blocking) requests
	virtual future<Card*> select_card(const Player *p, const cardpack& cards, bool passable = false);
	virtual future<cardpack> select_multi_cards(const Player *p, const cardpack& ccards);
	virtual future<Card*> select_counter(const Player *p, const cardpack& cards);
	virtual future<Player*> select_player(const Player *p, const std::list<Player*>& players);
	virtual future<ATYPE> select_atype(const Player *p);
	virtual future<Card*> five_second_timer(const std::list<Player*>& players, const playercards& additional_cards);

protected:
	class query;
	virtual void select_card(const query& q) = 0;
	virtual void select_player(const query& q) = 0;
	virtual void select_multi_cards(const query& q) = 0;
	virtual void select_atype(const query& q) = 0;
	virtual void timer(const query& q) = 0;
public:

	// teardown
	virtual void game_end() = 0;

	// wait loop
	virtual void wait(uint32_t id) = 0;

	virtual void attach_engine(Engine *engine) { _engine = engine; }

protected:
	virtual uint8_t _retrieve_players(std::vector<std::shared_ptr<Player>>& ps, Playfield *f) = 0;

	Engine *_engine;

	template<typename T>
	class future_state {
	public:
		uint32_t id = 0;
		std::shared_ptr<T> val;
		bool ready = false;
	};

	template<>
	class future_state<Card*> {
	public:
		uint32_t id = 0;
		std::shared_ptr<Card*> val;
		bool ready = false;
		std::function<Event(Card*)> now_event;
		std::shared_ptr<future_state<Event>> fevstate;
		std::function<Evlist(Card*)> now_evlist;
		std::shared_ptr<future_state<Evlist>> fevlstate;
	};

public:
	template<typename T>
	class future {
	public:
		future() { }
		future(Coordinator *parent, std::shared_ptr<future_state<T>> fstate) {
			get = [=]() {
				if (!fstate->ready) {
					parent->wait(fstate->id);
				}
				assertTrue(fstate->ready);
				return *fstate->val;
			};
			this->fstate = fstate;
		}

		std::function<T()> get;
		std::shared_ptr<future_state<T>> fstate;

		template<typename F>
		auto then(F fn) -> future<decltype(fn(get()))> {
			future<decltype(fn(get()))> ufn;
			auto pget = this->get; // needed so that we don't capture through this->get, since
			ufn.get = [=]() {      // "this" goes out of scope and gets destructed
				return fn(pget());
			};
			return ufn;
		}
	};

	template<>
	class future<Card*> {
	public:
		typedef Card* T;
		future() { }
		future(Coordinator *parent, std::shared_ptr<future_state<T>> fstate) {
			get = [=]() {
				if (!fstate->ready) {
					parent->wait(fstate->id);
				}
				assertTrue(fstate->ready);
				return *fstate->val;
			};
			this->fstate = fstate;
		}

		std::function<T()> get;
		std::shared_ptr<future_state<T>> fstate;

		template<typename F>
		auto then(F fn) -> future<decltype(fn(get()))> {
			future<decltype(fn(get()))> ufn;
			auto pget = this->get; // needed so that we don't capture through this->get, since
			ufn.get = [=]() {      // "this" goes out of scope and gets destructed
				return fn(pget());
			};
			return ufn;
		}

		future<Event> now_ev(std::function<Event(Card*)> fn) {
			future<Event> fev;
			std::shared_ptr<future_state<Event>> fevstate = std::make_shared<future_state<Event>>();
			fev.fstate = fevstate;
			this->fstate->now_event = fn;
			this->fstate->fevstate = fevstate;
			auto pget = this->get; // see above;
			fev.get = [=]() {
				pget();
				assertTrue(fevstate->ready);
				return *fevstate->val;
			};
			return fev;
		}

		future<Evlist> now_evl(std::function<Evlist(Card*)> fn) {
			future<Evlist> fevlist;
			std::shared_ptr<future_state<Evlist>> fevlstate = std::make_shared<future_state<Evlist>>();
			fevlist.fstate = fevlstate;
			this->fstate->now_evlist = fn;
			this->fstate->fevlstate = fevlstate;
			auto pget = this->get; // see above;
			fevlist.get = [=]() {
				pget();
				assertTrue(fevlstate->ready);
				return *fevlstate->val;
			};
			return fevlist;
		}
	};

	template<typename T>
	future<T> wrap(T&& val) {
		std::shared_ptr<future_state<T>> fstate(new future_state<T>);
		fstate->ready = true;
		fstate->val = std::make_shared<T>(std::move(val));
		return future<T>(this, fstate);
	}

protected:
	virtual uint32_t next_id() { return ++_next_id; }

	class query {
	public:
		enum {
			NONE,
			CARD,
			MULTI_CARDS,
			COUNTER,
			PLAYER,
			TIMER,
			ATCKTYPE,
		} type;
		std::shared_ptr<future_state<Card*>> fc;
		std::shared_ptr<future_state<cardpack>> fcc;
		std::shared_ptr<future_state<Player*>> fp;
		std::shared_ptr<future_state<ATYPE>> fa;
		const Player *p;
		cardpack cards;
		std::list<Player*> players;
		playercards additional_cards;
		bool passable;
	};

	void pop_query(uint32_t id) {
		auto iter = _queries.find(id);
		_queries.erase(iter);
	}

	query& get_query(uint32_t id) {
		auto iter = _queries.find(id);
		if (iter == _queries.end()) {
			static query q { query::NONE };
			return q;
		}
		query& q = iter->second;
		switch (q.type) {
		case query::CARD:
		case query::COUNTER:
		case query::TIMER:
			assertEqual(q.fc->id, id);
			break;
		case query::MULTI_CARDS:
			assertEqual(q.fcc->id, id);
			break;
		case query::PLAYER:
			assertEqual(q.fp->id, id);
			break;
		case query::ATCKTYPE:
			assertEqual(q.fa->id, id);
			break;
		}
		return q;
	}

	const std::vector<Player*> _ps;

private:
	std::unordered_map<uint32_t, query> _queries;

	uint32_t _next_id = 0;
};

/*class BasicCoordinator : public Coordinator
{
public:
	uint8_t _retrieve_players(std::vector<std::shared_ptr<Player>>& ps, Playfield *f) override;

	virtual void turn_start(const Player *curp) override;
	virtual void drew_card(const Player *p, const Card *c) override ;
	virtual void card_moved(const Player *from, const Player *to, const Card *cfrom, const Card *cto, Cardloc where) override;
	virtual void card_played(const Player *from, const Player *to, const Card *c) override;
	virtual void player_hit(const Player *p, int16_t hit) override;
	virtual void player_healed(const Player *p, int16_t hit) override;
	virtual void player_dead(const Player *p) override;

	virtual void game_end() override;

	virtual void wait(uint32_t id) override;

protected:
	uint16_t geti(size_t bigmax, bool passable);
	std::vector<uint16_t> get_multi(size_t bigmax, bool passable);
	Card* pass_priority(const query& q);
	Card* select_card(const query& q);
	cardpack select_multi_cards(const query& q);
	Player* select_player(const query& q);
	ATYPE select_atype(const query& q);
};*/

#endif // #ifndef _COORDINATOR_H

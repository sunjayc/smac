#include "smac_card_set.h"

#include <algorithm>
#include <functional>
#include <vector>

#include "asserts.h"
#include "card.h"
#include "engine.h"
#include "play_structures.h"

using std::bind;
using std::function;
using std::list;
using std::make_shared;
using std::placeholders::_1;
using std::placeholders::_2;
using std::placeholders::_3;
using std::placeholders::_4;
using std::vector;

// arguments for playfn_t and applyfn_t binds
#define _PFROM     std::placeholders::_1
#define _CARD      std::placeholders::_2
#define _PTARGET   std::placeholders::_3 // applyfn
#define _EV        std::placeholders::_3 // playfn
#define _ALSO_FLUX std::placeholders::_4 // playfn

#define PLAY_ATCK(hit) bind(&SmacCardSet::generic_attack, this, _1, _2, (hit))
#define PLAY_ATCK_WITH_FN(hit, applyfn, ...) [this](Player *p, Card *c, const Event& ev, cardpack& also_flux) { \
	(void)ev; \
	(void)also_flux; \
	Event e = generic_attack(p, c, hit); \
	e.apply_fn = bind(applyfn, this, __VA_ARGS__); \
	return e; \
}

#define PLAY_CNTR(hit) bind(&SmacCardSet::generic_counter, this, _1, _2, _3, (hit))
#define PLAY_HEAL(hit) bind(&SmacCardSet::generic_heal, this, _1, _2, (hit))

#define _PLAY_MOVE(loc, player, applyfn, is_free, ...) [this](Player *p, Card *c, const Event& ev, cardpack& also_flux) { \
	(void)ev; \
	(void)also_flux; \
	Event e(ETYPE::MOVE); \
	e.c = c; \
	e.from = p; \
	e.to = player; \
	e.m.where = loc; \
	e.m.cmove = c; \
	e.m.free = is_free; \
	e.apply_fn = bind(applyfn, this, __VA_ARGS__); \
	return e; \
}
#define PLAY_MOVE(loc, player, applyfn, ...) _PLAY_MOVE(loc, player, applyfn, false, __VA_ARGS__)
#define PLAY_FREE_MOVE(loc, player, applyfn, ...) _PLAY_MOVE(loc, player, applyfn, true, __VA_ARGS__)

#define PLAY_ITEM(applyfn, ...) PLAY_MOVE(Cardloc::ITEM, p, applyfn, __VA_ARGS__)
#define PLAY_FREE_ITEM(applyfn, ...) PLAY_FREE_MOVE(Cardloc::ITEM, p, applyfn, __VA_ARGS__)
#define PLAY_FFLD(applyfn, ...) PLAY_MOVE(Cardloc::FIELD, nullptr, applyfn, __VA_ARGS__)
#define PLAY_FREE_FFLD(applyfn, ...) PLAY_FREE_MOVE(Cardloc::FIELD, nullptr, applyfn, __VA_ARGS__)
#define PLAY_PRNK(applyfn, ...) PLAY_MOVE(Cardloc::PRANK, nullptr, applyfn, __VA_ARGS__)

#define PLAY_AFF_ATCK(s, m, a, c, i) bind(&SmacCardSet::affiliate_attack, this, _1, _2, (s), (m), (a), (c), (i))

static inline Event make_attack(Player *from, Player *to, Card *c, int16_t hit, atype_t atype) {
	Event e(ETYPE::ATTACK);
	e.c = c;
	e.from = from;
	e.to = to;
	e.a.hit = hit;
	e.a.aff = c->ci.aff;
	e.a.atype = atype;
	return e;
}

static inline Event make_attack(Player *from, Player *to, Card *c, int16_t hit) {
	return make_attack(from, to, c, hit, c->ci.atype);
}

static inline Event make_counter(Player *from, Card *c, const Event& ev, int16_t hit) {
	assertEqual(ev.etype, ETYPE::ATTACK);
	Event e = make_attack(from, ev.from, c, hit);
	e.a.counter_count = ev.a.counter_count + 1;
	// START card specific: pineapple
	if (ev.a.pineapple)
		e.a.hit = 0; // XXX should this be annulled instead?
	// END card specific: pineapple
	e.prev = make_shared<Event>(ev);
	return e;
}

static inline Event make_heal(Player *p, Card *c, int16_t hit) {
	Event e(ETYPE::HEAL);
	e.c = c;
	e.from = p;
	e.to = p;
	e.h.hit = hit;
	return e;
}

static inline Event make_discard(Player *from, Card *cfrom, Player *to, Card *cchoice) {
	Event e(ETYPE::DISCARD);
	e.c = cfrom;
	e.from = from;
	e.to = to;
	e.d.cdiscard = cchoice;
	return e;
}

static inline Event make_discard(Card *c) {
	return make_discard(nullptr, c, nullptr, c);
}

SmacCardSet::~SmacCardSet() {
	for (auto& c : _d) {
		delete c;
	}
}

Event SmacCardSet::generic_attack(Player *p, Card *c, int16_t hit) {
	Player *pchoice = _engine->select_target(p);
	return make_attack(p, pchoice, c, hit);
}

Event SmacCardSet::generic_counter(Player *p, Card *c, const Event& ev, int16_t hit) {
	assertEqual(ev.etype, ETYPE::ATTACK);
	return make_counter(p, c, ev, hit);
}

Event SmacCardSet::generic_heal(Player *p, Card *c, int16_t hit) {
	return make_heal(p, c, hit);
}

Event SmacCardSet::affiliate_attack(Player *p, Card *c, int16_t s, int16_t m, int16_t a, int16_t cc, int16_t i) {
	Player *pchoice = _engine->select_target(p);

	int16_t hit = 0;
	for (auto& card : pchoice->hand) {
		if (card->ci.aff & AFF::S)
			hit += s;
		if (card->ci.aff & AFF::M)
			hit += m;
		if (card->ci.aff & AFF::A)
			hit += a;
		if (card->ci.aff & AFF::C)
			hit += cc;
		if (card->ci.aff & AFF::I)
			hit += i;
	}

	Event e = make_attack(p, pchoice, c, hit);
	return e;
}

Event SmacCardSet::bother_attack(Player *p, Card *c, cardpack& also_flux) {
	cardpack bothers;
	for (auto& cc : p->hand) { // includes the card itself
		if (cc->_trait == Card::Trait::BOTHER)
			bothers.push_back(cc);
	}
	int16_t hit = static_cast<int16_t>(bothers.size() * 2 - 1);
	Event e = generic_attack(p, c, hit);
	also_flux.insert(also_flux.end(), bothers.begin(), bothers.end());
	return e;
}

Event SmacCardSet::stare_attack(Player *p, Card *c, cardpack& also_flux) {
	cardpack stares;
	for (auto& cc : p->hand) {
		if (cc->_trait == Card::Trait::STARE && cc != c)
			stares.push_back(cc);
	}
	if (!stares.empty())
		stares = _engine->select_multi_cards(p, stares);
	int16_t hit = c->_val_int16;
	for (auto& cc : stares) {
		assertEqual(cc->_trait, Card::Trait::STARE);
		hit += cc->_val_int16;
	}
	also_flux.insert(also_flux.end(), stares.begin(), stares.end());
	return generic_attack(p, c, hit);
}

void SmacCardSet::agree() {
	_d.push_back(
		(new Card(CardInfo({
			"Agree",
			{ "Attack's damage +1" },
			{ "Screw you!", "If you want!" },
			AFF::S | AFF::C,
			CTYPE::CNTR,
			ATYPE::MENTAL,
		}), bind(&SmacCardSet::agree_playfn, this, _PFROM, _CARD, _EV))));
}

Event SmacCardSet::agree_playfn(Player *p, Card *c, const Event& ev) {
	assertEqual(ev.etype, ETYPE::ATTACK);
	Event e = make_counter(p, c, ev, ev.a.hit + 1);
	return e;
}

void SmacCardSet::air_guitar() {
	_d.push_back(
		(new Card(CardInfo({
			"Air Guitar",
			{ "3" },
			{ "I'm using the right fingerings." },
			AFF::M,
			CTYPE::CNTR,
			ATYPE::MENTAL,
		}), PLAY_CNTR(3))));
}

void SmacCardSet::amnesia_dust() {
	_d.push_back(
		(new Card(CardInfo({
			"Amnesia dust",
			{ "3", "The opponent loses", "their next turn" },
			{ "You are Santa." },
			AFF::A,
			CTYPE::ATCK,
			ATYPE::MENTAL,
		}), PLAY_ATCK_WITH_FN(3, &SmacCardSet::amnesia_dust_applyfn, _PTARGET))));
}

Evlist SmacCardSet::amnesia_dust_applyfn(Player *pchoice) {
	pchoice->skip++;
	// XXX coord message saying turn will be skipped?

	return Evlist();
}

void SmacCardSet::big_fat_target() {
	_d.push_back(
		(new Card(CardInfo({
			"big fat target", 
			{ "select a person. they must be", "targeted except on their turn.", "this lasts for 1 round" },
			{ "don't go near an archery range" },
			AFF::A | AFF::C | AFF::I, 
			CTYPE::SPCL,
		}), bind(&SmacCardSet::big_fat_target_playfn, this, _PFROM, _CARD)))
		->playable_type(CTYPE::ATCK)
	);
}

Event SmacCardSet::big_fat_target_playfn(Player *p, Card *c) {
	Player *pchoice = _engine->select_target(p);

	Event e(ETYPE::MOVE);
	e.c = c;
	e.from = p;
	e.to = pchoice;
	e.m.where = Cardloc::OTHER;
	e.m.cmove = c;
	e.apply_fn = bind(&SmacCardSet::big_fat_target_applyfn, this, _PFROM, _CARD, _PTARGET);

	return e;
}

Evlist SmacCardSet::big_fat_target_applyfn(Player *p, Card *c, Player *pchoice) {
	_engine->elect_target_fns.emplace_back(c, [=](const Player *ptargeting, std::list<Player*> players) -> Player* {
		if (ptargeting != pchoice) {
			return pchoice;
		}
		return nullptr;
	});
	p->turnstart_fns.emplace_back(c, [=]() {
		Event e = make_discard(c);
		// XXX coord message saying big fat target has gone out of effect (actually, should be handled entirely by coord)
		return single_event(e);
	});
	return Evlist();
}

void SmacCardSet::boofy_jacket() {
	_d.push_back(
		(new Card(CardInfo({
			"Boofy jacket",
			{
				"Physical damage does nothing,",
				"but you have to hold",
				"your arms out",
			},
			{ "I'm not fat, I'm... boofy" },
			AFF::C,
			CTYPE::ITEM,
		}), PLAY_ITEM(&SmacCardSet::boofy_jacket_applyfn, _PTARGET, _CARD))));
}

Evlist SmacCardSet::boofy_jacket_applyfn(Player *p, Card *c) {
	p->damage_fns.emplace_back(c, [](Attack& a) {
		// [Player x] has a boofy jacket!
		// TODO: coord->card_had_effect(c);
		if (a.atype & ATYPE::PHYSICAL)
			a.annulled = true;
	});
	return Evlist();
}

void SmacCardSet::boom() {
	_d.push_back(
		(new Card(CardInfo({
			"BOOM",
			{ "5", "Applies to all players,", "including the user." },
			{ "Pretty colors..." },
			AFF::S,
			CTYPE::ATCK,
			ATYPE::PHYSICAL,
		}), bind(&SmacCardSet::boom_playfn, this, _PFROM, _CARD))));
}

Event SmacCardSet::boom_playfn(Player *p, Card *c) {
	Event ep(ETYPE::PARENT);
	ep.c = c;
	ep.from = p;
	ep.to = nullptr;

	for (auto& plyr : _engine->ps) {
		Event e = make_attack(p, plyr, c, 5);
		ep.child_events.push_back(e);
	}

	return ep;
}

void SmacCardSet::bother_machine_gun() {
	_d.push_back(
		(new Card(CardInfo({
			"Bother machine gun",
			{
				"all bother cards in your hand,",
				"the discard pile, and everyone",
				"else's hand get played on one",
				"person of your choice",
			},
			{
				"long distance rolling \"bother\"",
			},
			AFF::M,
			CTYPE::ATCK,
			ATYPE::PHYSICAL,
		}), bind(&SmacCardSet::bother_machine_gun_playfn, this, _PFROM, _CARD, _ALSO_FLUX)))
		->set_trait(Card::Trait::BOTHER)
	);
}

Event SmacCardSet::bother_machine_gun_playfn(Player *p, Card *c, cardpack& also_flux) {
	cardpack bothers;
	for (auto& pp : _engine->ps) {
		for (auto& cc : pp->hand) { // includes the card itself
			if (cc->_trait == Card::Trait::BOTHER)
				bothers.push_back(cc);
		}
	}
	for (auto& cc : _engine->f->discard) {
		if (cc->_trait == Card::Trait::BOTHER)
			bothers.push_back(cc);
	}
	int16_t hit = static_cast<int16_t>(bothers.size() * 2 - 1);
	Event e = generic_attack(p, c, hit);
	also_flux.insert(also_flux.end(), bothers.begin(), bothers.end());
	return e;
}

void SmacCardSet::bucket_of_jello() {
	_d.push_back(
		(new Card(CardInfo({
			"Bucket of jello",
			{
				"Whoever changes the field",
				"takes 5 physical damage",
			},
			{
				"The bucket hurts",
				"more than the jello.",
			},
			AFF::S | AFF::M,
			CTYPE::PRNK,
		}), PLAY_PRNK(&SmacCardSet::bucket_of_jello_applyfn, _CARD))));
}

Evlist SmacCardSet::bucket_of_jello_applyfn(Card *c) {
	_engine->last_minute_fns.emplace_back(c, [=](const Event& ev) {
		if (ev.etype == ETYPE::MOVE &&
			ev.m.cmove->ci.ctype & CTYPE::FFLD &&
			ev.m.where == Cardloc::FIELD) {
			Evlist elist;

			Event ea = make_attack(nullptr, ev.from, c, 5, ATYPE::PHYSICAL);
			ea.a.uncounterable = true;
			elist.push_back(ea);

			Event ed = make_discard(c); // discard the prank
			elist.push_back(ed);

			elist.push_back(ev); // field still changes
			return elist;
		}
		return Evlist();
	});
	return Evlist();
}

void SmacCardSet::caution_low_ceiling() {
	_d.push_back(
		(new Card(CardInfo({
			"Caution: low ceiling",
			{
				"max cards cannot be played",
				"unless they are also alex cards",
			},
			{
				"doncha wish your girlfriend was",
				"short like me?",
			},
			AFF::A,
			CTYPE::FFLD,
		}), PLAY_FFLD(&SmacCardSet::caution_low_ceiling_applyfn, _CARD))));
}

Evlist SmacCardSet::caution_low_ceiling_applyfn(Card *c) {
	_engine->playability_fns.emplace_back(c, [](Card *c) {
		return !(c->ci.aff & AFF::M) || c->ci.aff & AFF::A;
	});
	return Evlist();
}

void SmacCardSet::chair_stare() {
	_d.push_back(
		(new Card(CardInfo({
			"Chair Stare", 
			{ "2", "All stare cards in your hand", "may be played." },
			{ "Must... focus... on... chair..." },
			AFF::M,
			CTYPE::ATCK,
			ATYPE::MENTAL,
		}), bind(&SmacCardSet::stare_attack, this, _PFROM, _CARD, _ALSO_FLUX)))
		->set_trait(Card::Trait::STARE)
		->set_val(2)
	);
}

void SmacCardSet::chocolate_cow() {
	_d.push_back(
		(new Card(CardInfo({
			"Chocolate Cow",
			{
				"Heals 2 every turn of the user.",
				"Starts when this card is used.",
				"Lasts 3 heals.",
			},
			{ "My grandma claims to have one." },
			AFF::A | AFF::C | AFF::I,
			CTYPE::HEAL,
		}), bind(&SmacCardSet::chocolate_cow_playfn, this, _PFROM, _CARD))));
}

Event SmacCardSet::chocolate_cow_playfn(Player *p, Card *c) {
	Event e = make_heal(p, c, 2);
	e.h.chocolate_cow = true;
	e.apply_fn = bind(&SmacCardSet::chocolate_cow_applyfn, this, _PTARGET, _CARD);
	return e;
}

Evlist SmacCardSet::chocolate_cow_applyfn(Player *p, Card *c) {
	c->_val_uint16 = 2;
	Event em(ETYPE::MOVE);
	em.c = c;
	em.from = nullptr;
	em.to = p;
	em.m.where = Cardloc::OTHER;
	em.m.cmove = c;
	p->turnstart_fns.emplace_back(c, [=]() {
		Evlist evlist;
		Event e = make_heal(p, c, 2);
		e.h.unstealable = true;
		evlist.push_back(e);
		c->_val_uint16--;
		if (c->_val_uint16 == 0) {
			evlist.push_back(make_discard(c));
		}
		return evlist;
	});
	return single_event(em);
}

void SmacCardSet::circle_circle() {
	_d.push_back(
		(new Card(CardInfo({
			"Circle circle",
			{
				"rotate hands counterclockwise",
				"every turn",
			},
			{ "rectangle, rectangle,", "rectangle..." },
			AFF::C,
			CTYPE::FFLD,
		}), PLAY_FFLD(&SmacCardSet::circle_circle_applyfn, _CARD))));
}

Evlist SmacCardSet::circle_circle_applyfn(Card *c) {
	_engine->before_turn_start_fns.emplace_back(c, [this]() {
		deck temp(nullptr);
		auto ps = _engine->ps;
		ps.remove_if([](Player *p) { return p->_la_la_land; });
		temp.swap(ps.front()->hand);
		for (auto iter = ps.rbegin(); iter != ps.rend(); ++iter) {
			temp.swap((*iter)->hand);
		}
		_engine->resync_hands(ps);
		for (auto& p : ps) {
			p->draw(); // for extra arm
		}
	});
	return Evlist();
}

void SmacCardSet::condescending() {
	_d.push_back(
		(new Card(CardInfo({
			"condescending",
			{
				"3",
				"4 if the opponent has 3 or more",
				"Alex cards",
			},
			{ "high five!" },
			AFF::M,
			CTYPE::ATCK,
			ATYPE::MENTAL,
		}), bind(&SmacCardSet::condescending_playfn, this, _PFROM, _CARD))));
}

Event SmacCardSet::condescending_playfn(Player *p, Card *c) {
	Event e = generic_attack(p, c, 3);
	int nAlex = 0;
	for (auto& cc : e.to->hand) {
		if (cc->ci.aff & AFF::A)
			nAlex++;
	}
	if (nAlex >= 3)
		e.a.hit = 4;
	return e;
}

void SmacCardSet::cow_hat() {
	_d.push_back(
		(new Card(CardInfo({
			"Cow hat",
			{ "4" },
			{ "It comes with built-in", "cup holders!" },
			AFF::I,
			CTYPE::CNTR,
			ATYPE::MENTAL,
		}), PLAY_CNTR(4))));
}

void SmacCardSet::crud_comix() {
	_d.push_back(
		(new Card(CardInfo({
			"Crud Comix",
			{
				"2 damage per Spencer card and",
				"-1 per Cody card",
				"in the target's hand",
			},
			{ "I... HATE... STICK... SONIC!!!" },
			AFF::C,
			CTYPE::ATCK,
			ATYPE::MENTAL,
		}), PLAY_AFF_ATCK(2, 0, 0, -1, 0))));
}

void SmacCardSet::dazzling_gift() {
	_d.push_back(
		(new Card(CardInfo({
			"\"Dazzling\" Gift",
			{ "3", "An item from the opponent", "is discarded" },
			{ "What is that? A roasted cherry?" },
			AFF::A,
			CTYPE::ATCK,
			ATYPE::PHYSICAL,
		}), PLAY_ATCK_WITH_FN(3, &SmacCardSet::dazzling_gift_applyfn, _PFROM, _CARD, _PTARGET))));
}

Evlist SmacCardSet::dazzling_gift_applyfn(Player *p, Card *c, Player *pchoice) {
	cardpack items = pchoice->item.get_cards();
	Card *cchoice = nullptr;
	if (!items.empty()) {
		cchoice = _engine->select_card(p, items);
		// else XXX coord message saying "pchoice has no items"?

		Event e = make_discard(p, c, pchoice, cchoice);
		// XXX coord message saying whatever item has been removed (actually, should be handled entirely by coord)
		return single_event(e);
	}
	return Evlist();
}

void SmacCardSet::deaf() {
	_d.push_back(
		(new Card(CardInfo({
			"Deaf",
			{
				"verbal damage does not effect",
				"you, but you can't reply to what",
				"anyone says unless you pretend",
				"to misshear them",
			},
			{ "WHAT? TRUCK BLUE?" },
			AFF::S | AFF::M | AFF::A | AFF::C | AFF::I,
			CTYPE::ITEM,
		}), PLAY_ITEM(&SmacCardSet::deaf_applyfn, _PTARGET, _CARD))));
}

Evlist SmacCardSet::deaf_applyfn(Player *p, Card *c) {
	p->damage_fns.emplace_back(c, [](Attack& a) {
		// [Player x] is deaf!
		// TODO: coord->card_had_effect(c);
		if (a.atype & ATYPE::VERBAL)
			a.annulled = true;
	});
	return Evlist();
}

void SmacCardSet::dumping_area() {
	_d.push_back(
		(new Card(CardInfo({
			"dumping area",
			{ "physical cards can't be played" },
			{
				"dumping area?  you smell like a",
				"crapping area!",
			},
			AFF::S | AFF::M | AFF::C,
			CTYPE::FFLD,
		}), PLAY_FFLD(&SmacCardSet::dumping_area_applyfn, _CARD))));
}

Evlist SmacCardSet::dumping_area_applyfn(Card *c) {
	_engine->playability_fns.emplace_back(c, [](Card *c) {
		return !(c->ci.atype & ATYPE::PHYSICAL);
	});
	return Evlist();
}

void SmacCardSet::emit_smugness() {
	_d.push_back(
		(new Card(CardInfo({
			"emit smugness",
			{
				"whenever someone plays a card on",
				"you, they take 1 uncounterable",
				"mental dammage (doesn't work in",
				"Parson's class)",
			},
			{
				"with a picture like that who",
				"needs flavor text?",
			},
			AFF::A,
			CTYPE::ITEM,
		}), PLAY_ITEM(&SmacCardSet::emit_smugness_applyfn, _PTARGET, _CARD))));
}

Evlist SmacCardSet::emit_smugness_applyfn(Player *p, Card *c) {
	_engine->after_card_played_fns.emplace_back(c, [=](const Event& ev) {
		for (auto& cc : _engine->f->field) {
			if (cc->_trait == Card::Trait::PARSONS_CLASS && !p->_la_la_land)
				return Evlist();
		}
		if (ev.etype == ETYPE::ATTACK && ev.to == p && ev.from != nullptr) {
			Event e = make_attack(p, ev.from, c, 1, ATYPE::MENTAL);
			e.a.uncounterable = true;
			return single_event(e);
		}
		return Evlist();
	});

	return Evlist();
}

void SmacCardSet::extra_arm() {
	_d.push_back(
		(new Card(CardInfo({
			"extra arm",
			{
				"you can now have one extra card",
				"in your hand. when this is",
				"discarded, you may only have the",
				"normal amount in your hand",
			},
			{ "that one guy has like... 4" },
			AFF::I,
			CTYPE::ITEM,
		}), PLAY_ITEM(&SmacCardSet::extra_arm_applyfn, _PTARGET, _CARD))));
}

Evlist SmacCardSet::extra_arm_applyfn(Player *p, Card *c) {
	Event e(ETYPE::PARAM);
	e.c = c;
	e.from = nullptr;
	e.to = p;
	e.p.parameter = Parameter::HMAX;
	e.p.adjustment = +1;

	return single_event(e);
}

void SmacCardSet::extra_leg() {
	_d.push_back(
		(new Card(CardInfo({
			"extra leg",
			{
				"You may have one more item",
				"than the typical amount",
			},
			{ "Attack of the tripods" },
			AFF::I,
			CTYPE::ITEM,
		}), PLAY_FREE_ITEM(&SmacCardSet::extra_leg_applyfn, _PTARGET, _CARD))));
}

Evlist SmacCardSet::extra_leg_applyfn(Player *p, Card *c) {
	Event e(ETYPE::PARAM);
	e.c = c;
	e.from = nullptr;
	e.to = p;
	e.p.parameter = Parameter::IMAX;
	e.p.adjustment = +1;

	return single_event(e);
}

void SmacCardSet::folder_maze() {
	_d.push_back(
		(new Card(CardInfo({
			"Folder maze",
			{
				"Deals 1 uncounterable mental",
				"damage to any opponent each time",
				"you take a turn.",
			},
			{ "The folder lied to you." },
			AFF::M | AFF::A | AFF::C,
			CTYPE::ITEM,
		}), PLAY_ITEM(&SmacCardSet::folder_maze_applyfn, _PTARGET, _CARD))));
}

Evlist SmacCardSet::folder_maze_applyfn(Player *p, Card *c) {
	p->turnstart_fns.emplace_back(c, [=]() {
		Player *pchoice = _engine->select_target(p);
		Event e = make_attack(p, pchoice, c, 1, ATYPE::MENTAL);
		e.a.uncounterable = true;
		return single_event(e);
	});
	return Evlist();
}

void SmacCardSet::free_chocolate_milk() {
	_d.push_back(
		(new Card(CardInfo({
			"Free Chocolate Milk",
			{ "+4" },
			{ "It's free, chocolate, and milk!" },
			AFF::A | AFF::C | AFF::I,
			CTYPE::HEAL,
		}), PLAY_HEAL(4))));
}

void SmacCardSet::free_milk() {
	_d.push_back(
		(new Card(CardInfo({
			"Free Milk",
			{ "+3" },
			{ "Dibs on free milk!" },
			AFF::A | AFF::C | AFF::I,
			CTYPE::HEAL,
		}), PLAY_HEAL(3))));
}

void SmacCardSet::free_milk_steal() {
	_d.push_back(
		(new Card(CardInfo({
			"Free milk steal", 
			{ "If someone else plays a heal", "play this card and all", "effects go to you." },
			{ "Wait... you can just do that?" },
			AFF::I, 
			CTYPE::SPCL,
		}), bind(&SmacCardSet::free_milk_steal_playfn, this, _PFROM, _CARD, _EV, _ALSO_FLUX)))
		->can_play_now_fn(bind(&SmacCardSet::free_milk_steal_canplayfn, this, _1))
	);
}

bool SmacCardSet::free_milk_steal_canplayfn(const Event& ev) {
	if (ev.etype == ETYPE::HEAL && !ev.h.unstealable)
		return true;
	return false;
}

Event SmacCardSet::free_milk_steal_playfn(Player *p, Card *c, const Event &ev, cardpack& also_flux) {
	(void)c; // XXX eventually use this for a coord message saying &c->ci was responsible for stealing etc.
	assertEqual(ev.etype, ETYPE::HEAL);
	assertFalse(ev.h.unstealable);
	Event e = ev.c->play_fn(p, ev.c, ev, also_flux);
	e.prev = make_shared<Event>(ev);
	return e;
}

void SmacCardSet::i_know_you_are_but_what_am_i() {
	_d.push_back(
		(new Card(CardInfo({
			"I know you are but what am I",
			{
				"4",
				"after being played this goes",
				"into the other person's hand.",
			},
			{ "No you're mom." },
			AFF::A,
			CTYPE::CNTR,
			ATYPE::VERBAL,
		}), bind(&SmacCardSet::i_know_you_are_playfn, this, _PFROM, _CARD, _EV))));
}

Event SmacCardSet::i_know_you_are_playfn(Player *p, Card *c, const Event& ev) {
	Event e(ETYPE::PARENT);
	e.c = c;
	e.from = p;
	e.to = ev.from;
	e.child_events.push_back(generic_counter(p, c, ev, 4));
	// the move is not an applyfn because it should always go through
	// whether or not "I know you are" gets countered
	e.prev = e.child_events.front().prev;
	Event em(ETYPE::MOVE);
	em.c = c;
	em.from = p;
	em.to = ev.from;
	em.m.where = Cardloc::HAND;
	em.m.cmove = c;
	e.child_events.push_back(em);
	return e;
}

void SmacCardSet::ignored() {
	_d.push_back(
		(new Card(CardInfo({
			"ignore'd",
			{ "4" },
			{ "Whatever..." },
			AFF::A | AFF::C,
			CTYPE::CNTR,
			ATYPE::VERBAL,
		}), PLAY_CNTR(4))));
}

void SmacCardSet::im_a_superior_being() {
	_d.push_back(
		(new Card(CardInfo({
			"I'm a superior being",
			{ "3" },
			{ "inferior non-elf..." },
			AFF::A,
			CTYPE::ATCK,
			ATYPE::MENTAL,
		}), PLAY_ATCK(3))));
}

void SmacCardSet::khaos_realm() {
	_d.push_back(
		(new Card(CardInfo({
			"Khaos Realm",
			{
				"There can be any amount",
				"of field cards in play.",
				"If this card is removed, all",
				"other fields are also destroyed.",
			},
			{ "Don't you mean \"oink\"?" },
			AFF::C,
			CTYPE::FFLD,
		}), PLAY_FREE_FFLD(&SmacCardSet::khaos_realm_applyfn, _CARD))));
}

Evlist SmacCardSet::khaos_realm_applyfn(Card *c) {
	_engine->_khaos_realm = true;
	c->discard_fn([this]() {
		Evlist evlist;
		_engine->_khaos_realm = false;
		for (auto& cc : _engine->f->field) {
			evlist.push_back(make_discard(cc));
		}
		return evlist;
	});
	return Evlist();
}

void SmacCardSet::la_la_land() {
	_d.push_back(
		(new Card(CardInfo({
			"La La Land",
			{
				"The cards that you play are not",
				"affected by the field",
				"(ping pong excluded)",
			},
			{ "lalalalalala..." },
			AFF::S | AFF::M | AFF::A | AFF::C | AFF::I,
			CTYPE::ITEM,
		}), PLAY_ITEM(&SmacCardSet::la_la_land_applyfn, _PTARGET, _CARD))));
}

Evlist SmacCardSet::la_la_land_applyfn(Player *p, Card *c) {
	p->_la_la_land = true;
	c->discard_fn([=]() {
		p->_la_la_land = false;
		return Evlist();
	});
	return Evlist();
}

void SmacCardSet::library() {
	_d.push_back(
		(new Card(CardInfo({
			"Library",
			{ "verbal cards cannot be played" },
			{ "SHHHHHHHHHHHHHHH..." },
			AFF::S | AFF::M | AFF::A | AFF::C | AFF::I,
			CTYPE::FFLD,
		}), PLAY_FFLD(&SmacCardSet::library_applyfn, _CARD))));
}

Evlist SmacCardSet::library_applyfn(Card *c) {
	_engine->playability_fns.emplace_back(c, [](Card *c) {
		return !(c->ci.atype & ATYPE::VERBAL);
	});
	return Evlist();
}

void SmacCardSet::long_distance_bother() {
	_d.push_back(
		(new Card(CardInfo({
			"long distance bother",
			{
				"all bother cards in your hand",
				"can be played at once doing 2x",
				"the number of bother cards",
				"played minus 1",
			},
			{
				"long distance calls cost 0.50$,",
				"long distance bothers...",
			},
			AFF::M,
			CTYPE::ATCK,
			ATYPE::PHYSICAL,
		}), bind(&SmacCardSet::bother_attack, this, _PFROM, _CARD, _ALSO_FLUX)))
		->set_trait(Card::Trait::BOTHER)
	);
}

void SmacCardSet::math_teacher_stare() {
	_d.push_back(
		(new Card(CardInfo({
			"Math teacher Stare", 
			{ "5", "All stare cards in your hand", "may be played" },
			{ "ooh. Creepy." },
			AFF::S,
			CTYPE::ATCK,
			ATYPE::MENTAL,
		}), bind(&SmacCardSet::stare_attack, this, _PFROM, _CARD, _ALSO_FLUX)))
		->set_trait(Card::Trait::STARE)
		->set_val(5)
	);
}

void SmacCardSet::meat_shield() {
	_d.push_back(
		(new Card(CardInfo({
			"meat shield",
			{
				"Select someone else to take the",
				"damage and all effects",
			},
			{ "that boofy jacket sure is useful" },
			AFF::A | AFF::C,
			CTYPE::CNTR,
			ATYPE::PHYSICAL,
		}), bind(&SmacCardSet::meat_shield_playfn, this, _PFROM, _CARD, _EV))));
}

Event SmacCardSet::meat_shield_playfn(Player *p, Card *c, const Event& ev) {
	assertEqual(ev.etype, ETYPE::ATTACK);
	Player *pchoice = _engine->select_target(p);
	Event e(ETYPE::PARENT);
	e.c = c;
	e.from = p;
	e.to = pchoice;
	e.prev = make_shared<Event>(ev);
	Event ea = make_attack(p, pchoice, ev.c, ev.a.hit);
	ea.a.aff = c->ci.aff;
	ea.a.counter_count++;
	ea.a.pineapple = ev.a.pineapple;
	ea.apply_fn = ev.apply_fn;
	ea.prev = e.prev;
	e.child_events.push_back(ea);
	return e;
}

void SmacCardSet::no_brainer() {
	_d.push_back(
		(new Card(CardInfo({
			"No brainer",
			{
				"you have no mind!",
				"mental attacks do nothing",
				"to you. lose this card",
				"if you act smart.",
			},
			{ "duh... mental whats?" },
			AFF::S | AFF::M | AFF::A | AFF::C | AFF::I,
			CTYPE::ITEM,
		}), PLAY_ITEM(&SmacCardSet::no_brainer_applyfn, _PTARGET, _CARD))));
}

Evlist SmacCardSet::no_brainer_applyfn(Player *p, Card *c) {
	p->damage_fns.emplace_back(c, [](Attack& a) {
		// [Player x] has no brain!
		// TODO: coord->card_had_effect(c);
		if (a.atype & ATYPE::MENTAL)
			a.annulled = true;
	});
	return Evlist();
}

void SmacCardSet::no_your_mom_1() {
	_d.push_back(
		(new Card(CardInfo({
			"No your mom",
			{
				"3",
				"4 if it counters your mom",
			},
			{ "No your mom, no your mom..." },
			AFF::S | AFF::M | AFF::A | AFF::C,
			CTYPE::CNTR,
			ATYPE::VERBAL,
		}), bind(&SmacCardSet::no_your_mom_playfn, this, _PFROM, _CARD, _EV)))
		->set_trait(Card::Trait::NO_YOUR_MOM)
	);
}

Event SmacCardSet::no_your_mom_playfn(Player *p, Card *c, const Event& ev) {
	Event e = generic_counter(p, c, ev, 3);
	if (ev.c->_trait == Card::Trait::YOUR_MOM)
		e.a.hit = 4;
	return e;
}

void SmacCardSet::nope() {
	_d.push_back(
		(new Card(CardInfo({
			"NOPE.",
			{ "" },
			{ "" },
			AFF::S | AFF::M | AFF::A | AFF::C | AFF::I,
			CTYPE::SPCL,
		}), bind(&SmacCardSet::nope_playfn, this, _PFROM, _CARD, _EV)))
		->can_play_now_fn([](const Event& ev) { return ev.etype != ETYPE::NONE; })
	);
}

Event SmacCardSet::nope_playfn(Player *p, Card *c, const Event& ev) {
	Event e(ETYPE::PARENT);
	e.c = c;
	e.from = p;
	e.to = nullptr;
	e.prev = make_shared<Event>(ev);
	if (ev.prev) {
		Event prev = *ev.prev;
		prev.attempted = false;
		e.child_events.push_back(prev);
	}
	return e;
}

void SmacCardSet::oops() {
	_d.push_back(
		(new Card(CardInfo({
			"Oops",
			{ "Destroys the current field card" },
			{ "Ooh... shiny button" },
			AFF::S | AFF::M | AFF::A | AFF::C | AFF::I,
			CTYPE::SPCL,
		}), bind(&SmacCardSet::oops_playfn, this, _PFROM, _CARD)))
		->playable_fn(bind(&SmacCardSet::oops_canplaymainfn, this))
	);
}

bool SmacCardSet::oops_canplaymainfn() {
	return !_engine->f->field.empty();
}

Event SmacCardSet::oops_playfn(Player *p, Card *c) {
	cardpack fields = _engine->f->field.get_cards();
	assertFalse(fields.empty());
	Card *cchoice = fields.front();
	if (fields.size() > 1)
		cchoice = _engine->select_card(p, fields);
	return make_discard(p, c, nullptr, cchoice);
}

void SmacCardSet::oops_i_dropped_it() {
	_d.push_back(
		(new Card(CardInfo({
			"Oops I dropped it",
			{ "One of your opponents must", "discard an item of your choice." },
			{ "uh... sorry" },
			AFF::S,
			CTYPE::SPCL,
		}), bind(&SmacCardSet::oops_i_dropped_it_playfn, this, _PFROM, _CARD)))
		->playable_fn(bind(&SmacCardSet::oops_i_dropped_it_canplaymainfn, this))
	);
}

bool SmacCardSet::oops_i_dropped_it_canplaymainfn() {
	return std::any_of(_engine->ps.begin(), _engine->ps.end(), [](Player *p) {
		return !p->item.empty();
	});
}

Event SmacCardSet::oops_i_dropped_it_playfn(Player *p, Card *c) {
	// XXX limit targets to those with items
	Player *pchoice = _engine->select_target(p);
	cardpack items = pchoice->item.get_cards();
	assertFalse(items.empty());
	Card *cchoice = _engine->select_card(p, items);

	Event e = make_discard(p, c, pchoice, cchoice);
	// XXX coord message saying whatever item has been removed (actually, should be handled entirely by coord)

	return e;
}

void SmacCardSet::owned() {
	_d.push_back(
		(new Card(CardInfo({
			"owne'd",
			{
				"All attacks/counters do +1 but",
				"you have to say all attack names",
				"with a d on the end.",
			},
			{ "Your mom'd!!!" },
			AFF::M | AFF::A | AFF::C | AFF::I,
			CTYPE::ITEM,
		}), PLAY_ITEM(&SmacCardSet::owned_applyfn, _PTARGET, _CARD))));
}

Evlist SmacCardSet::owned_applyfn(Player *p, Card *c) {
	_engine->modify_attack_fns.emplace_back(c, [=](const Event& ev, AttackModifier& amod) {
		assertEqual(ev.etype, ETYPE::ATTACK);
		if (ev.from == p)
			amod.adder += 1;
	});
	return Evlist();
}

void SmacCardSet::parsons_class() {
	_d.push_back(
		(new Card(CardInfo({
			"Parsons' class",
			{ "mental cards can't be played" },
			{ "be self-directed" },
			AFF::S | AFF::M | AFF::A | AFF::C,
			CTYPE::FFLD,
		}), PLAY_FFLD(&SmacCardSet::parsons_class_applyfn, _CARD)))
		->set_trait(Card::Trait::PARSONS_CLASS)
	);
}

Evlist SmacCardSet::parsons_class_applyfn(Card *c) {
	_engine->playability_fns.emplace_back(c, [](Card *c) {
		return !(c->ci.atype & ATYPE::MENTAL);
	});
	return Evlist();
}

void SmacCardSet::percy_grainger() {
	_d.push_back(
		(new Card(CardInfo({
			"Percy Grainger",
			{
				"whenever you are hurt two times",
				"in a row by the same type of",
				"damage, you heal 5",
			},
			{ "That's a lot of milk..." },
			AFF::S | AFF::M,
			CTYPE::ITEM,
		}), PLAY_ITEM(&SmacCardSet::percy_grainger_applyfn, _PTARGET, _CARD))));
}

Evlist SmacCardSet::percy_grainger_applyfn(Player *p, Card *c) {
	_engine->post_damage_fns.emplace_back(c, [=](const Event& ev) {
		assertEqual(ev.etype, ETYPE::ATTACK);
		if (ev.to == p && !ev.a.annulled) {
			if (ev.a.atype & c->_val_atype) {
				c->_val_atype = ATYPE::NONE;
				Event e = make_heal(p, c, 5);
				e.h.unstealable = true;
				return single_event(e);
			}
			if (ev.a.atype & (ATYPE::PHYSICAL | ATYPE::VERBAL | ATYPE::MENTAL))
				c->_val_atype = ev.a.atype;
		}
		return Evlist();
	});
	return Evlist();
}

void SmacCardSet::pineapple() {
	_d.push_back(
		(new Card(CardInfo({
			"Pineapple!",
			{
				"4",
				"counters do no damage",
			},
			{ "It scares the jeepers out of MC." },
			AFF::I,
			CTYPE::ATCK,
			ATYPE::VERBAL,
		}), bind(&SmacCardSet::pineapple_playfn, this, _PFROM, _CARD))));
}

Event SmacCardSet::pineapple_playfn(Player *p, Card *c) {
	Event e = generic_attack(p, c, 4);
	e.a.pineapple = true;
	return e;
}

void SmacCardSet::ping_pong() {
	_d.push_back(
		(new Card(CardInfo({
			"Ping pong",
			{ "counters can be countered" },
			{ "No your mom, no your mom..." },
			AFF::M | AFF::A | AFF::C,
			CTYPE::FFLD,
		}), PLAY_FFLD(&SmacCardSet::ping_pong_applyfn, _CARD))));
}

Evlist SmacCardSet::ping_pong_applyfn(Card *c) {
	_engine->_ping_pong = true;
	c->discard_fn([this]() {
		_engine->_ping_pong = false;
		return Evlist();
	});
	return Evlist();
}

void SmacCardSet::ranch_in_milk() {
	_d.push_back(
		(new Card(CardInfo({
			"Ranch in Milk",
			{
				"If someone plays a heal,",
				"instead of healing that much,",
				"they take that much",
				"physical damage.",
			},
			{ "Is that a crouton?" },
			AFF::S | AFF::M,
			CTYPE::PRNK,
		}), PLAY_PRNK(&SmacCardSet::ranch_in_milk_applyfn, _CARD))));
}

Evlist SmacCardSet::ranch_in_milk_applyfn(Card *c) {
	_engine->last_minute_fns.emplace_back(c, [=](const Event& ev) {
		if (ev.etype == ETYPE::HEAL && ev.c->ci.ctype & CTYPE::HEAL) {
			Evlist elist;

			Event ea = make_attack(nullptr, ev.to, c, ev.h.hit, ATYPE::PHYSICAL);
			ea.a.uncounterable = true;
			ea.prev = make_shared<Event>(ev);
			elist.push_back(ea);
			// XXX should be counterable but I'm not getting the option

			if (ev.h.chocolate_cow) {
				// send ranch in milk and chocolate cow to player's "other" deck
				elist.front().apply_fn = bind(&SmacCardSet::ranch_in_milk_chocolate_cow_applyfn, this, _PTARGET, _CARD, ev.c);
			} else {
				Event ed = make_discard(c); // discard the prank
				elist.push_back(ed);
			}
			return elist;
		}
		return Evlist();
	});
	return Evlist();
}

Evlist SmacCardSet::ranch_in_milk_chocolate_cow_applyfn(Player *p, Card *c, Card *chocolate_cow) {
	c->_val_uint16 = 2;
	Evlist evlist;
	Event em1(ETYPE::MOVE);
	em1.c = c;
	em1.from = nullptr;
	em1.to = p;
	em1.m.where = Cardloc::OTHER;
	em1.m.cmove = c;
	evlist.push_back(em1);
	Event em2(ETYPE::MOVE);
	em2.c = c;
	em2.from = nullptr;
	em2.to = p;
	em2.m.where = Cardloc::OTHER;
	em2.m.cmove = chocolate_cow;
	evlist.push_back(em2);
	p->turnstart_fns.emplace_back(c, [=]() {
		Evlist evlist;
		Event e = make_attack(nullptr, p, c, 2, ATYPE::PHYSICAL);
		e.a.uncounterable = true;
		evlist.push_back(e);
		c->_val_uint16--;
		if (c->_val_uint16 == 0) {
			evlist.push_back(make_discard(c));
			evlist.push_back(make_discard(chocolate_cow));
		}
		return evlist;
	});
	return evlist;
}

void SmacCardSet::revenge() {
	_d.push_back(
		(new Card(CardInfo({
			"revenge",
			{
				"whenever someone damages you,",
				"they take the same damage as you",
			},
			{ "what did you say about my mom?" },
			AFF::S | AFF::I,
			CTYPE::ITEM,
		}), PLAY_ITEM(&SmacCardSet::revenge_applyfn, _PTARGET, _CARD))));
}

Evlist SmacCardSet::revenge_applyfn(Player *p, Card *c) {
	_engine->post_damage_fns.emplace_back(c, [=](const Event& ev) {
		assertEqual(ev.etype, ETYPE::ATTACK);
		if (ev.to == p && ev.from != p && ev.from != nullptr && !ev.a.annulled) {
			Event e = make_attack(p, ev.from, c, ev.a.hit, ev.a.atype);
			e.a.uncounterable = true;
			return single_event(e);
		}
		return Evlist();
	});
	return Evlist();
}

void SmacCardSet::rolling_bother() {
	_d.push_back(
		(new Card(CardInfo({
			"rolling bother",
			{
				"all bother cards in your hand",
				"can be played at once doing 2x",
				"the number of bother cards",
				"played minus 1",
			},
			{
				"it isn't the bother that is",
				"annoying... it's the shoes",
			},
			AFF::A,
			CTYPE::ATCK,
			ATYPE::PHYSICAL,
		}), bind(&SmacCardSet::bother_attack, this, _PFROM, _CARD, _ALSO_FLUX)))
		->set_trait(Card::Trait::BOTHER)
	);
}

void SmacCardSet::selective_weakness() {
	_d.push_back(
		(new Card(CardInfo({
			"Selective weakness",
			{
				"Select one type of annoyance.",
				"That type does triple, but other",
				"types do nothing.",
			},
			{
				"6 physical damage... nothing.",
				"2 verbal damage... AUGH!",
			},
			AFF::S | AFF::I,
			CTYPE::ITEM,
		}), PLAY_ITEM(&SmacCardSet::selective_weakness_applyfn, _PTARGET, _CARD))));
}

Evlist SmacCardSet::selective_weakness_applyfn(Player *p, Card *c) {
	ATYPE atype = _engine->select_atype(p);
	p->damage_fns.emplace_back(c, [=](Attack& a) {
		// [Player x]'s selective weakness is ____!
		// TODO: coord->card_had_effect(c);
		if (a.atype & atype)
			a.hit *= 3;
		else if (a.atype & (ATYPE::PHYSICAL | ATYPE::VERBAL | ATYPE::MENTAL))
			a.annulled = true;
	});
	return Evlist();
}

void SmacCardSet::shes_standing_right_behind_you() {
	_d.push_back(
		(new Card(CardInfo({
			"She's standing right behind you",
			{
				"Whoever plays a card with mom in",
				"the title takes 8 mental damage.",
			},
			{ "Your mom... is cool." },
			AFF::S | AFF::M | AFF::A | AFF::C,
			CTYPE::PRNK,
		}), PLAY_PRNK(&SmacCardSet::shes_standing_right_behind_you_applyfn, _CARD))));
}

Evlist SmacCardSet::shes_standing_right_behind_you_applyfn(Card *c) {
	_engine->card_played_fns.emplace_back(c, [=](const Event& ev) {
		if (ev.etype == ETYPE::ATTACK &&
			(ev.c->_trait == Card::Trait::YOUR_MOM ||
			ev.c->_trait == Card::Trait::NO_YOUR_MOM)) {
			Evlist elist;

			Event ea = make_attack(nullptr, ev.from, c, 8, ATYPE::MENTAL);
			ea.a.uncounterable = true;
			ea.prev = make_shared<Event>(ev);
			elist.push_back(ea);

			Event ed = make_discard(c); // discard the prank
			elist.push_back(ed);
			return elist;
		}
		return Evlist();
	});
	return Evlist();
}

void SmacCardSet::spencers_random_rage() {
	_d.push_back(
		(new Card(CardInfo({
			"Spencer's random rage",
			{
				"spin a spinner (or a bottle)",
				"whoever it lands on takes",
				"6 physical damage",
			},
			{ "Yes this card can hurt you too." },
			AFF::S,
			CTYPE::ATCK,
			ATYPE::PHYSICAL,
		}), bind(&SmacCardSet::spencers_random_rage_playfn, this, _PFROM, _CARD))));
}

Event SmacCardSet::spencers_random_rage_playfn(Player *p, Card *c) {
	Player *pchoice = _engine->random_player();
	return make_attack(p, pchoice, c, 6);
}

void SmacCardSet::standing_in_front_of_a_wall() {
	_d.push_back(
		(new Card(CardInfo({
			"Standing in front of a wall",
			{ "4" },
			{ "grrr..." },
			AFF::S | AFF::M | AFF::A | AFF::C | AFF::I,
			CTYPE::CNTR,
			ATYPE::PHYSICAL,
		}), PLAY_CNTR(4))));
}

void SmacCardSet::tag() {
	_d.push_back(
		(new Card(CardInfo({
			"tag",
			{
				"pick someone to be it. when it",
				"gets to their turn, they pick",
				"someone else to be it. whoever's",
				"it must be targeted",
			},
			{ "no tag backs" },
			AFF::S | AFF::M | AFF::A,
			CTYPE::FFLD,
		}), bind(&SmacCardSet::tag_playfn, this, _PFROM, _CARD))));
}

Event SmacCardSet::tag_playfn(Player *p, Card *c) {
	_engine->_currently_tagged = _engine->select_target(p);
	Event e(ETYPE::MOVE);
	e.c = c;
	e.from = p;
	e.to = nullptr;
	e.m.where = Cardloc::FIELD;
	e.m.cmove = c;
	e.apply_fn = bind(&SmacCardSet::tag_applyfn, this, _CARD);
	return e;
}

Evlist SmacCardSet::tag_applyfn(Card *c) {
	_engine->elect_target_fns.emplace_back(c, bind(&SmacCardSet::tag_targetingfn, this, _2));
	_engine->_currently_tagged->removable_turnstart_fns.emplace_back(c, bind(&SmacCardSet::tag_turnstartfn, this, c));
	return Evlist();
}

Player* SmacCardSet::tag_targetingfn(list<Player*> players) {
	return _engine->_currently_tagged;
}

bool SmacCardSet::tag_turnstartfn(Card *c) {
	Player *p = _engine->_currently_tagged;

	// select a new player without interference from our own targeting function
	_engine->elect_target_fns.remove(c);
	_engine->_currently_tagged = _engine->select_target(p);
	_engine->elect_target_fns.emplace_back(c, bind(&SmacCardSet::tag_targetingfn, this, _2));

	if (_engine->_currently_tagged != p) {
		// place turnstart hook on new player
		_engine->_currently_tagged->removable_turnstart_fns.emplace_back(c, bind(&SmacCardSet::tag_turnstartfn, this, c));
		return true; // remove hook from this player
	}
	return false; // keep hook on current player
}

void SmacCardSet::textbook_rap() {
	_d.push_back(
		(new Card(CardInfo({
			"Textbook rap",
			{ "4" },
			{ "\"beatboxing\"" },
			AFF::S | AFF::M,
			CTYPE::ATCK,
			ATYPE::VERBAL,
		}), PLAY_ATCK(4))));
}

void SmacCardSet::the_randomizer() {
	_d.push_back(
		(new Card(CardInfo({
			"the randomizer",
			{
				"all cards must be played on",
				"a random person",
			},
			{ "It's EVIL!" },
			AFF::S | AFF::A | AFF::C,
			CTYPE::FFLD,
		}), PLAY_FFLD(&SmacCardSet::the_randomizer_applyfn, _CARD))));
}

Evlist SmacCardSet::the_randomizer_applyfn(Card *c) {
	_engine->_randomizer = true;
	c->discard_fn([this]() {
		_engine->_randomizer = false;
		return Evlist();
	});
	return Evlist();
}

void SmacCardSet::the_song_that_never_ends() {
	_d.push_back(
		(new Card(CardInfo({
			"The song that never ends",
			{
				"1",
				"Does damage every turn of the",
				"opponent used on.",
			},
			{
				"It just goes on and on",
				"my friends",
			},
			AFF::M | AFF::A | AFF::I,
			CTYPE::ATCK,
			ATYPE::VERBAL,
		}), PLAY_ATCK_WITH_FN(1, &SmacCardSet::the_song_that_never_ends_applyfn, _PTARGET, _CARD))));
}

Evlist SmacCardSet::the_song_that_never_ends_applyfn(Player *p, Card *c) {
	Event e(ETYPE::MOVE);
	e.c = c;
	e.from = nullptr;
	e.to = p;
	e.m.where = Cardloc::OTHER;
	e.m.cmove = c;
	p->turnstart_fns.emplace_back(c, [=]() {
		Event e = make_attack(nullptr, p, c, 1);
		e.a.countered_fn = [=](const Event& ev) {
			return single_event(make_discard(ev.from, nullptr, p, c));
		};
		return single_event(e);
	});
	return single_event(e);
}

void SmacCardSet::the_wedding() {
	_d.push_back(
		(new Card(CardInfo({
			"The Wedding",
			{
				"All Max cards do double damage.",
				"If your name is Max or Hannah,",
				"you heal 5 every turn.",
			},
			{
				"It's starry night themed...",
				"...kinda.",
			},
			AFF::M,
			CTYPE::FFLD,
		}), PLAY_FFLD(&SmacCardSet::the_wedding_applyfn, _CARD))));
}

Evlist SmacCardSet::the_wedding_applyfn(Card *c) {
	_engine->modify_attack_fns.emplace_back(c, [](const Event& ev, AttackModifier& amod) {
		assertEqual(ev.etype, ETYPE::ATTACK);
		if (ev.a.aff & AFF::M)
			amod.multiplier *= 2;
	});
	for (auto& p : _engine->ps) {
		if (p->name == "Max" || p->name == "Hannah") {
			p->turnstart_fns.emplace_back(c, [=]() {
				return single_event(generic_heal(p, c, 5));
			});
		}
	}
	return Evlist();
}

void SmacCardSet::throw_away() {
	_d.push_back(
		(new Card(CardInfo({
			"Throw away",
			{
				"Choose someone",
				"(can be yourself).",
				"They must discard all cards in",
				"their hand and draw new cards.",
			},
			{ "Dang, I missed." },
			AFF::S | AFF::M | AFF::A | AFF::C | AFF::I,
			CTYPE::SPCL,
		}), bind(&SmacCardSet::throw_away_playfn, this, _PFROM, _CARD)))
		->playable_type(CTYPE::ATCK)
	);
}

Event SmacCardSet::throw_away_playfn(Player *p, Card *c) {
	Player *pchoice = _engine->select_target(p);

	Event e(ETYPE::PARENT);
	e.c = c;
	e.from = p;
	e.to = pchoice;
	for (auto& cc : pchoice->hand) {
		e.child_events.push_back(make_discard(p, c, pchoice, cc));
	}

	return e;
}

void SmacCardSet::yeah_right() {
	_d.push_back(
		(new Card(CardInfo({
			"yeah right",
			{ "3" },
			{ "curse you for your tallness" },
			AFF::M,
			CTYPE::CNTR,
			ATYPE::PHYSICAL,
		}), PLAY_CNTR(3))));
}

void SmacCardSet::you_have_no_limbs() {
	_d.push_back(
		(new Card(CardInfo({
			"You have no limbs",
			{
				"does 2 damage per Cody card and",
				"-1 per Spencer card.",
			},
			{ "It's only a flesh wound." },
			AFF::S,
			CTYPE::ATCK,
			ATYPE::VERBAL,
		}), PLAY_AFF_ATCK(-1, 0, 0, 2, 0))));
}

void SmacCardSet::your_mom_2() {
	_d.push_back(
		(new Card(CardInfo({
			"Your mom",
			{ "2" },
			{ "both" },
			AFF::S | AFF::M | AFF::A | AFF::C,
			CTYPE::ATCK,
			ATYPE::VERBAL,
		}), PLAY_ATCK(2)))
		->set_trait(Card::Trait::YOUR_MOM)
	);
}

void SmacCardSet::your_mom_7() {
	_d.push_back(
		(new Card(CardInfo({
			"Your mom",
			{ "2" },
			{ "Your mom is not cool... 7." },
			AFF::S | AFF::M | AFF::A | AFF::C,
			CTYPE::ATCK,
			ATYPE::VERBAL,
		}), PLAY_ATCK(2)))
		->set_trait(Card::Trait::YOUR_MOM)
	);
}

SmacCardSet* SmacCardSet::generate_test_cards() {
	generate_basic_20();
	generate_additional_40();

	return this;
}

SmacCardSet* SmacCardSet::generate_basic_20() {
	_d.reserve(_d.size() + 20);

	air_guitar();
	big_fat_target();
	boom();
	caution_low_ceiling();
	chair_stare();
	deaf();
	extra_arm();
	free_chocolate_milk();
	ignored();
	im_a_superior_being();
	library();
	no_brainer();
	oops_i_dropped_it();
	ranch_in_milk();
	revenge();
	rolling_bother();
	standing_in_front_of_a_wall();
	tag();
	textbook_rap();
	your_mom_2();

	return this;
}

SmacCardSet* SmacCardSet::generate_additional_40() {
	_d.reserve(_d.size() + 40);

	agree();
	amnesia_dust();
	boofy_jacket();
	bother_machine_gun();
	bucket_of_jello();
	chocolate_cow();
	circle_circle();
	condescending();
	cow_hat();
	crud_comix();
	dazzling_gift();
	dumping_area();
	folder_maze();
	emit_smugness();
	extra_leg();
	free_milk();
	free_milk_steal();
	i_know_you_are_but_what_am_i();
	khaos_realm();
	la_la_land();
	long_distance_bother();
	math_teacher_stare();
	meat_shield();
	no_your_mom_1();
	nope();
	oops();
	owned();
	parsons_class();
	percy_grainger();
	pineapple();
	ping_pong();
	selective_weakness();
	shes_standing_right_behind_you();
	spencers_random_rage();
	the_randomizer();
	the_song_that_never_ends();
	throw_away();
	yeah_right();
	you_have_no_limbs();
	your_mom_7();

	return this;
}

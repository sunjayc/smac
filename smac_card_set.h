#ifndef _SMAC_CARD_SET_H
#define _SMAC_CARD_SET_H

#include "cardpack.h"
#include "engine.h"

class SmacCardSet
{
public:
	~SmacCardSet();

	void attach(Engine* engine) { _engine = engine; }

	SmacCardSet* generate_test_cards();
	SmacCardSet* generate_basic_20();
	SmacCardSet* generate_additional_40();

	cardpack get_cards() { return _d; }
	cardpack _play_immediately;

	Event  generic_attack(Player *p, Card *c, int16_t hit);
	Event  generic_counter(Player *p, Card *c, const Event& ev, int16_t hit);
	Event  generic_heal(Player *p, Card *c, int16_t hit);

	Event  affiliate_attack(Player *p, Card *c, int16_t s, int16_t m, int16_t a, int16_t cc, int16_t i);
	Event  bother_attack(Player *p, Card *c, cardpack& also_flux);
	Event  stare_attack(Player *p, Card *c, cardpack& also_flux);

	Event  agree_playfn(Player *p, Card *c, const Event &ev);
	Evlist amnesia_dust_applyfn(Player *pchoice);
	Event  big_fat_target_playfn(Player *p, Card *c);
	Evlist big_fat_target_applyfn(Player *p, Card *c, Player *pchoice);
	Evlist boofy_jacket_applyfn(Player *p, Card *c);
	Event  boom_playfn(Player *p, Card *c);
	Event  bother_machine_gun_playfn(Player *p, Card *c, cardpack& also_flux);
	Evlist bucket_of_jello_applyfn(Card *c);
	Evlist caution_low_ceiling_applyfn(Card *c);
	Event  chocolate_cow_playfn(Player *p, Card *c);
	Evlist chocolate_cow_applyfn(Player *p, Card *c);
	Evlist circle_circle_applyfn(Card *c);
	Event  condescending_playfn(Player *p, Card *c);
	Evlist dazzling_gift_applyfn(Player *p, Card *c, Player *pchoice);
	Evlist deaf_applyfn(Player *p, Card *c);
	Evlist dumping_area_applyfn(Card *c);
	Evlist emit_smugness_applyfn(Player *p, Card *c);
	Evlist extra_arm_applyfn(Player *p, Card *c);
	Evlist extra_leg_applyfn(Player *p, Card *c);
	Evlist folder_maze_applyfn(Player *p, Card *c);
	bool   free_milk_steal_canplayfn(const Event& ev);
	Event  free_milk_steal_playfn(Player *p, Card *c, const Event &ev, cardpack& also_flux);
	Event  i_know_you_are_playfn(Player *p, Card *c, const Event &ev);
	Evlist khaos_realm_applyfn(Card *c);
	Evlist la_la_land_applyfn(Player *p, Card *c);
	Evlist library_applyfn(Card *c);
	Event  meat_shield_playfn(Player *p, Card *c, const Event &ev);
	Event  nope_playfn(Player *p, Card *c, const Event& ev);
	Evlist no_brainer_applyfn(Player *p, Card *c);
	Event  no_your_mom_playfn(Player *p, Card *c, const Event &ev);
	bool   oops_canplaymainfn();
	Event  oops_playfn(Player *p, Card *c);
	bool   oops_i_dropped_it_canplaymainfn();
	Event  oops_i_dropped_it_playfn(Player *p, Card *c);
	Evlist parsons_class_applyfn(Card *c);
	Evlist owned_applyfn(Player *p, Card *c);
	Evlist percy_grainger_applyfn(Player *p, Card *c);
	Event  pineapple_playfn(Player *p, Card *c);
	Evlist ping_pong_applyfn(Card *c);
	Evlist ranch_in_milk_applyfn(Card *c);
	Evlist ranch_in_milk_chocolate_cow_applyfn(Player *p, Card *c, Card *chocolate_cow);
	Evlist revenge_applyfn(Player *p, Card *c);
	Evlist selective_weakness_applyfn(Player *p, Card *c);
	Evlist shes_standing_right_behind_you_applyfn(Card *c);
	Event  spencers_random_rage_playfn(Player *p, Card *c);
	Event  tag_playfn(Player *p, Card *c);
	Evlist tag_applyfn(Card *c);
	Player*tag_targetingfn(std::list<Player*> players);
	bool   tag_turnstartfn(Card *c);
	Evlist the_randomizer_applyfn(Card *c);
	Evlist the_song_that_never_ends_applyfn(Player *p, Card *c);
	Evlist the_wedding_applyfn(Card *c);
	Event  throw_away_playfn(Player *p, Card *c);

	void agree();
	void air_guitar();
	void amnesia_dust();
	void big_fat_target();
	void boofy_jacket();
	void boom();
	void bother_machine_gun();
	void bucket_of_jello();
	void caution_low_ceiling();
	void chair_stare();
	void chocolate_cow();
	void circle_circle();
	void condescending();
	void cow_hat();
	void crud_comix();
	void dazzling_gift();
	void deaf();
	void dumping_area();
	void emit_smugness();
	void extra_arm();
	void extra_leg();
	void folder_maze();
	void free_chocolate_milk();
	void free_milk();
	void free_milk_steal();
	void i_know_you_are_but_what_am_i();
	void ignored();
	void im_a_superior_being();
	void khaos_realm();
	void la_la_land();
	void library();
	void long_distance_bother();
	void math_teacher_stare();
	void meat_shield();
	void no_brainer();
	void no_your_mom_1();
	void nope();
	void oops();
	void oops_i_dropped_it();
	void owned();
	void parsons_class();
	void percy_grainger();
	void pineapple();
	void ping_pong();
	void ranch_in_milk();
	void revenge();
	void rolling_bother();
	void selective_weakness();
	void shes_standing_right_behind_you();
	void spencers_random_rage();
	void standing_in_front_of_a_wall();
	void tag();
	void textbook_rap();
	void the_randomizer();
	void the_song_that_never_ends();
	void the_wedding();
	void throw_away();
	void yeah_right();
	void you_have_no_limbs();
	void your_mom_2();
	void your_mom_7();
private:
	Engine* _engine;
	cardpack _d;
};

#endif // #ifndef _SMAC_CARD_SET_H

#include "network.h"

#include <string>
#include <vector>

#include <WinSock2.h>
#include <WS2tcpip.h>

#include "asserts.h"

#pragma comment(lib, "Ws2_32.lib")

using std::string;
using std::vector;

WinsockInitializer::WinsockInitializer() {
	WSADATA wsaData;
	int res = WSAStartup(MAKEWORD(2,2), &wsaData);
	assertEqual(res, 0);
}

WinsockInitializer::~WinsockInitializer() {
	WSACleanup();
}

WinsockInitializer WindowsSocketBase::_winsockInit;

WindowsSocket::~WindowsSocket() {
	if (!_invalid && _sock != INVALID_SOCKET)
		closesocket(_sock);
}

bool WindowsSocket::connectServer(string hostname, unsigned short port) {
	int res;
	struct addrinfo *result = nullptr, *ptr = nullptr, hints;
	ZeroMemory(&hints, sizeof(hints));
	hints.ai_family = AF_UNSPEC;
	hints.ai_socktype = SOCK_STREAM;
	hints.ai_protocol = IPPROTO_TCP;

	char portstr[10];
	sprintf_s(portstr, "%u", port);
	res = getaddrinfo(hostname.c_str(), portstr, &hints, &result);
	if (res != 0)
		return false;

	_sock = INVALID_SOCKET;

	ptr = result;
	do {
		_sock = socket(ptr->ai_family, ptr->ai_socktype, ptr->ai_protocol);
		if (_sock == INVALID_SOCKET) {
			freeaddrinfo(result);
			return false;
		}

		res = connect(_sock, ptr->ai_addr, ptr->ai_addrlen);
		if (res == SOCKET_ERROR) {
			closesocket(_sock);
			ptr = result->ai_next;
		}
	} while (res == SOCKET_ERROR);
	freeaddrinfo(result);

	_invalid = false;
	_connected = true;
	return true;
}

bool WindowsSocket::read(void* buf, uint32_t n) {
	if (invalid())
		return false;
	int res;

	do {
		res = recv(_sock, static_cast<char*>(buf), n, 0);
		if (res == SOCKET_ERROR || res == 0) {
			_connected = false;
			return false;
		} 
		n -= res;
	} while (n > 0);

	return true;
}

bool WindowsEventSocket::read(void* buf, uint32_t n) {
	return read(buf, n, WSA_INFINITE);
}

bool WindowsEventSocket::read(void* buf, uint32_t n, unsigned long timeout) {
	if (invalid())
		return false;
	int res;

	do {
		res = recv(_sock, static_cast<char*>(buf), n, 0);
		if (res == SOCKET_ERROR || res == 0) {
			if (res == SOCKET_ERROR && WSAGetLastError() == WSAEWOULDBLOCK) {
				res = WSAWaitForMultipleEvents(1, &hEvent, FALSE, timeout, FALSE);
				if (res == WSA_WAIT_FAILED) {
					_connected = false;
					return false;
				} else if (res == WSA_WAIT_TIMEOUT)
					return false; // still valid though
				else
					continue;
			} else {
				_connected = false;
				return false;
			}
		} 
		n -= res;
	} while (n > 0);

	return true;
}

vector<WindowsEventSocket*> WindowsEventSocket::wait_for_read(const vector<WindowsEventSocket*>& wesocks, unsigned long timeout) {
	vector<WindowsEventSocket*> readySocks;
	auto sz = wesocks.size();
	WSAEVENT* arrEvents = new WSAEVENT[sz];
	DWORD i = 0;
	for (auto& wesock : wesocks) {
		arrEvents[i++] = wesock->hEvent;
	}
	DWORD res = WSAWaitForMultipleEvents(sz, arrEvents, FALSE, timeout, FALSE);
	if (res == WSA_WAIT_FAILED || res == WSA_WAIT_TIMEOUT) {
		delete [] arrEvents;
		return readySocks;
	}
	for (i = 0; i < sz; i++) {
		res = WSAWaitForMultipleEvents(1, &arrEvents[i], FALSE, 0, FALSE);
		if (res == WSA_WAIT_EVENT_0) {
			WSANETWORKEVENTS networkEvents;
			WSAEnumNetworkEvents(wesocks[i]->_sock, wesocks[i]->hEvent, &networkEvents);
			if (networkEvents.lNetworkEvents & FD_READ)
				readySocks.push_back(wesocks[i]);
		}
	}
	delete [] arrEvents;
	return readySocks;
}

bool WindowsSocket::write(const void* buf, uint32_t n) {
	if (invalid())
		return false;
	int res;

	do {
		res = send(_sock, static_cast<const char*>(buf), n, 0);
		if (res == SOCKET_ERROR) {
			_connected = false;
			return false;
		}
		n -= res;
	} while (n > 0);

	return true;
}

WindowsServerSocket::WindowsServerSocket(string port) {
	int res;
	struct addrinfo *result = nullptr, hints;
	ZeroMemory(&hints, sizeof(hints));
	hints.ai_family = AF_INET;
	hints.ai_socktype = SOCK_STREAM;
	hints.ai_protocol = IPPROTO_TCP;
	hints.ai_flags = AI_PASSIVE;

	res = getaddrinfo(nullptr, port.c_str(), &hints, &result);
	if (res != 0)
		return;

	_sock = socket(result->ai_family, result->ai_socktype, result->ai_protocol);
	if (_sock == INVALID_SOCKET) {
		freeaddrinfo(result);
		return;
	}

	res = bind(_sock, result->ai_addr, result->ai_addrlen);
	if (res == SOCKET_ERROR) {
		closesocket(_sock);
		freeaddrinfo(result);
		return;
	}
	freeaddrinfo(result);

	if (listen(_sock, SOMAXCONN) == SOCKET_ERROR) {
		closesocket(_sock);
		return;
	}

	_invalid = false;
}

WindowsServerSocket::~WindowsServerSocket() {
	if (!_invalid && _sock != INVALID_SOCKET)
		closesocket(_sock);
}

WindowsEventSocket* WindowsServerSocket::acceptClient() {
	assertFalse(invalid());
	SOCKET clientsock;
	clientsock = accept(_sock, nullptr, nullptr);
	if (clientsock == INVALID_SOCKET) {
		closesocket(_sock);
		return nullptr;
	}
	return new WindowsEventSocket(clientsock);
}

WindowsEventSocket::WindowsEventSocket(SOCKET sock) : WindowsSocket(sock) {
	hEvent = WSACreateEvent();
	WSAEventSelect(_sock, hEvent, FD_READ);
}

WindowsEventSocket::~WindowsEventSocket() {
	WSACloseEvent(hEvent);
}

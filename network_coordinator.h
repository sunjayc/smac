#ifndef _NETWORK_COORDINATOR_H

#include <string>
#include <memory>
#include <vector>

#include "card.h"
#include "coordinator.h"
#include "network.h"

class Card;
class Player;

const uint8_t NETWORK_COORDINATOR_VER = 0;

enum class Message : uint8_t {
	TURN_START,
	NEW_HANDS,
	DREW_CARD,
	CARD_PLAYED,
	CARD_MOVED,
	CARD_REVEALED,
	CARD_DISCARDED,
	MUNDANE_DISCARD,
	ATTEMPT,
	PLAYER_HIT,
	PLAYER_HEALED,
	PLAYER_DEAD,
	TIMER,
	SELECT_CARD,
	SELECT_MULTI_CARDS,
	SELECT_PLAYER,
	SELECT_ATYPE,
	GAME_END,
};

class NetworkCoordinator : public Coordinator
{
public:
	NetworkCoordinator(std::string port);

	/**
	 * For each connecting player:
	 *   sendVer
	 *   get name (tbd)
	 * After all players connected:
	 *   for each connection:
	 *     send player index
	 *     send num players
	 *     send player names and health in order
	 *   for each connection:
	 *     send deck size
	 *     send card info in order
	 */
	virtual uint8_t _retrieve_players(std::vector<std::shared_ptr<Player>>& ps, Playfield *f) override;

	/**
	 * For each connection:
	 *   send message TURN_START
	 *   send index of curp
	 **/
	virtual void turn_start(const Player *curp) override;

	virtual void new_hands(const std::list<Player*>& players) override;
	virtual void drew_card(const Player *p, const Card *c) override;
	virtual void card_moved(const Player *from, const Player *to, const Card *cfrom, const Card *cto, Cardloc where) override;
	virtual void card_played(const Player *from, const Player *to, const Card *c) override;
	virtual void card_revealed(const Card *c, Cardloc where) override;
	virtual void card_discarded(const Event& ev) override;
	virtual void mundane_discard(const Card *c) override;

	virtual void attempt(const Event& ev) override;

	virtual void player_hit(const Player *p, int16_t hit) override;
	virtual void player_healed(const Player *p, int16_t hit) override;
	virtual void player_dead(const Player *p) override;

	virtual void game_end() override;

	virtual void wait(uint32_t id) override;
protected:
	void handle_read(Player *p);

	/**
	 * send message SELECT_CARD
	 * send passable
	 * send cardpack q.cards
	 * read Card response
	 **/
	virtual void select_card(const query& q) override;
	void get_select_card(NetworkPacker& conn, query& q);

	/**
	 * send message SELECT_PLAYER
	 * send passable
	 * send players q.players
	 * read Player response
	 **/
	virtual void select_player(const query& q) override;
	void get_select_player(NetworkPacker& conn, query& q);

	virtual void select_multi_cards(const query& q) override;
	void get_select_multi_cards(NetworkPacker& conn, query& q);

	virtual void select_atype(const query& q) override;
	void get_select_atype(NetworkPacker& conn, query& q);

	virtual void timer(const query& q) override;
	void get_timer(Player *p, query& q);

	std::shared_ptr<ServerSocket> _ssock;
	std::vector<NetworkPacker> _conns;

	// messages are encoded as (uint8_t msg, uint32_t id)
	bool send(NetworkPacker& conn, const Message& msg, const uint32_t id = UINT32_MAX);
	bool recv(NetworkPacker& conn, Message& msg, uint32_t& id);

	// uint8s and containers of uint8s
	bool send(NetworkPacker& conn, const AFF& aff);
	bool send(NetworkPacker& conn, const aff_t& aff);
	bool send(NetworkPacker& conn, const CTYPE& ctype);
	bool send(NetworkPacker& conn, const ctype_t& ctype);
	bool send(NetworkPacker& conn, const ATYPE& atype);
	bool send(NetworkPacker& conn, const atype_t& atype);
	bool recv(NetworkPacker& conn, ATYPE& atype);

	// cards are encoded as (uint16_t index)
	bool send_blank_card(NetworkPacker& conn);
	bool send(NetworkPacker& conn, const Card* c);
	bool recv(NetworkPacker& conn, Card* &c);

	// cardpacks are encoded as (uint8_t size, <Card*>[size])
	bool send(NetworkPacker& conn, const cardpack& cards);
	bool recv(NetworkPacker& conn, cardpack& cards);

	// players are encoded as (uint8_t index)
	bool send(NetworkPacker& conn, const Player* p);
	bool recv(NetworkPacker& conn, Player* &p);

	// player lists are encoded as (uint8_t size, <Player*>[size])
	bool send(NetworkPacker& conn, const std::list<Player*>& players);

	cardpack _d;
};

struct ICard;
typedef std::vector<std::shared_ptr<ICard>> icardpack;
struct ICard {
	uint16_t index;
	CardInfo ci;
	icardpack *loc;

	static std::shared_ptr<ICard> blank_card() {
		static std::shared_ptr<ICard> blank = []() {
			auto blank = std::make_shared<ICard>();
			blank->index = UINT16_MAX;
			blank->ci.name = "???";
			blank->loc = nullptr;
			return blank;
		}();
		return blank;
	}
	bool blank() const { return index == UINT16_MAX; }
};

struct IPlayer {
	std::string name;
	uint8_t index;

	int16_t health;
	icardpack hand;
	icardpack items;
	icardpack allies;
	icardpack other;
};

struct IPlayfield {
	icardpack fields;
	icardpack pranks;
	icardpack discard;
};

class NetworkCoordinatorClient {
public:
	NetworkCoordinatorClient(std::shared_ptr<Socket> sock) :
		_conn(sock),
		_f(new IPlayfield)
	{ }

	bool init() {
		if (!_conn.getVer(0))
			return false;
		std::cout << "Name: " << std::endl;
		std::string name;
		getline(std::cin, name);
		return _conn.write(name);
	}

	bool getPlayers();

	bool handle_message();
private:
	bool handle_input(const std::string& input, int& val);
	bool handle_input(const std::string& input, std::vector<int>& vals);

	void turn_start();
	void new_hands();
	void drew_card();
	void card_played();
	void card_moved();
	void card_revealed();
	void card_discarded();
	void mundane_discard();
	void attempt();
	void player_hit();
	void player_healed();
	void player_dead();
	void game_end();

	void print_players();
	void print_hand();
	void print_items();
	void print_field();
	void print_pranks();
	void show_card(uint16_t index);

	void timer(uint32_t id);
	void select_card(uint32_t id);
	void select_multi_cards(uint32_t id);
	void select_player(uint32_t id);
	void select_atype(uint32_t id);

	// messages are encoded as (uint8_t msg, uint32_t id)
	bool send(const Message& msg, const uint32_t id);
	bool recv(Message& msg, uint32_t& id);

	// uint8s and containers of uint8s
	bool recv(AFF& aff);
	bool recv(aff_t& aff);
	bool recv(CTYPE& ctype);
	bool recv(ctype_t& ctype);
	bool send(const ATYPE& atype);
	bool recv(ATYPE& atype);
	bool recv(atype_t& atype);

	// cards are encoded as (uint16_t index)
	bool send_null_card();
	bool send(const std::shared_ptr<ICard> ic);
	bool recv(std::shared_ptr<ICard>& ic);

	// cardpacks are encoded as (uint8_t size, <CardInfo*>[size])
	bool send(const icardpack& cards);
	bool recv(icardpack& icards);

	// players are encoded as (uint8_t index)
	bool send_null_player();
	bool send(const std::shared_ptr<IPlayer> p);
	bool recv(std::shared_ptr<IPlayer>& p);

	// player lists are encoded as (uint8_t size, <IPlayer*>[size])
	bool recv(std::vector<std::shared_ptr<IPlayer>>& players);

	NetworkPacker _conn;
	std::vector<std::shared_ptr<ICard>> _d;
	std::shared_ptr<IPlayfield> _f;
	std::vector<std::shared_ptr<IPlayer>> _ps;
	std::shared_ptr<IPlayer> _p;
};

#endif // #ifndef _NETWORK_COORDINATOR_H

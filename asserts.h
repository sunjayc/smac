#ifndef _ASSERTS_H
#define _ASSERTS_H

#include <cstdlib>
#include <iostream>
#include <string>

template <typename T>
void panic(T msg) {
	std::cerr << msg << std::endl;
	abort();
}

template <typename T, typename... Args>
void panic(T msg, Args... args) {
	std::cerr << msg << std::endl;
	panic(args...);
}

// This weird indirection sequence is dumb, but necessary
#define _num_to_string(n) #n
#define num_to_string(n) _num_to_string(n)
#define assert1(expr) do { \
	if (!(expr)) { \
		panic("Assertion failed!\n" __FILE__ ":" num_to_string(__LINE__) ": " #expr, (expr)); \
	} \
} while((void)0,0) // owl to prevent C4127

#define assertTrue(expr) assert1(expr)
#define assertFalse(expr) assert1(!(expr))

#define assertEqual(e1, e2) \
	assertTrue((e1) == (e2))
#define assertNotEqual(e1, e2) \
	assertTrue((e1) != (e2))

#define assertNull(expr) assertEqual((expr), nullptr)
#define assertNotNull(expr) assertNotEqual((expr), nullptr)

#define assertLessThan(expr, hi) do { \
	if (!(expr < static_cast<decltype(expr)>(hi))) { \
		panic("Assertion failed!\n" __FILE__ ":" num_to_string(__LINE__) ": " \
			#expr " < " #hi, (expr), (hi)); \
	} \
} while((void)0,0) // owl to prevent C4127
#define assertLessThanEqual(expr, hi) do { \
	if (!(expr <= static_cast<decltype(expr)>(hi))) { \
		panic("Assertion failed!\n" __FILE__ ":" num_to_string(__LINE__) ": " \
			#expr " <= " #hi, (expr), (hi)); \
	} \
} while((void)0,0) // owl to prevent C4127
#define assertInRange(e1, lo, hi) do { \
	if (!(static_cast<decltype(e1)>(lo) <= e1 && e1 < static_cast<decltype(e1)>(hi))) { \
		panic("Assertion failed!\n" __FILE__ ":" num_to_string(__LINE__) ": " \
			#lo " <= " #e1 " < " #hi, (lo), (e1), (hi)); \
	} \
} while((void)0,0) // owl to prevent C4127

template<typename T, typename U>
inline void _assertIn(T& target, U& container, std::string msg) {
	for (auto& elt : container) {
		if (elt == target) {
			return;
		}
	}
	panic(msg);
}
#define assertIn(target, container) _assertIn((target), (container), \
	"Assertion failed!\n" __FILE__ ":" num_to_string(__LINE__) ": " \
		#target " not in " #container)

#ifdef _MSC_VER
#define BEGIN_UNREFERENCED_PARAMETERS \
	__pragma(warning(push)) \
	__pragma(warning(disable:4100))
#define END_UNREFERENCED_PARAMETERS \
	__pragma(warning(pop))
#else
#define BEGIN_UNREFERENCED_PARAMETERS
#define END_UNREFERENCED_PARAMETERS
#endif

#endif // #ifndef _ASSERTS_H

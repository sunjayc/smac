#ifndef _ENGINE_H
#define _ENGINE_H

#include <list>
#include <random>

#include "card.h"
#include "coordinator.h"
#include "event.h"
#include "play_structures.h"

typedef Coordinator::future<Evlist> Evlistf;

const Event EMPTY_EVENT = { ETYPE::NONE };

class SmacCardSet;

class Engine
{
public:
	typedef std::function<Evlist(const Event&)> card_played_fn_t;
	typedef std::function<bool(Card*)> playability_fn_t;
	typedef std::function<Evlist(const Event&)> last_minute_fn_t;
	typedef std::function<void(const Event&, AttackModifier&)> modify_attack_fn_t;
	typedef std::function<Evlist(const Event&)> after_card_played_fn_t;
	typedef std::function<Evlist(const Event&)> post_damage_fn_t;
	typedef std::function<void()> before_turn_start_fn_t;
	typedef std::function<Player*(const Player*, std::list<Player*>)> elect_target_fn_t;
	typedef std::function<void(const Player*,std::list<Player*>&)> remove_target_fn_t;

	Engine(Playfield *f, Coordinator *coord, SmacCardSet *d);
	void init();

	void gameloop();

private:
	friend class SmacCardSet;
	friend class Player;

	hooklist<before_turn_start_fn_t> before_turn_start_fns; // e.g. Circle circle
	void resync_hands(const std::list<Player*>& ps); // for Circle circle

	void attempt_and_apply(Evlist elist);
	Evlistf attempt(Event& e);
	Evlist apply(const Event& e);
	hooklist<last_minute_fn_t> last_minute_fns; // e.g. Ranch in Milk, Bucket of jello
	hooklist<post_damage_fn_t> post_damage_fns; // e.g. Percy Grainger, revenge
	Evlist adjust_params(Player *p, const Parameter& param);

	Evlist play_card(Player *p, Card *c, const Event& prev);
	Event play_card_event(Player *p, Card *c, const Event& prev);
	Evlist play_card_react(const Event& ev);
	void modify_attacks(Event& ev);
	hooklist<modify_attack_fn_t> modify_attack_fns; // e.g. verbal attacks x2 or all attacks +1
	hooklist<card_played_fn_t> card_played_fns; // can replace event, e.g. pranks
	hooklist<after_card_played_fn_t> after_card_played_fns; // adds events, e.g. emit smugness
	Evlist discard_card(Card *c);

	Evlist select_and_play(Player *p, const cardpack& cards, const Event& ev);
	Evlistf future_select_and_play(Player *p, const cardpack& cards, const Event& ev);
	Card* select_card(const Player *p, const cardpack& cards);
	cardpack select_multi_cards(const Player *p, const cardpack& ccards);

	Event handle_response(const Event& ev);

	Player* select_target(const Player *p);
	hooklist<elect_target_fn_t> elect_target_fns; // e.g. tag, big fat target
	hooklist<remove_target_fn_t> remove_target_fns; // e.g. birthdays
	Player* random_player();

	ATYPE select_atype(const Player *p);

	bool main_playable(Player *p, Card *c);
	hooklist<playability_fn_t> playability_fns;

	bool counterable(Player *p, const Event& ev);

	bool everyone_dead();

	Playfield *f;
	Coordinator *coord;
	SmacCardSet *d;
	std::vector<std::shared_ptr<Player>> all_ps;
	std::list<Player*> ps;
	std::vector<Player*> dead_ps;

	std::mt19937_64 _rng;

	bool _running;

	// card specific things
	// for field traits, remember to add a La La Land exception if necessary!
	// XXX roll the bools into Event::Parameter so that, say, multiple randomizers don't turn each other off
	bool _khaos_realm = false;
	bool _ping_pong = false;
	bool _randomizer = false;
	Player *_currently_tagged = nullptr;
	hooklist<Parameter> _param_adjustments;

	friend std::vector<Player*> test_engine_get_ps(Engine& e);
};

#endif // #ifndef _ENGINE_H

Air guitar DONE
3
I'm using the right fingerings.
counter [mental]
M

big fat target DONE
select a person. they must be targeted except on their turn. this lasts for 1 round
don't go near an archery range
special
AC!

BOOM DONE
5 applies to all players including the user
Pretty colors...
physical [attack]
S

Caution: low ceiling DONE
max cards cannot be played unless the are also alex cards
doncha wish your girlfriend was short like me?
field
A

Chair Stare DONE
2 All stare cards in your hand may be played.
Must... focus... on... chair...
attack [mental]
M
S: Do you have the option to *not* play all the stare cards in your hand? M: Yes.

Deaf DONE [but not deafness effect]
verbal damage does not effect you but you can't reaply to what anyone says unless you pretend to mishear them
WHAT? TRUCK BLUE?
item
SMAC!
S: Is it cool if I just make this turn off all messages sent to the player? M: If you want to. I don't really care.

extra arm DONE
you can now have one extra card in your hand. when this is discarded, you may only have the normal amount of cards in your hand
that one guy has like... 4
item
!

Free Chocolate Milk DONE
4
It's free, chocolate, and milk!
heal
AC!

I'm a superior being DONE
3
inferior non-elf...
attack [mental]
A

ignore'd DONE
4
Whatever...
counter [verbal]
AC
S: Is there an exclamation mark in the title? M: doesn't look like it. S: You sure? It looks like there's one in the  scan. M: Can't tell, don't really care.

Library DONE
verbal cards cannot be played
SHHHHHHHHHHHHHHH...
field
SMAC!

No brainer DONE
you have no mind! mental attacks do nothing to you. lose this card if you act smart.
duh... mental whats?
item
SMAC!
S: How to implement? I haven't experimented much with programming mind-reading hardware M: Do nothing at all. Do this for all similar  cards. S: Can it do something else instead? Like reverse all the arrow keys? M: I don't care. 

Oops I dropped it DONE
One of your opponents must discard an item of your choice.
uh... sorry
special
S
M: I made this card yesterday morning.

Ranch in Milk DONE
?
Is that a cruton?
prank
SM
M: Can't freaking read this one.  Just... you know how it works. S: The description is going to read "HARGGLARHARLABHGAGLAHRAB" except it's going to take up the entire space M: I'm okay with this. S: Wait, how does this one work again? M: If someone plays a heal, instead of healing that much, they take that much physical damage.

revenge DONE
whenever someone damages you, they take the same damage as you
what did you say about my mom?
item
S!

rolling bother DONE
all bother cards in your hand can be played at once doing 2x the number of bother cards played minus 1
it isn't the bother that is annoying... it's the shoes
physical [attack]
A
S: Do you have the option to *not* play all the bother cards in your hand? M: Yes.

Standing in front of a wall DONE
4
grrr...
physical [counter]
SMAC!
M: Newish card.  You don't have it scanned in.

tag DONE
pick someone to be it. when it gets to their turn, they pick someone else to be it. whoever's it must be targeted
no tag backs
field
SMA

Textbook rap DONE
4
(beatboxing)
attack [verbal]
SM

Your mom DONE
2
both
attack [verbal]
SMAC






"Dazzling" Gift DONE
3 The opponent's active item is discarded
What is that? A roasted cherry?
physical [attack]
A
M: Since I made a special that gets rid of opponents' items, there is no need for the whole "works even if countered" thing. S: So... if it gets countered then the opponent's active item is not discarded then? Also, which item is the active item? M: I wrote down what the card said, not how it works, so ignore "active" item.  How this will work is that the attacker will choose one item to be discarded.  If it's countered, nothing happens. S: Ok, that makes sense.

Agree DONE
Attack's damage +1
Screw you!  If you want!
counter [mental]
SC

Amnesia dust DONE
3 The opponent loses their next turn
You are Santa.
attack [mental]
A

Boofy jacket DONE [but not boofy effect]
Physical damage does nothing, but you have to hold your arms out
I'm not fat, I'm... boofy
item
C
M: ignore the holding arms up, acting dumb, saying 'd, etc. stuff S: But, but...! What's it gonna' do then? M: I don't understand the question.  Here's how it will work.  If you play boofy jacket, you will be impervious to physical.  Tada.  In other words, pretend the part of the card that makes the player do something irl doesn't exist. S: But then that jacket ain't boofy no more M: It still protects you from physicals. 

Bother machine gun DONE
all bother cards in your hand, the discard pile, and everyone elses hands get played on one person of your choice
long distance rolling "bother"
physical [attack]
M

Bucket of jello DONE
Whoever changes the field takes 5 physical damage
The bucket hurts more than the jello.
prank
SM

Chocolate Cow DONE
Heals 2 every turn of the user. Starts when this card is used. Lasts 3 heals.
My grandma claims to have one.
heal
AC!

Circle circle DONE
rotate hands counterclockwise every turn
rectangle, rectangle, rectangle...
field
C

condescending DONE
3 4 if opponent has 3 or more Alex cards
high five!
attack [mental]
M

Cow hat DONE
4
It comes with built-in cup holders!
counter [mental]
!

Crud Comix DONE
2 damage per Spencer card and -1 per Cody card in the targets hand
I... HATE... STICK... SONIC!!!
attack [mental]
C

dumping area DONE
physical cards can't be played
dumping area! you smell like a crapping area!
field
SMC

emit smugness DONE
whenever someone plays a card on you, they take 1 uncounterable mental dammage (doesn't work in Parson's class)
with a picture like that who needs flavor text?
item
A

extra leg DONE
You may have one more item than the typical amount
Attack of the tripods
free item
!

Folder maze DONE
Deals 1 uncounterable mental damage to any opponent on your turn.
The folder lied to you.
item
MAC

Free Milk DONE
3
Dibs on free milk!
heal
AC!

Free milk steal DONE
If someone else plays a heal play this cand and all effects go to you.
Wait... you can just do that?
special
!
M: Can't read flavor text. S: Best guess? M: I'll just make some up.

I know you are but what am I DONE
4 after being played this goes into the next person's hand.
No you're mom.
counter [verbal]
A

Khaos Realm DONE
There can be any amount of field cards in play.
Don't you mean "oink"?
field
C

La La Land DONE
The cards that you play are not affected by the field (ping pong excluded)
lalalalalala...
item
SMAC!

long distance bother DONE
all bother cards in your hand can be played at once doing 2x the number of bother cards played minus 1
long distance calls cost 0.50$, long distance bothers...
physical [attack]
M

Math teacher Stare DONE
5 All stare cards in your hand may be played
ooh. Creepy.
attack [mental]
S

meat sheild DONE
play after you get attacked but before you calculate damage.  Someone else takes the damage and all effects
that boofy jacket is useful
physical [counter]
AC

No your mom DONE
3 4 if it counters your mom
No your mom, no your mom...
counter [verbal]
SMAC

NOPE. DONE
special
SMAC!
no.

Oops DONE
Destroys the current field card
Ooh... shiny button
special
SMAC!

owne'd DONE
All attacks do +1 but you have to say all attacks with a d on the end.
Your mom'd!!!
item
MAC!
M: This applies to counters too, but I was too dumb to say that in 8th grade.

Parsons' class DONE
mental cards can't be played
be self-directed
field
SMAC

Percy Grainger DONE
whenever you are hurted two times in a row by the same type of damage, you heal 5
That's a lot of milk...
item
SM
M: I'm changing the title of the card, but I think the flavor text is pretty funny as is. S: ... 

Pineapple! DONE
4 counters do no damage
It scares the jeepers out of MC.
attack [verbal]
!

Ping pong DONE
counters can be countered
No your mom, no your mom...
field
MAC

Selective weakness DONE
Select one type of annoyance.  That type does triple, but other types do nothing.
6 physical damage... nothing. 2 verbal damage... AUGH!
item
S!

She's staning behind you DONE
Whoever plays a card with mom in the title takes 8 mental damage.
Your mom... is cool.
prank
SMAC

Spencer's random rage DONE
spin a spinner (or a bottle) whoever it lands on takes 6 physical damage
Yes this card can hurt you too.
physical [attack]
S

the randomizer DONE
all cards must be played on a random person
It's EVIL!
field
SAC
M: Anything targetable is random

The song that never ends DONE
1 Does damage every turn of the opponent used on.
It just goes on and on my friends.
attack [verbal]
MA!

Throw away DONE
Choose someone (can be yourself).  They must discard all cards in their hand and draw new cards.
Dang, I missed.
special
SMAC!

yeah right DONE
3
curse you for your tallness
physical [counter]
M

You have no limbs DONE
does 2 damage per Cody card and -1 per Spencer card.
It's only a flesh wound.
attack [verbal]
S

Your Mom DONE
2
Your mom is not cool... 7.
attack [verbal]
SMAC





Bako
4 each point must be countered seperately
Bako. Bako. Bako. Bako.
attack [verbal]
MC

Ian's B-day
Play this as soon as you get it.  You heal 1 per ! card and no one can target you for one round.
Happy birthday...?
special
!

The fifth Your mom!
2
There's five of them!
attack [verbal]
SMAC

pummeling induced pummeling
3 4 if used on cow induced pummeling
Why's my arm so long?
counter [physical]
!

Mountain Dew
Physical cards do x2
Cupcake is a compound word!
item
SMAC!

Alex's B-day
play this card as soon as you get it. you heal 1 per Alex card in your hand and no one can target you for 1 round.
I feel short now - Maxwell
special
A

Army of Moms
all cards with "your mom" in the title do x3 and can be stacked
Moms with mustaches
field
SMAC
S: Does "no your mom" stack with "your mom"? Or do they stack separately? M: separately 

Cow induced pummeling
4
Cow. Ahhhh
attack [physical]
S

Chuck Norris' legs
Physical attacks do x2. you may have as many chuck norris items as you want
Law order
item
M: Obviously this means all physical damage, but Alex just wrote attack, because it was 8th grade.

Everyone's Special
When someone plays a special, everyone gets the effect
in their own special way
prank
SMAC!

Milk Disease
gives +2 to all people playing. free
Is that contagious?
heal
AC!

Generic Mom
2
no flavor text
attack [verbal]
SMAC

Peanut butter
1 you can't play verbals until this is countered
You mean it's not orange?
attack [physical]
MA
M: on a few of these I added affiliations based on who would be likely to do that thing.

Megaphone
Verbal cards do double damage.
UR MOM!
item
M

Flawless Robot Clone
Trigger - an ally or enemy is played. You may chose a new target
IUN UNIT LOVES KITTEN
prank
SMAC!

Dr Pepper
Heals 1 per Max card in your hand
non-scary 23!
heal
M

"bother"
all bother cards in you hand can be played at once, doing 2x the number of bother cards played minus 1
that isn't a bother, it's a nice right hook
attack [physical]
!

Max & Cody's b-day
play the card as soon as you get it. you heal 1 per M&C card. no one can taget you for one round
look how they spelled our names
special
MC

Not technically in SMAC
1 damage per Ian card
The ! is an upside down "i"
attack [mental]
SMAC

Eli
you take one verbal damage every turn. 5 health. mental do not hurt him
You're short. You're fat.
enemy

Prank War
Choose any opponent. Now you can no longer attack/counter them, but they can no longer attack/counter you. If one of you has a heal, you may heal the other person with it. (You cannot divide heals)
What's in my milk?!?
item
SMAC

hypnotic stare
2 All stare cards in you hand may be played.
You will buy me some licorice.
attack [mental]
!

Mr Parsons
whenever you play an attack you take 1 mental damage 6HP
Change gears quickly
enemy

no your mom
3 4 if you counter your mom
it's your not you're
counter [verbal]
SMAC

bother
does 2x the number of bother cards played minus 1
botherbotherbotherbother
attack [physical]
SMAC!
Re-incarnation of long lost card

spelling mock
does 2 for every Alex cards in the opponents hand.
Isn't that greet?
attack [mental]
MC!

Cheryl
all your targetable are random. Health -1. also dies with specific Cody cards 
I'm not goth, I'm emo!
emo enemy
M: Specific Cody card means having an affiliation of C and no one else.  The player doesn't even have to play it on Cheryl specifically.  There are only 4 of them anyways.

Brain Hat
Mental damage does x2
Does that cover his eyes?
item
A!

Chuck Norris' stomach
physical damage doesn't hurt you. you may have as many chuck norris cards as you want
food... GET EATEN
item

Peanut Stare
2 All stare cards in your hand may be played
Ow my peanuttude!
attack [mental]
S

Sunjay
play this card on yourself.  Whenever you would take damage, Sunjay does it for you.  Everyone has to target you until you lose this.
standing up for myself would take effort
ally

Chuck Norris' face
verbal and stare cards do +3. you may have as many chuck norris cards as you want
Chuck Norris beat the sun in a staring contest.
item

I'm misinterpereting that
4 only counters verbals
Yeah it's too big.
counter [mental]
C

Chuck Norris' arms
if anyone uses a verbal card on you, the take 4 damage. you may have as many chuck nottis cards as you want
wow, those look buff
item

Cheap copout
3
Insert flavor text here.
counter [mental]
SMAC!

Steve One
verb. att.s do +2 Phys. and Ment. att.s do +1.  Use a pencil as a spinner. if it points to you, discard Steve 1
I'm a caaard? Awwww...
Ally
M

Stop KFCing!
3 only counters physical
stop kickindafrickinchickin
counter [verbal]
SMAC!

Your Mom
2
well, maxwell's mom actually
attack [verbal]
SMAC

Kick in the leg
4 only counters mentals
Wait... what just happened?
counter [physical]
!
New card

Spencer's B-day
play this card as soon as you get it. you heal 1 per Spencer card no on can target you for one round
Drooling noises...
special
S


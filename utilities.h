#ifndef _UTILITIES_H
#define _UTILITIES_H

#include <algorithm>

template<typename C, typename T>
inline auto find(C& c, const T& val) -> decltype(c.begin()) {
	return std::find(c.begin(), c.end(), val);
}

template<typename C, typename P>
inline auto find_if(C& c, const P& pred) -> decltype(c.begin()) {
	return std::find_if(c.begin(), c.end(), pred);
}

template<typename C, typename T>
inline bool found(const C& c, const T& val) {
	for (const auto& el : c) {
		if (el == val)
			return true;
	}
	return false;
}

template<typename C, typename T, typename P>
inline bool found(const C& c, const T& val, const P& pred) {
	for (const auto& el : c) {
		if (pred(el) == val)
			return true;
	}
	return false;
}

#endif // #ifndef _UTILITIES_H

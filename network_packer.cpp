#include "network.h"

#ifdef __linux__
#include <arpa/inet.h>
#endif // __linux__

#include "asserts.h"
#include "network_coordinator.h" // for enum class Message

static const uint8_t PACKER_VER = 0;

#define NETBUG 0

#if NETBUG
#include <iostream>
#define DOUT(expr) std::cout << " - " << expr << std::endl
#else
#define DOUT(expr)
#endif

bool NetworkPacker::sendVer(uint8_t ver) {
	uint8_t cpver, ccver;

	// make sure the packers are the same version
	_sock->write(&PACKER_VER, 1);
	_sock->read(&cpver, 1);
	if (cpver != PACKER_VER)
		return false;

	// make sure the packer's client is the same version
	_sock->write(&ver, 1);
	_sock->read(&ccver, 1);

	DOUT( "sendVer:   us: (" << static_cast<int>(PACKER_VER) << "," << static_cast<int>(ver) << ")" );
	DOUT( "sendVer: them: (" << static_cast<int>(cpver) << "," << static_cast<int>(ccver) << ")" );

	return ccver == ver;
}

bool NetworkPacker::getVer(uint8_t ver) {
	uint8_t spver, scver;

	// make sure the packers are the same version
	_sock->read(&spver, 1);
	_sock->write(&PACKER_VER, 1);
	if (spver != PACKER_VER)
		return false;

	// make sure the packer's client is the same version
	_sock->read(&scver, 1);
	_sock->write(&ver, 1);

	DOUT( "sendVer: them: (" << static_cast<int>(spver) << "," << static_cast<int>(scver) << ")" );
	DOUT( "sendVer:   us: (" << static_cast<int>(PACKER_VER) << "," << static_cast<int>(ver) << ")" );

	return scver == ver;
}

bool NetworkPacker::read(std::string& str) {
	uint8_t sz;
	if (!_sock->read(&sz, 1))
		return false;

	char s[256];
	if (sz > 0) {
		if (!_sock->read(s, sz))
			return false;
	}
	s[sz] = '\0';

	str = s;
	DOUT( "read str: " << str );
	return true;
}

bool NetworkPacker::write(const std::string& str) {
	assertLessThan(str.length(), 256);
	uint8_t sz = static_cast<uint8_t>(str.length());
	if (!_sock->write(&sz, 1))
		return false;

	if (sz > 0) {
		if (!_sock->write(str.c_str(), sz))
			return false;
	}

	DOUT( "write str: " << str );
	return true;
}

bool NetworkPacker::read(uint8_t& val) {
	bool res = _sock->read(&val, 1);
	DOUT( "read uint8: " << static_cast<int>(val) );
	return res;
}

bool NetworkPacker::write(const uint8_t val) {
	DOUT( "write uint8: " << static_cast<int>(val) );
	return _sock->write(&val, 1);
}

bool NetworkPacker::read(bool& val) {
	bool res = _sock->read(&val, 1);
	DOUT( "read bool: " << static_cast<int>(val) );
	return res;
}

bool NetworkPacker::write(const bool val) {
	DOUT( "write bool: " << static_cast<int>(val) );
	return _sock->write(&val, 1);
}

bool NetworkPacker::read(int16_t& val) {
	int16_t nval;
	if (!_sock->read(&nval, 2))
		return false;
	val = ntohs(nval);
	DOUT( "read int16: " << val );
	return true;
}

bool NetworkPacker::write(const int16_t val) {
	DOUT( "write int16: " << val );
	int16_t nval = htons(val);
	return _sock->write(&nval, 2);
}

bool NetworkPacker::read(uint16_t& val) {
	uint16_t nval;
	if (!_sock->read(&nval, 2))
		return false;
	val = ntohs(nval);
	DOUT( "read uint16: " << val );
	return true;
}

bool NetworkPacker::write(const uint16_t val) {
	DOUT( "write uint16: " << val );
	uint16_t nval = htons(val);
	return _sock->write(&nval, 2);
}

bool NetworkPacker::read(uint32_t& val) {
	uint32_t nval;
	if (!_sock->read(&nval, 4))
		return false;
	val = ntohl(nval);
	DOUT( "read uin32: " << val );
	return true;
}

bool NetworkPacker::write(const uint32_t val) {
	DOUT( "write uint32: " << val );
	uint32_t nval = htonl(val);
	return _sock->write(&nval, 4);
}

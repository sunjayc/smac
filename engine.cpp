#include "engine.h"

#include <algorithm>
#include <list>
#include <random>
#include <vector>

#include "asserts.h"
#include "card.h"
#include "play_structures.h"
#include "smac_card_set.h"
#include "utilities.h"

using std::list;
using std::make_shared;
using std::shared_ptr;
using std::vector;

Engine::Engine(Playfield *f, Coordinator *coord, SmacCardSet *d) :
	before_turn_start_fns(f),
	last_minute_fns(f),
	post_damage_fns(f),
	modify_attack_fns(f),
	card_played_fns(f),
	after_card_played_fns(f),
	elect_target_fns(f),
	remove_target_fns(f),
	playability_fns(f),
	f(f),
	coord(coord),
	d(d),
	_rng(std::random_device()()),
	_running(true),
	_param_adjustments(f)
{
	coord->attach_engine(this);
	d->attach(this);
}

void Engine::init() {
	f->set_cards(d->get_cards());
	std::shuffle(f->draw.begin(), f->draw.end(), _rng);
	coord->retrieve_players(all_ps, f);
	for (auto& p : all_ps) {
		p->attach(this);
		ps.push_back(p.get());
		p->draw();
	}
}

void Engine::resync_hands(const list<Player*>& ps) {
	coord->new_hands(ps);
}

static inline void next_not_dead(vector<shared_ptr<Player>>& all_ps, decltype(all_ps.begin())& curp) {
	// Next player's turn
	do {
		curp++;
		if (curp == all_ps.end())
			curp = all_ps.begin();
	} while ((*curp)->dead());
}

void Engine::gameloop() {
	for (auto& c : d->_play_immediately) {
		Evlist evlist = play_card(ps.front(), c, EMPTY_EVENT);
		attempt_and_apply(evlist);
	}
	auto curp = all_ps.begin();
	for (; _running; next_not_dead(all_ps, curp)) {
		Player *p = curp->get();
		Evlist elist;

		// Skip p's turn if necessary
		if (p->skip) {
			p->skip--;
			continue;
		}

		for (auto& pair : before_turn_start_fns) {
			auto& fn = pair.second;
			fn();
		}

		coord->turn_start(p);

		p->removable_turnstart_fns.remove_if([](std::pair<Card*,std::function<bool()>>& pair) {
			auto& fn = pair.second;
			return fn();
		});

		for (auto& pair : p->turnstart_fns) {
			auto& fn = pair.second;
			elist.append(fn());
		}
		attempt_and_apply(elist);
		elist.clear();

		// select a card
		cardpack cards;
		for (auto& cc : p->hand) {
			if (main_playable(p, cc))
				cards.push_back(cc);
		}
		if (!cards.empty())
			elist = select_and_play(p, cards, EMPTY_EVENT);
		attempt_and_apply(elist);

		// using deck::get_cards() because
		// discard_card will remove the card from flux
		for (auto& c : f->flux.get_cards()) {
			// TODO: do something with returned Evlists?
			discard_card(c);
			coord->mundane_discard(c);
		}

		// XXX this needs to be done after every time someone is damaged
		// so somehow in Player::damage (and possibly Player::heal)?
		// Player is now a friend of Engine so this should be easier
		auto iter = ps.begin();
		while (iter != ps.end()) {
			if ((*iter)->dead()) {
				// XXX discard all of the player's cards
				coord->player_dead(*iter);
				dead_ps.push_back(*iter);
				iter = ps.erase(iter);
			} else {
				iter++;
			}
		}

		if (everyone_dead()) {
			_running = false;
			coord->game_end();
		}
	}
}

void Engine::attempt_and_apply(Evlist elist) {
	Evlist next_round = elist;
	while (!next_round.empty()) {
		Evlist collected;
		list<Evlistf> will_collect;
		for (auto& e : next_round) {
			will_collect.push_back(attempt(e));
		}
		for (auto& e : will_collect) {
			collected.append(e.get());
		}
		next_round.clear();
		for (auto& e : collected) {
			next_round.append(apply(e));
		}
	}
}

Evlistf Engine::attempt(Event& ev) {
	Evlist evlist;

	if (ev.attempted)
		return coord->wrap(single_event(ev));
	ev.attempted = true;

	if (ev.c->_facedown) {
		ev.c->_facedown = false;
		coord->card_revealed(ev.c, Cardloc::PRANK);
	}

	coord->attempt(ev);

	switch (ev.etype) {
	case ETYPE::NONE:
		break;
	case ETYPE::ATTACK:
		if (counterable(ev.to, ev)) {
			Player *p = ev.to;
			cardpack cards;
			for (auto& cc : p->hand) {
				if (cc->playable_type() & CTYPE::CNTR) {
					if (cc->can_counter(ev)) {
						cards.push_back(cc);
					}
				}
			}
			return future_select_and_play(p, cards, ev);
		} else {
			return coord->wrap(single_event(ev));
		}
		break;
	case ETYPE::HEAL:
	case ETYPE::MOVE:
	case ETYPE::DISCARD:
	case ETYPE::PARAM:
		return coord->wrap(single_event(ev));
	default:
		assertFalse("Bad ETYPE for attempt()");
	}
	return coord->wrap(Evlist());
}

Event Engine::handle_response(const Event& ev) {
	playercards additional_cards;
	for (auto& pp : ps) {
		for (auto& cc : pp->hand) {
			if (cc->can_play_now(ev))
				additional_cards.emplace_back(pp, cc);
		}
	}
	return coord->five_second_timer(ps, additional_cards).now_ev([=](Card *c) -> Event {
		if (!c) { // everyone passed priority
			return ev;
		}
		// assert c is in somebody's hand and that they can play it
		auto pc = find_if(additional_cards, [=](const playercard& pc) { return pc.second == c; });
		assertNotEqual(pc, additional_cards.end());
		assertTrue(c->can_play_now(ev));
		Player *p = pc->first;
		return play_card_event(p, c, ev);
	}).get();
}

Evlistf Engine::future_select_and_play(Player *p, const cardpack& cards, const Event& ev) {
	return coord->select_counter(p, cards).now_evl([=](Card *c) -> Evlist {
		if (!c) { // pass
			return single_event(ev);
		}
		assertIn(c, cards);
		return play_card(p, c, ev);
	});
}

Evlist Engine::select_and_play(Player *p, const cardpack& cards, const Event& ev) {
	Card *cchoice = coord->select_card(p, cards).get();
	assertIn(cchoice, cards);
	Evlist evlist = play_card(p, cchoice, ev);
	return evlist;
}

Player* Engine::select_target(const Player *p) {
	if (_randomizer && !p->_la_la_land)
		return random_player();

	list<Player*> players = ps;
	
	// stable-sort the field cards to the beginning of the list
	// since field cards get lower priority
	auto sorted_elect_target_fns = elect_target_fns;
	sorted_elect_target_fns.sort(
		[](std::pair<Card*,elect_target_fn_t>& lhs, std::pair<Card*,elect_target_fn_t>& rhs) {
		return (lhs.first->ci.ctype & CTYPE::FFLD) && !(rhs.first->ci.ctype & CTYPE::FFLD);
	});

	Player *pelected = nullptr;
	for (auto& pair : sorted_elect_target_fns) {
		auto& chook = pair.first;
		if (!(p->_la_la_land && (chook->ci.ctype & CTYPE::FFLD))) {
			auto& fn = pair.second;
			Player *pnew = fn(p, players);
			if (pnew) {
				assertIn(pnew, players);
				pelected = pnew;
			}
		}
	}

	if (pelected) {
		players.clear();
		players.push_back(pelected);
	}
	for (auto& pair : remove_target_fns) {
		auto& fn = pair.second;
		fn(p, players);
	}

	Player *choice = coord->select_player(p, players).get();
	assertIn(choice, players);
	return choice;
}

Player* Engine::random_player() {
	std::uniform_int_distribution<> uid(0, ps.size()-1);
	return *std::next(ps.begin(), uid(_rng));
}

Card* Engine::select_card(const Player *p, const cardpack& cards) {
	Card *choice = coord->select_card(p, cards).get();
	assertIn(choice, cards);
	return choice;
}

cardpack Engine::select_multi_cards(const Player *p, const cardpack& ccards) {
	cardpack choices = coord->select_multi_cards(p, ccards).get();
	for (auto& card : ccards) {
		assertIn(card, ccards);
	}
	return choices;
}

ATYPE Engine::select_atype(const Player *p) {
	ATYPE choice = coord->select_atype(p).get();
	return choice;
}

Evlist Engine::apply(const Event& ev) {
	if (!ev.attempted)
		return single_event(ev);

	for (auto& pair : last_minute_fns) {
		auto& fn = pair.second;
		Evlist evlist = fn(ev);
		if (!evlist.empty())
			return evlist; // need to go back through attempt()
	}

	Evlist elist;
	switch (ev.etype) {
	case ETYPE::NONE:
		break;
	case ETYPE::ATTACK:
		if (ev.to) {
			Event ea = ev;
			ea.a = ev.to->damage(ev.a);
			if (!ea.a.annulled) {
				for (auto& pair : post_damage_fns) {
					auto& fn = pair.second;
					elist.append(fn(ea));
				}
			}
		}
		break;
	case ETYPE::HEAL:
		assertNotNull(ev.to);
		ev.to->heal(ev.h);
		break;
	case ETYPE::MOVE:
		switch (ev.m.where) {
		case Cardloc::HAND:
			assertNotNull(ev.to);
			ev.to->hand.push_back(ev.m.cmove);
			break;
		case Cardloc::ITEM:
			assertNotNull(ev.to);
			// check max hand size, replace item if necessary
			if (ev.m.free) {
				Parameter param;
				param.parameter = Parameter::IMAX;
				param.adjustment = +1;
				adjust_params(ev.to, param);
				ev.to->param_adjustments.emplace_back(ev.c, std::move(param));
			} else if (ev.to->item.size() == ev.to->item.max()) {
				Card *cchoice = coord->select_card(ev.to, ev.to->item.get_cards()).get();
				elist.append(discard_card(cchoice));
				coord->mundane_discard(cchoice);
			}
			ev.to->item.push_back(ev.m.cmove);
			break;
		case Cardloc::OTHER:
			assertNotNull(ev.to);
			ev.to->othr.push_back(ev.m.cmove);
			break;
		case Cardloc::FIELD:
			assertNull(ev.to);
			// replace field if necessary
			f->field.hasmax(!_khaos_realm); // as good a place to put this line as any
			if (ev.m.free) {
				Parameter param;
				param.parameter = Parameter::FMAX;
				param.adjustment = +1;
				adjust_params(nullptr, param);
				_param_adjustments.emplace_back(ev.c, std::move(param));
			} else if (_khaos_realm) {
				Card *cchoice = coord->select_card(ev.from, f->field.get_cards(), true).get();
				if (cchoice) {
					elist.append(discard_card(cchoice));
					coord->mundane_discard(cchoice);
				}
			} else if (f->field.size() == f->field.max()) {
				Card *cchoice = f->field.back();
				if (f->field.size() > 1)
					cchoice = coord->select_card(ev.from, f->field.get_cards()).get();
				elist.append(discard_card(cchoice));
				coord->mundane_discard(cchoice);
			}
			f->field.push_back(ev.m.cmove);
			break;
		case Cardloc::PRANK:
			assertNull(ev.to);
			ev.m.cmove->_facedown = true;
			f->pranks.push_back(ev.m.cmove);
			break;
		default:
			assertFalse("Bad Move location for apply()");
		}
		coord->card_moved(ev.from, ev.to, ev.c, ev.m.cmove, ev.m.where);
		break;
	case ETYPE::DISCARD:
		elist.append(discard_card(ev.d.cdiscard));
		coord->card_discarded(ev);
		break;
	case ETYPE::PARAM:
		assertNotNull(ev.to);
		elist.append(adjust_params(ev.to, ev.p));
		ev.to->param_adjustments.emplace_back(ev.c, Parameter(ev.p)); // not std::move because we want to keep it for the applyfns
		break;
	default:
		assertFalse("Bad ETYPE for apply()");
	}
	if (ev.apply_fn)
		elist.append(ev.apply_fn(ev.from, ev.c, ev.to));
	if (ev.prev && ev.prev->etype == ETYPE::ATTACK && ev.prev->a.countered_fn) {
		elist.append(ev.prev->a.countered_fn(ev));
	}
	return elist;
}

void Engine::modify_attacks(Event& ev) {
	if (ev.etype == ETYPE::PARENT) {
		for (auto& e : ev.child_events) {
			modify_attacks(e);
		}
	} else if (ev.etype == ETYPE::ATTACK) {
		AttackModifier amod;
		for (auto& pair : modify_attack_fns) {
			auto& fn = pair.second;
			fn(ev, amod);
		}
		ev.a.hit *= amod.multiplier;
		ev.a.hit += amod.adder;
	}
}

Evlist Engine::play_card(Player *p, Card *c, const Event& prev) {
	return play_card_react(play_card_event(p, c, prev));
}

Event Engine::play_card_event(Player *p, Card *c, const Event& prev) {
	cardpack also_flux;
	Event ev = c->play_fn(p, c, prev, also_flux);
	coord->card_played(ev.from, ev.to, c);

	f->flux.push_back(c);
	f->flux.copy_back(also_flux);

	modify_attacks(ev);

	return handle_response(ev);
}

Evlist Engine::play_card_react(const Event& ev) {
	for (auto& pair : card_played_fns) {
		auto& fn = pair.second;
		Evlist evlist = fn(ev);
		if (!evlist.empty())
			return evlist;
	}

	Evlist evlist = (ev.etype == ETYPE::PARENT) ? ev.child_events : single_event(ev);
	Evlist additional_elist;
	for (const auto& ev : evlist) {
		for (auto& pair : after_card_played_fns) {
			auto& fn = pair.second;
			additional_elist.append(fn(ev));
		}
	}
	evlist.append(std::move(additional_elist));

	return evlist;
}

Evlist Engine::adjust_params(Player *p, const Parameter& param) {
	Evlist evlist;
	switch (param.parameter) {
	case Parameter::HMAX:
		assertNotNull(p);
		p->hand.max(p->hand.max() + param.adjustment);
		// drawing to fill is handled by deck::max()
		break;
	case Parameter::IMAX:
		assertNotNull(p);
		p->item.max(p->item.max() + param.adjustment);
		while (p->item.size() > p->item.max()) {
			Card *cchoice = select_card(p, p->item.get_cards());
			evlist.append(discard_card(cchoice));
			coord->mundane_discard(cchoice);
		}
		break;
	case Parameter::FMAX:
		assertNull(p);
		f->field.max(f->field.max() + param.adjustment);
		// can't assert field.size() <= field.max() because if we're
		// removing khaos realm, the other field cards are still here
		break;
	default:
		assertFalse("Bad Parameter for apply()");
	}
	return evlist;
}

Evlist Engine::discard_card(Card *c) {
	Evlist evlist = c->discard_fn();

	list<std::pair<Player*,std::pair<Card*,Parameter>>> relevant_adjustments;

	// tear down all registrations
	card_played_fns.remove(c);
	playability_fns.remove(c);
	last_minute_fns.remove(c);
	modify_attack_fns.remove(c);
	after_card_played_fns.remove(c);
	post_damage_fns.remove(c);
	before_turn_start_fns.remove(c);
	elect_target_fns.remove(c);
	remove_target_fns.remove(c);

	_param_adjustments.remove_if([&](std::pair<Card*,Parameter> pair) {
		if(pair.first == c) {
			relevant_adjustments.emplace_back(nullptr, std::move(pair));
			return true;
		}
		return false;
	});

	// player registrations
	for (auto& p : all_ps) {
		p->turnstart_fns.remove(c);
		p->damage_fns.remove(c);
		p->removable_turnstart_fns.remove(c);

		p->param_adjustments.remove_if([&](std::pair<Card*,Parameter> pair) {
			if(pair.first == c) {
				relevant_adjustments.emplace_back(p.get(), std::move(pair));
				return true;
			}
			return false;
		});
	}

	f->discard.push_back(c);
	for (auto& pair : relevant_adjustments) {
		Player *p = pair.first;
		Parameter& param = pair.second.second;
		param.adjustment *= -1;
		adjust_params(p, param);
	}
	return evlist;
}

bool Engine::main_playable(Player *p, Card *c) {
	if (!c->can_play_main())
		return false;
	for (auto& pair : playability_fns) {
		auto& chook = pair.first;
		if (!(p->_la_la_land && !(chook->ci.ctype & CTYPE::FFLD))) {
			auto& fn = pair.second;
			if (!fn(c))
				return false;
		}
	}
	return true;
}

bool Engine::counterable(Player *p, const Event& ev) {
	(void)p;
	return ev.etype == ETYPE::ATTACK &&
		(_ping_pong || ev.a.counter_count == 0) &&
		!ev.a.uncounterable;
}

bool Engine::everyone_dead() {
	int num_alive = 0;
	for (auto& p : ps) {
		if (!p->dead())
			num_alive++;
	}
	return num_alive == 1;
}

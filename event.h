#ifndef _EVENT_H
#define _EVENT_H

#include <functional>
#include <list>
#include <memory>

#include "card_flags.h"

struct Event;
class Player;

class Evlist : private std::list<Event> {
	typedef std::list<Event> Evlist_base;
public:
	Evlist() : Evlist_base() { }
	Evlist(std::list<Event> base) : Evlist_base(base) { }

	void append(Evlist&& rhs) {
		Evlist_base::splice(Evlist_base::end(), std::move(rhs));
	}

	using Evlist_base::push_back;
	using Evlist_base::cend;
	using Evlist_base::empty;
	using Evlist_base::clear;
	using Evlist_base::cbegin;
	using Evlist_base::end;
	using Evlist_base::begin;
	using Evlist_base::front;
};

enum class ETYPE : uint8_t {
	NONE,
	ATTACK,
	HEAL,
	MOVE,
	DISCARD,
	PARAM,
	PARENT, // for boom, slap to the left disease, etc. Everything else should use apply_fn when possible
};

typedef std::function<Evlist(Player*,Card*,Player*)> applyfn_t;
typedef std::function<Evlist(const Event&)> counteredfn_t;

// should always be initialized from make_attack() in smac_card_set.cpp
struct Attack {
	int16_t hit = 0;
	aff_t aff;
	atype_t atype;

	int16_t counter_count = 0;
	bool uncounterable = false;
	counteredfn_t countered_fn;

	bool annulled = false;

	// card specific fields (make sure you update meat_shield_playfn accordingly)
	bool pineapple = false;
};

struct AttackModifier {
	int16_t adder = 0;
	int16_t multiplier = 1;
};

struct Heal {
	int16_t hit;

	// for preventing free milk steal
	bool unstealable = false;

	// card specific fields
	bool chocolate_cow = false;
};

struct Move {
	Cardloc where;
	Card *cmove;
	bool free = false; // for things like "free" items e.g. extra leg
};

struct Discard {
	Card *cdiscard;
};

struct Parameter {
	enum {
		HMAX,
		IMAX,
		FMAX,
	} parameter;
	int16_t adjustment;
};

struct Event {
	ETYPE etype;
	Card *c; // the card generating this event
	Player *from;
	Player *to;

	Attack a;
	Heal h;
	Move m;
	Discard d;
	Parameter p;
	Evlist child_events;

	applyfn_t apply_fn;

	std::shared_ptr<Event> prev;

	bool attempted = false;

	Event(ETYPE etype) : etype(etype) { }
};

static inline Evlist single_event(const Event& ev) {
	Evlist evlist;
	evlist.push_back(ev);
	return evlist;
}

#endif // #ifndef _EVENT_H

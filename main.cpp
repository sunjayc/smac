#include <iostream>

#include "engine.h"
#include "flagset.h"
#include "network.h"
#include "network_coordinator.h"
#include "play_structures.h"
#include "smac_card_set.h"

using std::vector;
using std::string;

int main(int argc, char** argv) {
	if (argc > 1) {
		string arg = argv[1];
		if (arg == "test") {
			std::cout << "Running test suite..." << std::endl;
			test_flagset();
		} else if (arg == "servertest") {
			std::cout << "Testing server..." << std::endl;
			test_server();
		} else if (arg == "clienttest") {
			std::cout << "Testing client..." << std::endl;
			test_client();
		} else {
			Playfield f;
			f.shealth = 20;
			f.shmax = 5;
			f.simax = 1;
			NetworkCoordinator coord("9949");
			SmacCardSet d;
			d.generate_basic_20();
			d.generate_additional_40();
			if (arg == "wedding")
				d.the_wedding();
			Engine e (&f, &coord, &d);
			e.init();

			e.gameloop();
		}
		std::cout << "Success!" << std::endl;
		exit(0);
	}

	std::cout << "Starting game client..." << std::endl;
	auto csock = std::make_shared<OSSocket>();
	assertTrue(csock->connectServer("localhost", 9949));
	NetworkCoordinatorClient ncc (csock);
	assertTrue(ncc.init());
	assertTrue(ncc.getPlayers());
	while (ncc.handle_message());

	return 0;
}

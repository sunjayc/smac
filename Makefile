CXX = g++
CFLAGS = -g -Wall -Werror -Wpedantic -std=gnu++11
LDFLAGS =

EXCLUDE = console_interface.cpp windows_sockets.cpp basic_coordinator.cpp
SOURCE = $(filter-out $(EXCLUDE),$(wildcard *.cpp))
HEADERS = $(filter-out $(EXCLUDE), $(wildcard *.h))

BINDIR = bin
OBJS = $(addprefix $(BINDIR)/,$(SOURCE:.cpp=.o))
DEPS = $(OBJS:.o=.d)

.PHONY: all clean depend

$(shell mkdir -p $(BINDIR))

all: $(BINDIR)/smac tags

clean:
	rm -rf $(BINDIR)

depend: $(DEPS)

tags: $(SOURCE) $(HEADERS)
	ctags $^

$(BINDIR)/%.d: %.cpp
	$(CXX) $(CFLAGS) -MM $< -MT '$(@:.d=.o)' -MF $@

-include $(DEPS)

$(BINDIR)/%.o: %.cpp
	$(CXX) -o $@ -c $(CFLAGS) $<

$(BINDIR)/smac: $(OBJS)
	$(CXX) $(LDFLAGS) $(OBJS) -o $@


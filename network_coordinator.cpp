#include "network_coordinator.h"

#include <algorithm>
#include <iostream>
#include <list>
#include <memory>
#include <sstream>
#include <string>
#include <vector>

#include "asserts.h"
#include "card.h"
#include "network.h"
#include "play_structures.h"
#include "utilities.h"

using std::cin;
using std::cout;
using std::endl;
using std::getline;
using std::list;
using std::make_shared;
using std::shared_ptr;
using std::string;
using std::vector;

NetworkCoordinator::NetworkCoordinator(string port) :
	_ssock(std::make_shared<OSServerSocket>(port))
{ }

uint8_t NetworkCoordinator::_retrieve_players(std::vector<shared_ptr<Player>>& ps, Playfield *f) {
	_d = f->get_cards();
	cout << "Number of players? ";
	string s;
	getline(cin, s);
	int numpint;
	std::stringstream(s) >> numpint;
	uint8_t nump = (uint8_t)numpint;
	cout << "Waiting for players..." << endl;

	uint8_t i = 0;
	while (i < nump) {
		auto csock = std::shared_ptr<Socket>(_ssock->acceptClient());
		cout << "accepted client" << endl;
		NetworkPacker np (csock);
		if (np.sendVer(NETWORK_COORDINATOR_VER)) {
			_conns.push_back(np);
			auto p = make_shared<Player>(f, this);
			string name;
			np.read(name);
			p->name = name;
			ps.push_back(p);
			i++;
		}
		cout << "got player " << static_cast<int>(i) << endl;
	}

	uint8_t index = 0;
	for (auto& conn : _conns) {
		conn.write(index++);
		conn.write_size(ps);
		for (auto& p : ps) {
			conn.write(p->name);
			conn.write(p->_health);
		}
	}

	for (auto& conn: _conns) {
		const auto& d = f->get_cards();
		// can't use write_size because we need uint16_t
		conn.write(static_cast<uint16_t>(d.size()));
		for (auto& c : d) {
			auto& ci = c->ci;
			conn.write(ci.name);
			for (auto& line : ci.desc)
				conn.write(line);
			for (auto& line : ci.flavtxt)
				conn.write(line);
			send(conn, ci.aff);
			send(conn, ci.ctype);
			send(conn, ci.atype);
		}
	}

	return nump;
}

void NetworkCoordinator::turn_start(const Player *curp) {
	cout << "turn start" << endl;
	for (auto& conn : _conns) {
		send(conn, Message::TURN_START);
		send(conn, curp);
	}
}

void NetworkCoordinator::new_hands(const list<Player*>& players) {
	cout << "new hands" << endl;
	for (auto& p : _ps) {
		auto& conn = _conns[p->index()];
		send(conn, Message::NEW_HANDS);
		conn.write_size(players);
		for (auto& pp : players) {
			send(conn, pp);
			if (pp == p)
				send(conn, p->hand.get_cards());
			else
				conn.write_size(pp->hand);
		}
	}
}

void NetworkCoordinator::drew_card(const Player *p, const Card *c) {
	cout << "drew card" << endl;
	for (auto& pp : _ps) {
		auto& conn = _conns[pp->index()];
		send(conn, Message::DREW_CARD);
		send(conn, p);
		if (pp == p)
			send(conn, c);
		else
			send_blank_card(conn);
	}
}

void NetworkCoordinator::card_moved(const Player *from, const Player *to, const Card *cfrom, const Card *cto, Cardloc where) {
	cout << "card moved" << endl;
	for (auto& pp : _ps) {
		auto& conn = _conns[pp->index()];
		send(conn, Message::CARD_MOVED);
		send(conn, from);
		send(conn, to);
		if (where == Cardloc::PRANK && from != pp) {
			send_blank_card(conn);
			send_blank_card(conn);
		} else {
			send(conn, cfrom);
			send(conn, cto);
		}
		conn.write(static_cast<uint8_t>(where));
	}
}

void NetworkCoordinator::card_played(const Player *from, const Player *to, const Card *c) {
	cout << "card played" << endl;
	for (auto& pp : _ps) {
		auto& conn = _conns[pp->index()];
		send(conn, Message::CARD_PLAYED);
		send(conn, from);
		send(conn, to);
		if ((c->ci.ctype & CTYPE::PRNK) && from != pp)
			send_blank_card(conn);
		else
			send(conn, c);
	}
}

void NetworkCoordinator::card_revealed(const Card *c, Cardloc where) {
	cout << "card revealed" << endl;
	for (auto& conn : _conns) {
		send(conn, Message::CARD_REVEALED);
		send(conn, c);
		conn.write(static_cast<uint8_t>(where));
	}
}

void NetworkCoordinator::card_discarded(const Event& ev) {
	cout << "card discarded" << endl;
	assertEqual(ev.etype, ETYPE::DISCARD);
	for (auto& conn : _conns) {
		send(conn, Message::CARD_DISCARDED);
		send(conn, ev.from);
		send(conn, ev.to);
		send(conn, ev.c);
		send(conn, ev.d.cdiscard);
	}
}

void NetworkCoordinator::mundane_discard(const Card *c) {
	cout << "mundane discard" << endl;
	for (auto& conn : _conns) {
		send(conn, Message::MUNDANE_DISCARD);
		send(conn, c);
	}
}

void NetworkCoordinator::attempt(const Event& ev) {
	cout << "attempt" << endl;
	for (auto& conn : _conns) {
		send(conn, Message::ATTEMPT);
		send(conn, ev.from);
		send(conn, ev.to);
		send(conn, ev.c);
		conn.write(static_cast<uint8_t>(ev.etype));
		switch (ev.etype) {
		case ETYPE::ATTACK:
			conn.write(ev.a.hit);
			break;
		case ETYPE::HEAL:
			conn.write(ev.h.hit);
			break;
		default:
			// do nothing
			break;
		}
	}
}

void NetworkCoordinator::player_hit(const Player *p, int16_t hit) {
	cout << "player hit" << endl;
	for (auto& conn : _conns) {
		send(conn, Message::PLAYER_HIT);
		send(conn, p);
		conn.write(hit);
	}
}

void NetworkCoordinator::player_healed(const Player *p, int16_t hit) {
	cout << "player healed" << endl;
	for (auto& conn : _conns) {
		send(conn, Message::PLAYER_HEALED);
		send(conn, p);
		conn.write(hit);
	}
}

void NetworkCoordinator::player_dead(const Player *p) {
	cout << "player dead" << endl;
	for (auto& conn : _conns) {
		send(conn, Message::PLAYER_DEAD);
		send(conn, p);
	}
}

void NetworkCoordinator::game_end() {
	cout << "game end" << endl;
	for (auto& conn : _conns) {
		send(conn, Message::GAME_END);
	}
}

void NetworkCoordinator::wait(uint32_t id) {
	query q = get_query(id);
	bool *ready;
	switch (q.type) {
	case query::CARD:
	case query::COUNTER:
		cout << "card/counter" << endl;
		ready = &q.fc->ready;
		break;
	case query::TIMER:
		cout << "timer" << endl;
		ready = &q.fc->ready;
		break;
	case query::MULTI_CARDS:
		cout << "multi cards" << endl;
		ready = &q.fcc->ready;
		break;
	case query::PLAYER:
		cout << "player" << endl;
		ready = &q.fp->ready;
		break;
	case query::ATCKTYPE:
		cout << "atck type" << endl;
		ready = &q.fa->ready;
		break;
	default:
		cout << "invalid query" << endl;
		return;
	}

	while (!*ready) {
		vector<WindowsEventSocket*> wesocks;
		for (auto& conn : _conns) {
			wesocks.push_back(dynamic_cast<WindowsEventSocket*>(conn._sock.get()));
		}
		wesocks = WindowsEventSocket::wait_for_read(wesocks);
		for (auto& wesock : wesocks) {
			// XXX this is terrible and you know it
			auto index = find_if(_conns, [&](const NetworkPacker& np) { return np._sock.get() == wesock; }) - _conns.begin();
			auto& p = _ps[index];
			handle_read(p);
		}
	}
}

void NetworkCoordinator::handle_read(Player *p) {
	auto& conn = _conns[p->index()];
	Message msg;
	uint32_t id;
	recv(conn, msg, id);
	query& q = get_query(id);
	switch (msg) {
	case Message::SELECT_CARD:
		get_select_card(conn, q);
		break;
	case Message::SELECT_PLAYER:
		get_select_player(conn, q);
		break;
	case Message::SELECT_MULTI_CARDS:
		get_select_multi_cards(conn, q);
		break;
	case Message::TIMER:
		get_timer(p, q);
		break;
	case Message::SELECT_ATYPE:
		get_select_atype(conn, q);
		break;
	}
}

void NetworkCoordinator::timer(const query& q) {
	for (auto& p : q.players) {
		auto& conn = _conns[p->index()];
		send(conn, Message::TIMER, q.fc->id);
		cardpack cards;
		for (auto& pair : q.additional_cards) {
			if (pair.first == p)
				cards.push_back(pair.second);
		}
		send(conn, cards);
	}
}

void NetworkCoordinator::get_timer(Player *p, query& q) {
	Card *c = nullptr;
	auto& conn = _conns[p->index()];
	recv(conn, c);

	if (q.type == query::NONE)
		return;
	// XXX check card
	if (c) {
		q.fc->val = make_shared<Card*>(c);
		q.fc->ready = true;
	} else {
		q.players.remove(p);
		if (q.players.empty()) {
			q.fc->val = make_shared<Card*>(nullptr);
			q.fc->ready = true;
		}
	}
	if (q.fc->ready) {
		auto fc = q.fc; // very deliberately not auto&
		pop_query(q.fc->id); // because this kills references
		if (fc->now_event) {
			fc->fevstate->val = make_shared<Event>(fc->now_event(c));
			fc->fevstate->ready = true;
		}
		if (fc->now_evlist) {
			fc->fevlstate->val = make_shared<Evlist>(fc->now_evlist(c));
			fc->fevlstate->ready = true;
		}
	}
}

void NetworkCoordinator::select_card(const query& q) {
	uint8_t index = q.p->index();
	auto& conn = _conns[index];
	send(conn, Message::SELECT_CARD, q.fc->id);
	conn.write(q.passable);
	send(conn, q.cards);
}

void NetworkCoordinator::get_select_card(NetworkPacker& conn, query& q) {
	Card *c = nullptr;
	recv(conn, c);

	if (q.type == query::NONE)
		return;
	if (c == nullptr && !q.passable)
		return select_card(q);
	if (c != nullptr && !found(q.cards, c))
		return select_card(q);
	q.fc->val = make_shared<Card*>(c);
	q.fc->ready = true;
	auto fc = q.fc; // very deliberately no auto&
	pop_query(q.fc->id); // because this kills references
	if (fc->now_event) {
		fc->fevstate->val = make_shared<Event>(fc->now_event(c));
		fc->fevstate->ready = true;
	}
	if (fc->now_evlist) {
		fc->fevlstate->val = make_shared<Evlist>(fc->now_evlist(c));
		fc->fevlstate->ready = true;
	}
}

void NetworkCoordinator::select_multi_cards(const query& q) {
	uint8_t index = q.p->index();
	auto& conn = _conns[index];
	send(conn, Message::SELECT_MULTI_CARDS, q.fcc->id);
	conn.write(q.passable);
	send(conn, q.cards);
}

void NetworkCoordinator::get_select_multi_cards(NetworkPacker& conn, query& q) {
	cardpack cards;
	recv(conn, cards);

	if (q.type == query::NONE)
		return;
	if (cards.empty() && !q.passable)
		return select_multi_cards(q);
	if (!cards.empty()) {
		for (auto& c : cards) {
			if (!found(q.cards, c))
				return select_multi_cards(q);
		}
	}
	q.fcc->val = make_shared<cardpack>(cards);
	q.fcc->ready = true;
	pop_query(q.fcc->id);
}

void NetworkCoordinator::select_player(const query& q) {
	uint8_t index = q.p->index();
	auto& conn = _conns[index];
	send(conn, Message::SELECT_PLAYER, q.fp->id);
	conn.write(q.passable);
	send(conn, q.players);
}

void NetworkCoordinator::get_select_player(NetworkPacker& conn, query& q) {
	Player *p = nullptr;
	recv(conn, p);

	if (q.type == query::NONE)
		return;
	if (p == nullptr && !q.passable)
		return select_player(q);
	if (p != nullptr && !found(q.players, p))
		return select_player(q);
	q.fp->val = make_shared<Player*>(p);
	q.fp->ready = true;
	pop_query(q.fp->id);
}

void NetworkCoordinator::select_atype(const query& q) {
	uint8_t index = q.p->index();
	auto& conn = _conns[index];
	send(conn, Message::SELECT_ATYPE, q.fa->id);
	conn.write(q.passable);
}

void NetworkCoordinator::get_select_atype(NetworkPacker& conn, query& q) {
	ATYPE atype;
	recv(conn, atype);

	if (q.type == query::NONE)
		return;
	if (atype == ATYPE::NONE && !q.passable)
		return select_atype(q);
	q.fa->val = make_shared<ATYPE>(atype);
	q.fa->ready = true;
	pop_query(q.fa->id);
}

bool NetworkCoordinator::send(NetworkPacker& conn, const Message& msg, const uint32_t id) {
	if (!conn.write(static_cast<uint8_t>(msg)))
		return false;
	return conn.write(id);
}

bool NetworkCoordinator::recv(NetworkPacker& conn, Message& msg, uint32_t& id) {
	uint8_t msg_code;
	if (!conn.read(msg_code))
		return false;
	msg = static_cast<Message>(msg_code);
	return conn.read(id);
}

bool NetworkCoordinator::send(NetworkPacker& conn, const AFF& aff) {
	return conn.write(static_cast<uint8_t>(aff));
}

bool NetworkCoordinator::send(NetworkPacker& conn, const aff_t& aff) {
	// this should be done better
	uint8_t counter = 0;
	for (uint8_t i = 0; i < (uint8_t)AFF::MAX_VAL; i++) {
		AFF a = (AFF)i;
		if (aff & a)
			counter++;
	}
	conn.write(counter);
	for (uint8_t i = 0; i < (uint8_t)AFF::MAX_VAL; i++) {
		AFF a = (AFF)i;
		if (aff & a)
			if (!send(conn, a))
				return false;
	}
	return true;
}

bool NetworkCoordinator::send(NetworkPacker& conn, const CTYPE& ctype) {
	return conn.write(static_cast<uint8_t>(ctype));
}

bool NetworkCoordinator::send(NetworkPacker& conn, const ctype_t& ctype) {
	// this should be done better
	uint8_t counter = 0;
	for (uint8_t i = 0; i < (uint8_t)CTYPE::MAX_VAL; i++) {
		CTYPE c = (CTYPE)i;
		if (ctype & c)
			counter++;
	}
	conn.write(counter);
	for (uint8_t i = 0; i < (uint8_t)CTYPE::MAX_VAL; i++) {
		CTYPE c = (CTYPE)i;
		if (ctype & c)
			if (!send(conn, c))
				return false;
	}
	return true;
}

bool NetworkCoordinator::send(NetworkPacker& conn, const ATYPE& atype) {
	return conn.write(static_cast<uint8_t>(atype));
}

bool NetworkCoordinator::send(NetworkPacker& conn, const atype_t& atype) {
	// this should be done better
	uint8_t counter = 0;
	for (uint8_t i = 0; i < (uint8_t)ATYPE::MAX_VAL; i++) {
		ATYPE a = (ATYPE)i;
		if (atype & a)
			counter++;
	}
	conn.write(counter);
	for (uint8_t i = 0; i < (uint8_t)ATYPE::MAX_VAL; i++) {
		ATYPE a = (ATYPE)i;
		if (atype & a)
			if (!send(conn, a))
				return false;
	}
	return true;
}

bool NetworkCoordinator::recv(NetworkPacker& conn, ATYPE& atype) {
	return conn.read((uint8_t&)atype);
}

bool NetworkCoordinator::send_blank_card(NetworkPacker& conn) {
	return conn.write(static_cast<uint16_t>(UINT16_MAX));
}

bool NetworkCoordinator::send(NetworkPacker& conn, const Card* c) {
	assertNotNull(c);
	return conn.write(c->index());
}

bool NetworkCoordinator::recv(NetworkPacker& conn, Card* &c) {
	uint16_t index;
	if (!conn.read(index))
		return false;
	if (index >= _d.size())
		c = nullptr;
	else
		c = _d[index];
	return true;
}

bool NetworkCoordinator::send(NetworkPacker& conn, const cardpack& cards) {
	if (!conn.write_size(cards))
		return false;
	for (auto& c : cards) {
		if (!send(conn, c))
			return false;
	}
	return true;
}

bool NetworkCoordinator::recv(NetworkPacker& conn, cardpack& cards) {
	uint8_t size;
	if (!conn.read(size))
		return false;
	for (uint8_t i = 0; i < size; i++) {
		Card *c;
		if (!recv(conn, c))
			return false;
		cards.push_back(c);
	}
	return true;
}

bool NetworkCoordinator::send(NetworkPacker& conn, const Player* p) {
	if (p == nullptr)
		return conn.write(static_cast<uint8_t>(UINT8_MAX));
	return conn.write(p->index());
}

bool NetworkCoordinator::recv(NetworkPacker& conn, Player* &p) {
	uint8_t index;
	if (!conn.read(index))
		return false;
	if (index >= _ps.size())
		return false;
	p = _ps[index];
	return true;
}

bool NetworkCoordinator::send(NetworkPacker& conn, const list<Player*>& players) {
	if (!conn.write_size(players))
		return false;
	for (auto& p : players) {
		if (!send(conn, p))
			return false;
	}
	return true;
}



bool NetworkCoordinatorClient::handle_input(const string& input, int& val) {
	vector<int> vals;
	bool res = handle_input(input, vals);
	if (!vals.empty())
		val = vals[0];
	return res;
}

bool NetworkCoordinatorClient::handle_input(const string& input, vector<int>& vals) {
	string tok;
	std::stringstream ss (input);
	int val;
	ss >> tok;
	vals.clear();
	if (tok == "help" || tok == "?") {
		cout << "  [s]how <card number>" << endl;
		cout << "  [h]and" << endl;
		cout << "  [i]tems" << endl;
		cout << "  [f]ield" << endl;
		cout << "  [p]ranks" << endl;
		cout << "  players" << endl;
		cout << "  health" << endl;
		cout << "  play <card number(s)>" << endl;
		cout << "  target <player number(s)>" << endl;
		cout << "  select/x <card/player/choice number(s)>" << endl;
		cout << "  pass" << endl;
	} else if (tok == "hand") {
		print_hand();
	} else if (tok == "item" || tok == "items" || tok == "i") {
		print_items();
	} else if (tok == "field" || tok == "fields" || tok == "f") {
		print_field();
	} else if (tok == "prank" || tok == "pranks" || tok == "p") {
		print_pranks();
	} else if (tok == "players" || tok == "health") {
		print_players();
	} else if (tok == "show" || tok == "info" || tok == "s") {
		uint16_t index;
		ss >> index;
		index -= 1;
		show_card(index);
	} else if (tok == "play" || tok == "target" || tok == "select" || tok == "x") {
		while (ss >> val) {
			if (val <= 0) {
				cout << "bad value" << endl;
				return false;
			}
			val -= 1;
			vals.push_back(val);
		}
		return true;
	} else if (tok == "pass") {
		val = -1;
		return true;
	} else {
		cout << "bad command" << endl;
	}
	return false;
}

bool NetworkCoordinatorClient::send(const Message& msg, const uint32_t id) {
	if (!_conn.write(static_cast<uint8_t>(msg)))
		return false;
	return _conn.write(id);
}

bool NetworkCoordinatorClient::recv(Message& msg, uint32_t& id) {
	uint8_t msg_code;
	if (!_conn.read(msg_code))
		return false;
	msg = static_cast<Message>(msg_code);
	return _conn.read(id);
}

bool NetworkCoordinatorClient::recv(AFF& aff) {
	return _conn.read((uint8_t&)aff);
}

bool NetworkCoordinatorClient::recv(aff_t& aff) {
	uint8_t sz;
	if (!_conn.read(sz))
		return false;
	for (uint8_t i = 0; i < sz; i++) {
		AFF a;
		if (!recv(a))
			return false;
		aff = aff | a;
	}
	return true;
}

bool NetworkCoordinatorClient::recv(CTYPE& ctype) {
	return _conn.read((uint8_t&)ctype);
}

bool NetworkCoordinatorClient::recv(ctype_t& ctype) {
	uint8_t sz;
	if (!_conn.read(sz))
		return false;
	for (uint8_t i = 0; i < sz; i++) {
		CTYPE c;
		if (!recv(c))
			return false;
		ctype = ctype | c;
	}
	return true;
}

bool NetworkCoordinatorClient::send(const ATYPE& atype) {
	return _conn.write(static_cast<uint8_t>(atype));
}

bool NetworkCoordinatorClient::recv(ATYPE& atype) {
	return _conn.read((uint8_t&)atype);
}

bool NetworkCoordinatorClient::recv(atype_t& atype) {
	uint8_t sz;
	if (!_conn.read(sz))
		return false;
	for (uint8_t i = 0; i < sz; i++) {
		ATYPE a;
		if (!recv(a))
			return false;
		atype = atype | a;
	}
	return true;
}

bool NetworkCoordinatorClient::send_null_card() {
	return _conn.write(static_cast<uint16_t>(UINT16_MAX));
}

bool NetworkCoordinatorClient::send(const shared_ptr<ICard> ic) {
	return _conn.write(ic->index);
}

bool NetworkCoordinatorClient::recv(shared_ptr<ICard>& ic) {
	uint16_t index;
	if (!_conn.read(index))
		return false;
	if (index >= _d.size())
		ic = ICard::blank_card();
	else
		ic = _d[index];
	return true;
}

bool NetworkCoordinatorClient::send(const icardpack& icards) {
	if (!_conn.write_size(icards))
		return false;
	for (auto& ic : icards) {
		if (!send(ic))
			return false;
	}
	return true;
}

bool NetworkCoordinatorClient::recv(icardpack& icards) {
	uint8_t size;
	if (!_conn.read(size))
		return false;
	icards.clear();
	for (uint8_t i = 0; i < size; i++) {
		shared_ptr<ICard> ic;
		if (!recv(ic))
			return false;
		icards.push_back(ic);
	}
	return true;
}

bool NetworkCoordinatorClient::send_null_player() {
	return _conn.write(static_cast<uint8_t>(UINT8_MAX));
}

bool NetworkCoordinatorClient::send(const shared_ptr<IPlayer> p) {
	if (p == nullptr)
		return send_null_player();
	return _conn.write(p->index);
}

bool NetworkCoordinatorClient::recv(shared_ptr<IPlayer> &p) {
	uint8_t index;
	if (!_conn.read(index))
		return false;
	if (index >= _ps.size())
		p = nullptr;
	else
		p = _ps[index];
	return true;
}

bool NetworkCoordinatorClient::recv(vector<shared_ptr<IPlayer>>& players) {
	uint8_t size;
	if (!_conn.read(size))
		return false;
	for (uint8_t i = 0; i < size; i++) {
		shared_ptr<IPlayer> p;
		if (!recv(p))
			return false;
		players.push_back(p);
	}
	return true;
}

bool NetworkCoordinatorClient::getPlayers() {
	// send name?
	uint8_t index;
	if (!_conn.read(index))
		return false;
	uint8_t nump;
	if (!_conn.read(nump))
		return false;
	for (uint8_t i = 0; i < nump; i++) {
		shared_ptr<IPlayer> p = make_shared<IPlayer>();
		if (!_conn.read(p->name))
			return false;
		if (!_conn.read(p->health))
			return false;
		p->index = i;
		if (p->index == index)
			_p = p;
		_ps.push_back(p);
	}

	uint16_t numc;
	if (!_conn.read(numc))
		return false;
	for (uint16_t i = 0; i < numc; i++) {
		shared_ptr<ICard> ic = make_shared<ICard>();
		ic->index = i;
		_conn.read(ic->ci.name);
		for (auto& line : ic->ci.desc)
			_conn.read(line);
		for (auto& line : ic->ci.flavtxt)
			_conn.read(line);
		recv(ic->ci.aff);
		recv(ic->ci.ctype);
		recv(ic->ci.atype);
		ic->loc = nullptr;
		_d.push_back(ic);
	}
	return true;
}

bool NetworkCoordinatorClient::handle_message() {
	Message msg;
	uint32_t id;
	if (!recv(msg, id))
		return false;

	switch (msg) {
	case Message::TURN_START:
		turn_start();
		break;
	case Message::NEW_HANDS:
		new_hands();
		break;
	case Message::DREW_CARD:
		drew_card();
		break;
	case Message::CARD_PLAYED:
		card_played();
		break;
	case Message::CARD_MOVED:
		card_moved();
		break;
	case Message::CARD_REVEALED:
		card_revealed();
		break;
	case Message::CARD_DISCARDED:
		card_discarded();
		break;
	case Message::MUNDANE_DISCARD:
		mundane_discard();
		break;
	case Message::ATTEMPT:
		attempt();
		break;
	case Message::PLAYER_HIT:
		player_hit();
		break;
	case Message::PLAYER_HEALED:
		player_healed();
		break;
	case Message::PLAYER_DEAD:
		player_dead();
		break;
	case Message::GAME_END:
		game_end();
		return false; // XXX set some flag to differentiate from error?
	case Message::TIMER:
		timer(id);
		break;
	case Message::SELECT_CARD:
		select_card(id);
		break;
	case Message::SELECT_MULTI_CARDS:
		select_multi_cards(id);
		break;
	case Message::SELECT_PLAYER:
		select_player(id);
		break;
	case Message::SELECT_ATYPE:
		select_atype(id);
		break;
	default:
		return false;
	}

	return true;
}

static inline std::ostream& operator<< (std::ostream& o, const shared_ptr<IPlayer> p) {
	return o << (p ? p->name : "nil");
}

static inline std::ostream& operator<< (std::ostream& o, const shared_ptr<ICard> ic) {
	return o << ic->ci.name << " [" << (ic->index + 1) << "]";
}

void NetworkCoordinatorClient::turn_start() {
	assertFalse(_ps.empty());
	shared_ptr<IPlayer> curp;
	recv(curp);
	cout << curp << "'s turn" << endl;
	print_players();
}

void NetworkCoordinatorClient::new_hands() {
	uint8_t sz;
	_conn.read(sz);
	for (uint8_t i = 0; i < sz; i++) {
		shared_ptr<IPlayer> p;
		recv(p);
		if (p == _p) {
			for (auto& c : _p->hand) {
				c->loc = nullptr;
			}
			recv(_p->hand);
			for (auto& c : _p->hand) {
				c->loc = &_p->hand;
			}
			cout << _p << " received a new hand" << endl;
		}
		else {
			p->hand.clear();
			uint8_t handsz;
			_conn.read(handsz);
			for (uint8_t j = 0; j < handsz; j++) {
				p->hand.push_back(ICard::blank_card());
			}
		}
	}
}

static inline void remove_icard(icardpack& icards, const shared_ptr<ICard> ic) {
	icards.erase(std::remove(icards.begin(), icards.end(), ic));
}

void NetworkCoordinatorClient::drew_card() {
	shared_ptr<IPlayer> p;
	shared_ptr<ICard> ic;
	recv(p);
	recv(ic);
	if (ic->loc)
		remove_icard(*ic->loc, ic);
	p->hand.push_back(ic);
	if (!ic->blank())
		ic->loc = &p->hand;

	if (p == _p) {
		assertFalse(ic->blank());
		cout << p << " drew " << ic << endl;
	}
}

void NetworkCoordinatorClient::card_played() {
	shared_ptr<IPlayer> from, to;
	shared_ptr<ICard> ic;
	recv(from);
	recv(to);
	recv(ic);
	if (ic->loc) {
		remove_icard(*ic->loc, ic);
		ic->loc = nullptr;
	} else {
		from->hand.pop_back();
	}

	cout << from << " played ";
	if (ic->ci.ctype & CTYPE::PRNK)
		cout << "a prank";
	else
		cout << ic;
	if (to)
		cout << " on " << to;
	cout << endl;
}

void NetworkCoordinatorClient::card_moved() {
	shared_ptr<IPlayer> from, to;
	shared_ptr<ICard> icfrom, icto;
	uint8_t loccode;
	recv(from);
	recv(to);
	recv(icfrom);
	recv(icto);
	_conn.read(loccode);
	Cardloc loc = static_cast<Cardloc>(loccode);

	if (icto->loc)
		remove_icard(*icto->loc, icto);

	icardpack *icp = nullptr;
	switch (loc) {
	case Cardloc::HAND:
		icp = &to->hand;
		break;
	case Cardloc::ITEM:
		icp = &to->items;
		break;
	case Cardloc::FIELD:
		icp = &_f->fields;
		break;
	case Cardloc::PRANK:
		icp = &_f->pranks;
		break;
	case Cardloc::OTHER:
		icp = &to->other;
		break;
	}
	icp->push_back(icto);
	if (!icto->blank())
		icto->loc = icp;
}

void NetworkCoordinatorClient::card_revealed() {
	shared_ptr<ICard> ic;
	uint8_t loccode;
	recv(ic);
	_conn.read(loccode);
	Cardloc loc = static_cast<Cardloc>(loccode);

	if (loc != Cardloc::PRANK)
		return; // XXX
	if (!found(_f->pranks, ic)) {
		ic->loc = &_f->pranks;
		const auto& iter = find_if(_f->pranks, [](shared_ptr<ICard> ic) {
			return ic->blank();
		});
		*iter = ic;
	}
}

void NetworkCoordinatorClient::card_discarded() {
	shared_ptr<IPlayer> pfrom, pto;
	shared_ptr<ICard> icfrom, icto;
	recv(pfrom);
	recv(pto);
	recv(icfrom);
	recv(icto);

	if (icto->loc)
		remove_icard(*icto->loc, icto);
	// XXX check that icto isn't blank
	_f->discard.push_back(icto);
	icto->loc = &_f->discard;
}

void NetworkCoordinatorClient::mundane_discard() {
	shared_ptr<ICard> ic;
	recv(ic);

	// XXX there should always be a loc
	if (ic->loc) // so this should always be true
		remove_icard(*ic->loc, ic);
	// XXX check that ic isn't blank
	_f->discard.push_back(ic);
	ic->loc = &_f->discard;
}

void NetworkCoordinatorClient::attempt() {
	shared_ptr<IPlayer> pfrom, pto;
	shared_ptr<ICard> c;
	ETYPE etype;
	recv(pfrom);
	recv(pto);
	recv(c);
	_conn.read((uint8_t&)etype);

	int16_t hit;
	switch (etype) {
	case ETYPE::ATTACK:
		_conn.read(hit);
		if (pfrom && pto)
			cout << pfrom << " attacking " << pto << " for " << hit << " damage" << endl;
		else if (!pfrom && pto)
			cout << pto << " is being attacked by " << c << " for " << hit << " damage" << endl;
		break;
	case ETYPE::HEAL:
		_conn.read(hit);
		if (pfrom && pfrom == pto)
			cout << pfrom << " healing for " << hit << endl;
		else if (pfrom && pfrom != pto)
			cout << pfrom << " healing " << pto << " for  " << hit << endl;
		else if (!pfrom && pto)
			cout << pto << " is getting healed by " << c << " for " << hit << endl;
		break;
	default:
		// do nothing
		break;
	}
}

void NetworkCoordinatorClient::player_hit() {
	shared_ptr<IPlayer> p;
	int16_t hit;
	recv(p);
	_conn.read(hit);

	p->health -= hit;

	cout << p << " took " << hit << " damage" << endl;
	cout << p << ": " << p->health << endl;
}

void NetworkCoordinatorClient::player_healed() {
	shared_ptr<IPlayer> p;
	int16_t hit;
	recv(p);
	_conn.read(hit);

	p->health += hit;

	cout << p << " healed " << hit << endl;
	cout << p << ": " << p->health << endl;
}

void NetworkCoordinatorClient::player_dead() {
	shared_ptr<IPlayer> p;
	recv(p);

	cout << p << " died" << endl;
}

void NetworkCoordinatorClient::game_end() {
	cout << "Game Over" << endl;
}

void NetworkCoordinatorClient::print_players() {
	int i = 1;
	for (auto& p : _ps) {
		cout << "  [" << i++ << "] " << p << ": ";
		if (p->health <= 0)
			cout << "dead";
		else
			cout << p->health;
		cout << endl;
	}
}

void NetworkCoordinatorClient::print_hand() {
	for (auto& ic : _p->hand) {
		cout << "  " << ic << endl;
	}
}

void NetworkCoordinatorClient::print_items() {
	for (auto& p : _ps) {
		cout << "  " << p << endl;
		if (p->items.empty())
			cout << "    no items" << endl;
		else for (auto& ic : p->items) {
			cout << "    " << ic << endl;
		}
	}
}

void NetworkCoordinatorClient::print_field() {
	if (_f->fields.empty())
		cout << "  no field" << endl;
	else for (auto& ic : _f->fields) {
		cout << "  " << ic << endl;
	}
}

void NetworkCoordinatorClient::print_pranks() {
	if (_f->pranks.empty())
		cout << "  no pranks" << endl;
	else for (auto& ic : _f->pranks) {
		cout << "  " << ic << endl;
	}
}

void NetworkCoordinatorClient::show_card(uint16_t index) {
	if (index >= _d.size()) {
		cout << "Not a card" << endl;
		return;
	}
	shared_ptr<ICard> ic = _d[index];
	cout << ic->ci.name << endl;
	cout << "  ";
	if (ic->ci.aff & AFF::S)
		cout << "S";
	if (ic->ci.aff & AFF::M)
		cout << "M";
	if (ic->ci.aff & AFF::A)
		cout << "A";
	if (ic->ci.aff & AFF::C)
		cout << "C";
	if (ic->ci.aff & AFF::I)
		cout << "!";
	if (!ic->ci.aff)
		cout << "no affiliation";
	cout << endl;
	cout << "  ";
	if (ic->ci.ctype & CTYPE::ATCK) {
		cout << "Attack: ";
		if (ic->ci.atype & ATYPE::PHYSICAL)
			cout << "Physical";
		if (ic->ci.atype & ATYPE::VERBAL)
			cout << "Verbal";
		if (ic->ci.atype & ATYPE::MENTAL)
			cout << "Mental";
	}
	if (ic->ci.ctype & CTYPE::CNTR) {
		cout << "Counter: ";
		if (ic->ci.atype & ATYPE::PHYSICAL)
			cout << "Physical";
		if (ic->ci.atype & ATYPE::VERBAL)
			cout << "Verbal";
		if (ic->ci.atype & ATYPE::MENTAL)
			cout << "Mental";
	}
	if (ic->ci.ctype & CTYPE::HEAL)
		cout << "Heal";
	if (ic->ci.ctype & CTYPE::ITEM)
		cout << "Item";
	if (ic->ci.ctype & CTYPE::FFLD)
		cout << "Field";
	if (ic->ci.ctype & CTYPE::SPCL)
		cout << "Special";
	if (ic->ci.ctype & CTYPE::PRNK)
		cout << "Prank";
	cout << endl;
	for (auto& line : ic->ci.desc) {
		if (!line.empty())
			cout << "  " << line << endl;
	}
	for (auto& line : ic->ci.flavtxt) {
		if (!line.empty())
			cout << "  [" << line << "]" << endl;
	}
	// TODO aff, ctype, atype
}

void NetworkCoordinatorClient::timer(uint32_t id) {
	icardpack icards;
	recv(icards);
	cout << "Select a card (timed):" << endl;
	for (auto& ic : icards) {
		cout << "  " << ic << endl;;
	}
	string input;
	int val;
	int sz = _d.size();
	for (;;) {
		cout << ">> ";
		getline(cin, input);
		if (input.empty()) {
			cout << "Select a card (timed):" << endl;
			for (auto& ic : icards) {
				cout << "  " << ic << endl;;
			}
			continue;
		}
		if (!handle_input(input, val))
			continue;
		else if (val >= sz) {
			cout << "invalid card" << endl;
			continue;
		} else
			break;
	}

	send(Message::TIMER, id);
	if (val < 0)
		send_null_card();
	else
		send(_d[val]);
}

void NetworkCoordinatorClient::select_card(uint32_t id) {
	assertFalse(_d.empty());
	bool passable;
	_conn.read(passable);
	icardpack icards;
	recv(icards);
	cout << "Select a card:" << endl;
	for (auto& ic : icards) {
		cout << "  " << ic << endl;;
	}
	string input;
	int val;
	int sz = _d.size();
	for (;;) {
		cout << ">> ";
		getline(cin, input);
		if (input.empty()) {
			cout << "Select a card:" << endl;
			for (auto& ic : icards) {
				cout << "  " << ic << endl;;
			}
			continue;
		}
		if (!handle_input(input, val))
			continue;
		else if (val >= sz) {
			cout << "invalid card" << endl;
			continue;
		} else if (val < 0 && !passable) {
			cout << "cannot pass" << endl;
			continue;
		} else
			break;
	}

	send(Message::SELECT_CARD, id);
	if (val < 0)
		send_null_card();
	else
		send(_d[val]);
}

void NetworkCoordinatorClient::select_multi_cards(uint32_t id) {
	bool passable;
	icardpack icards;
	_conn.read(passable);
	recv(icards);

	if (passable)
		cout << "Select zero or more of the following:" << endl;
	else
		cout << "Select one or the more of the following:" << endl;
	for (auto& ic : icards) {
		cout << "  " << ic << endl;
	}

	string input;
	vector<int> vals;
	int sz = _d.size();
	for (;;) {
		cout << ">> ";
		getline(cin, input);
		if (input.empty()) {
			if (passable)
				cout << "Select zero or more of the following:" << endl;
			else
				cout << "Select one or the more of the following:" << endl;
			for (auto& ic : icards) {
				cout << "  " << ic << endl;
			}
			continue;
		}
		if (!handle_input(input, vals))
			continue;
		else if (vals.empty() && !passable) {
			cout << "must select at least one card" << endl;
			continue;
		} else {
			for (auto& val : vals) {
				if (val >= sz) {
					cout << "invalid card" << endl;
					continue;
				}
			}
		}
		break;
	}

	send(Message::SELECT_MULTI_CARDS, id);
	icardpack selected;
	for (auto& val : vals) {
		selected.push_back(_d[val]);
	}
	send(selected);
}

void NetworkCoordinatorClient::select_player(uint32_t id) {
	vector<shared_ptr<IPlayer>> players;
	bool passable;
	_conn.read(passable);
	recv(players);
	cout << "Select a player:" << endl;
	for (auto& p : players) {
		cout << "  [" << (p->index + 1) << "] " << p << endl;
	}
	string input;
	int val;
	int sz = _ps.size();
	for (;;) {
		cout << ">> ";
		getline(cin, input);
		if (input.empty()) {
			cout << "Select a player:" << endl;
			for (auto& p : players) {
				cout << "  [" << (p->index + 1) << "] " << p << endl;
			}
			continue;
		}
		if (!handle_input(input, val))
			continue;
		else if (val >= sz) {
			cout << "invalid player" << endl;
			continue;
		} else if (val < 0 && !passable) {
			cout << "cannot pass" << endl;
			continue;
		} else
			break;
	}

	send(Message::SELECT_PLAYER, id);
	if (val < 0)
		send_null_player();
	else
		send(_ps[val]);
}

void NetworkCoordinatorClient::select_atype(uint32_t id) {
	bool passable;
	_conn.read(passable);
	cout << "Select an attack type:" << endl;
	cout << "  [1] Physical" << endl;
	cout << "  [2] Verbal" << endl;
	cout << "  [3] Mental" << endl;
	string input;
	int val;
	int sz = 3;
	for (;;) {
		cout << ">> ";
		getline(cin, input);
		if (input.empty()) {
			cout << "Select an attack type:" << endl;
			cout << "  [1] Physical" << endl;
			cout << "  [2] Verbal" << endl;
			cout << "  [3] Mental" << endl;
			continue;
		}
		if (!handle_input(input, val))
			continue;
		else if (val >= sz) {
			cout << "invalid type" << endl;
			continue;
		} else if (val < 0 && !passable) {
			cout << "cannot pass" << endl;
			continue;
		} else
			break;
	}

	send(Message::SELECT_ATYPE, id);
	if (val < 0)
		send(ATYPE::NONE);
	else if (val == 0)
		send(ATYPE::PHYSICAL);
	else if (val == 1)
		send(ATYPE::VERBAL);
	else if (val == 2)
		send(ATYPE::MENTAL);
}

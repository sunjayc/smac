#include "deck.h"

#include <algorithm>

#include "asserts.h"
#include "card.h"
#include "play_structures.h"

deck::deck(Player *owner) : _hasmax(false), _owner(owner), _is_hand(false) { }
deck::deck(Player *owner, uint16_t max, bool is_hand) :
	_hasmax(true),
	_max(max),
	_owner(owner),
	_is_hand(is_hand)
{ }

void deck::max(uint16_t max) {
	_max = max;
	if (_is_hand)
		_owner->draw();
}

void deck::hasmax(bool hasmax) {
	_hasmax = hasmax;
	if (_hasmax)
		assertLessThanEqual(this->size(), _max);
}

void deck::push_back(Card *c) {
	if (_hasmax && !_is_hand) {
		assertNotEqual(this->size(), _max);
	}

	deck *where = c->_where;
	if (where) {
		if (where == this)
			return;
		auto cp = std::find(where->begin(), where->end(), c);
		assertNotEqual(cp, where->end());
		where->erase(cp);
		if (where->_is_hand) {
			// if where is a player hand, immediately redraw
			where->_owner->draw();
		}
	}

	c->_where = this;
	c->attach(_owner);
	deck_base::push_back(c);
}

void deck::copy_back(const cardpack &rhs) {
	if (_hasmax) {
		assertLessThanEqual(this->size() + rhs.size(), _max);
	}
	for (auto& c : rhs) {
		push_back(c);
	}
}

void deck::slurp(deck &rhs) {
	if (&rhs == this)
		return;
	for (auto& c : rhs) {
		assertEqual(c->_where, &rhs);
		c->_where = this;
		c->attach(_owner);
	}
	deck_base::insert(this->end(), rhs.begin(), rhs.end());
	rhs.deck_base::clear();
}

void deck::swap(deck &rhs) {
	if (&rhs == this)
		return;
	for (auto& c : rhs) {
		assertEqual(c->_where, &rhs);
		c->_where = this;
		c->attach(_owner);
	}
	for (auto& c : *this) {
		assertEqual(c->_where, this);
		c->_where = &rhs;
		c->attach(rhs._owner);
	}
	deck_base::swap(rhs);
}

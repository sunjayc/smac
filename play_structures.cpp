#include "play_structures.h"

#include <algorithm>

#include "asserts.h"
#include "card.h"
#include "cardpack.h"
#include "coordinator.h"
#include "engine.h"

using std::vector;

Player::Player(Playfield *f, Coordinator *coord) :
	removable_turnstart_fns(f),
	turnstart_fns(f),
	damage_fns(f),
	_health(f->shealth),
	hand(this, f->shmax, true),
	item(this, f->simax),
	ally(this),
	enmy(this),
	othr(this),
	param_adjustments(f),
	f(f),
	coord(coord)
{
	assertNotNull(f);
	assertNotNull(coord);
}

Attack Player::damage(const Attack& a) {
	Evlist evlist;
	Attack final = a;
	for (auto& fn : damage_fns) {
		fn.second(final);
	}
	if (!final.annulled) {
		_health -= final.hit;
		coord->player_hit(this, final.hit);
	}
	return final;
}

Heal Player::heal(const Heal& h) {
	int16_t hit = h.hit;
	_health += hit;
	coord->player_healed(this, hit);
	return h;
}

void Player::draw() {
	while (hand.size() < hand.max()) {
		if (f->draw.empty()) {
			f->draw.swap(f->discard);
			std::shuffle(f->draw.begin(), f->draw.end(), _engine->_rng);
		}
		assertFalse(f->draw.empty());
		Card *c = f->draw.back();
		hand.push_back(c);
		coord->drew_card(this, c);
	}
}

void Playfield::set_cards(const cardpack& d) {
	_d = d;
	uint16_t i = 0;
	for (auto& c : _d) {
		c->index(i++);
		draw.push_back(c);
	}
}

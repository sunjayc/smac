#ifndef _CARD_H
#define _CARD_H

#include <functional>
#include <list>
#include <string>
#include <vector>

#include "asserts.h"
#include "cardpack.h"
#include "card_flags.h"
#include "event.h"
#include "flagset.h"

class deck;
template <typename F>
class hooklist;
class Player;

struct CardInfo
{
	std::string name; // Name of card (34 chars)
	std::string desc[4]; // Description (4x32 chars)
	std::string flavtxt[2]; // Flavor text (2x32 chars)
	aff_t aff; // SMAC! affiliation
	ctype_t ctype; // Card type(s)
	atype_t atype; // Attack/Counter type (optional)
};

class Card {
public:
	typedef std::function<bool()> canplaymainfn_t;
	typedef std::function<Event(Player *,Card*,const Event&,cardpack&)> playfn_t;
	typedef std::function<bool(const Event&)> cancntrfn_t;
	typedef std::function<Evlist()> discardfn_t;
	typedef std::function<bool(const Event&)> canplaynowfn_t;

	Card(const CardInfo& cinfo, const playfn_t play_fn) :
		ci(cinfo),
		play_fn(play_fn),
		_p(nullptr),
		_where(nullptr),
		_playable_type()
	{
		assertLessThanEqual(ci.name.size(), 34);
		assertLessThanEqual(ci.desc[0].size(), 32);
		assertLessThanEqual(ci.desc[1].size(), 32);
		assertLessThanEqual(ci.desc[2].size(), 32);
		assertLessThanEqual(ci.desc[3].size(), 32);
		assertLessThanEqual(ci.flavtxt[0].size(), 32);
		assertLessThanEqual(ci.flavtxt[1].size(), 32);
	}
	Card& operator=(const Card&) = delete;
	~Card() { }

	void index(uint16_t index) { _index = index; }
	uint16_t index() const { return _index; }

	Card* playable_type(ctype_t ctype) { _playable_type = ctype; return this; }
	Card* playable_fn(canplaymainfn_t can_play_main_fn) { _can_play_main_fn = can_play_main_fn; return this; }
	Card* can_counter_fn(cancntrfn_t can_counter_fn) { _can_counter_fn = can_counter_fn; return this; }
	Card* discard_fn(discardfn_t discard_fn) { _discard_fn = discard_fn; return this; }
	Card* can_play_now_fn(canplaynowfn_t can_play_now_fn) { _can_play_now_fn = can_play_now_fn; return this; }

	ctype_t playable_type() const {
		if (_playable_type)
			return _playable_type;
		return ci.ctype;
	}
	bool can_play_main() const {
		if (_can_play_main_fn)
			return _can_play_main_fn();
		ctype_t ctype = _playable_type ? _playable_type : ci.ctype;
		return ctype & (CTYPE::ATCK | CTYPE::HEAL | CTYPE::ITEM | CTYPE::FFLD | CTYPE::PRNK);
	}
	bool can_counter(const Event& ev) const {
		assertEqual(ev.etype, ETYPE::ATTACK);
		if (_can_counter_fn)
			return _can_counter_fn(ev);
		return (ci.atype & ev.a.atype);
	}
	Evlist discard_fn() {
		if (!_discard_fn)
			return Evlist();
		return _discard_fn();
	}
	bool can_play_now(const Event& ev) const {
		if (_can_play_now_fn)
			return _can_play_now_fn(ev);
		return false;
	}

	Player* player() const { return _p; }

	const CardInfo ci;
	const playfn_t play_fn;


	bool _facedown = false;

	// card specific traits

	enum class Trait {
		NONE,
		STARE,
		BOTHER,
		YOUR_MOM,
		NO_YOUR_MOM,
		PARSONS_CLASS,
	};
	Trait _trait = Trait::NONE;
	Card* set_trait(Trait trait) { _trait = trait; return this; }

	int16_t _val_int16;
	Card* set_val(int16_t val) { _val_int16 = val; return this; }

	uint16_t _val_uint16;

	atype_t _val_atype = ATYPE::NONE;
protected:
	Player *_p;
	deck *_where;
	uint16_t _index;

	void attach(Player *p) { _p = p; }

	ctype_t _playable_type;
	canplaymainfn_t _can_play_main_fn;

	cancntrfn_t _can_counter_fn;
	discardfn_t _discard_fn;
	canplaynowfn_t _can_play_now_fn;

	friend class deck;
	template <typename F>
	friend class hooklist;
	friend void test_card_movement();
};

#endif // #ifndef _CARD_H

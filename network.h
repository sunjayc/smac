#ifndef _NETWORK_H
#define _NETWORK_H

#include <cinttypes>
#include <memory>
#include <vector>
#include <string>

#ifdef WIN32
#include <WinSock2.h>
#include <WS2tcpip.h>
#endif // WIN32

class Socket
{
public:
	virtual ~Socket() { }
	virtual bool invalid() { return _invalid || !_connected; }

	Socket(const Socket&) = delete;
	Socket& operator= (const Socket&) = delete;

	virtual bool connectServer(std::string hostname, unsigned short port) = 0;
	virtual bool read(void* buf, uint32_t n) = 0;
	virtual bool write(const void* buf, uint32_t n) = 0;
protected:
	Socket() : _invalid(true), _connected(false) { }

	bool _invalid;
	bool _connected;
};

class ServerSocket
{
public:
	virtual ~ServerSocket() { }
	virtual bool invalid() { return _invalid; }

	ServerSocket(const ServerSocket&) = delete;
	ServerSocket& operator= (const ServerSocket&) = delete;

	// caller takes ownership of returned Socket*
	virtual Socket* acceptClient() = 0;
protected:
	ServerSocket() : _invalid(true) { }

	bool _invalid;
};

#ifdef __linux__
class BerkeleySocketBase
{
protected:
	BerkeleySocketBase() { }
	BerkeleySocketBase(int fd) : _sockfd(fd) { }
	int _sockfd;
};

class BerkeleySocket : public Socket, public BerkeleySocketBase
{
public:
	BerkeleySocket() { };
	virtual ~BerkeleySocket() override;

	virtual bool connectServer(std::string hostname, unsigned short port) override;

	virtual bool read(void* buf, uint32_t n) override;
	virtual bool write(const void* buf, uint32_t n) override;
protected:
	BerkeleySocket(int fd) : BerkeleySocketBase(fd) { _invalid = false; _connected = true; }
	friend class BerkeleyServerSocket;
};

class BerkeleyServerSocket : public ServerSocket, public BerkeleySocketBase
{
public:
	BerkeleyServerSocket(std::string port);
	virtual ~BerkeleyServerSocket() override;

	virtual Socket* acceptClient() override;
protected:
};

typedef BerkeleySocket OSSocket;
typedef BerkeleyServerSocket OSServerSocket;
#endif // __linux__

#ifdef WIN32
class WinsockInitializer {
public:
	WinsockInitializer();
	~WinsockInitializer();
};

class WindowsSocketBase {
protected:
	WindowsSocketBase() { }
	WindowsSocketBase(SOCKET sock) : _sock(sock) { }
	SOCKET _sock;
private:
	static WinsockInitializer _winsockInit;
};

class WindowsSocket : public Socket, public WindowsSocketBase
{
public:
	WindowsSocket() { }
	virtual ~WindowsSocket() override;

	virtual bool connectServer(std::string hostname, unsigned short port) override;
	virtual bool read(void* buf, uint32_t n) override;
	virtual bool write(const void* buf, uint32_t n) override;
protected:
	WindowsSocket(SOCKET sock) : WindowsSocketBase(sock) { _invalid = false; _connected = true; }
	friend class WindowsServerSocket;
};

class WindowsEventSocket : public WindowsSocket
{
public:
	WindowsEventSocket(SOCKET sock);
	virtual ~WindowsEventSocket();

	virtual bool read(void* buf, uint32_t n) override;
	virtual bool read(void* buf, uint32_t n, unsigned long timeout);

	static std::vector<WindowsEventSocket*> wait_for_read(const std::vector<WindowsEventSocket*>& wesocks, unsigned long timeout = WSA_INFINITE);
protected:
	WSAEVENT hEvent;
};

class WindowsServerSocket : public ServerSocket, public WindowsSocketBase
{
public:
	WindowsServerSocket(std::string port);
	virtual ~WindowsServerSocket() override;

	// return an async socket
	virtual WindowsEventSocket* acceptClient() override;
};

typedef WindowsSocket OSSocket;
typedef WindowsServerSocket OSServerSocket;
#endif // WIN32

void test_server();
void test_client();

class NetworkPacker {
public:
	NetworkPacker(Socket *sock) : _sock(sock) { }
	NetworkPacker(std::shared_ptr<Socket> sock) : _sock(sock) { }

	bool sendVer(uint8_t ver);
	bool getVer(uint8_t ver);

	// strings are encoded as (uint8_t sz) followed by (char s[sz])
	bool read(std::string& str);
	bool write(const std::string& str);

	// sent as single byte, no encoding
	bool read(uint8_t& val);
	bool write(const uint8_t val);
	template <typename T> bool write_size(const T& container) { return write(static_cast<uint8_t>(container.size())); }

	// sent as single byte: false <=> 0, true <=> nonzero
	bool read(bool& val);
	bool write(const bool val);

	// encoded in network byte order
	bool read(int16_t& val);
	bool write(const int16_t val);
	bool read(uint16_t& val);
	bool write(const uint16_t val);
	bool read(uint32_t& val);
	bool write(const uint32_t val);

	std::shared_ptr<Socket> _sock;
};

#endif // #ifndef _NETWORK_H

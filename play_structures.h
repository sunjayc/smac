#ifndef _PLAY_STRUCTURES_H
#define _PLAY_STRUCTURES_H

#include <list>
#include <unordered_map>

#include "card.h"
#include "cardpack.h"
#include "deck.h"
#include "event.h"
#include "flagset.h"

class Coordinator;
class Engine;

/**
 * Server side main playing field data.
 */
class Playfield
{
public:
	Playfield() :
		draw(nullptr),
		discard(nullptr),
		flux(nullptr),
		field(nullptr, 1),
		pranks(nullptr),
		shealth(20),
		shmax(5),
		simax(2)
	{ }

	Playfield(const Playfield&) = delete;
	Playfield& operator= (const Playfield&) = delete;

	void set_cards(const cardpack& d);
	const cardpack& get_cards() const { return _d; }

	deck draw; // Cards in the draw pile
	deck discard; // Cards in the discard pile
	deck flux; // Cards in flux; everything here gets discarded at the end of each turn

	deck field; // Current field(s) in play
	deck pranks; // Current prank(s) in play

	short shealth; // Starting health
	short shmax; // Starting max number of cards in hand
	short simax; // Starting max number of items
private:
	cardpack _d; // Master deck
};

template <typename F>
class hooklist : private std::list<std::pair<Card*,F>> {
	typedef std::list<std::pair<Card*,F>> hooklist_base;

public:
	hooklist (Playfield *f) : f(f) { }

	void emplace_back(Card *c, F&& fn) {
		assertTrue(c->_where != &f->discard);
		hooklist_base::emplace_back(c, fn);
	}

	void remove(Card *c) {
		hooklist_base::remove_if([=](const typename hooklist_base::value_type& pair) {
			return pair.first == c;
		});
	}

	using hooklist_base::begin;
	using hooklist_base::end;
	using hooklist_base::remove_if;
	using hooklist_base::push_back;
	using hooklist_base::sort;
private:
	Playfield *f;
};

class Player
{
public:
	typedef std::function<Evlist()> turnstart_fn_t;
	typedef std::function<void(Attack&)> damagefn_t;
	typedef std::function<bool()> removable_turnstart_fn_t;

	Player(Playfield *f, Coordinator *coord);

	void attach(Engine *engine) { _engine = engine; }

	Player(const Player&) = delete;
	Player& operator= (const Player&) = delete;

	void index(uint8_t index) { _index = index; }
	uint8_t index() const { return _index; }

	hooklist<removable_turnstart_fn_t> removable_turnstart_fns; // for tag
	hooklist<turnstart_fn_t> turnstart_fns;

	Attack damage(const Attack& a);
	hooklist<damagefn_t> damage_fns;

	Heal heal(const Heal& h);

	int16_t _health;
	int16_t health() const { return _health; }
	bool dead() const { return health() <= 0; }

	void draw();

	std::string name;

	deck hand; // This player's hand
	deck item; // Items in play

	deck ally; // Allies attached to player
	deck enmy; // Enemies attached to player
	deck othr; // Anything else attached to player

	uint16_t skip = 0; // Any remaining turns to be skipped

	hooklist<Parameter> param_adjustments;

	// card specific things
	bool _la_la_land = false;
private:
	Playfield *f;
	Coordinator *coord;
	Engine *_engine;

	uint8_t _index;
};

#endif // #ifndef _PLAY_STRUCTURES_H

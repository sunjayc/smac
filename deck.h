#ifndef _DECK_H
#define _DECK_H

#include <cinttypes>
#include <vector>

#include "cardpack.h"

class Card;
class Player;
typedef std::vector<Card*> deck_base;

class deck : private deck_base {
public:
	deck(Player *owner);
	deck(Player *owner, uint16_t max, bool is_hand = false);

	cardpack get_cards() const {
		return static_cast<cardpack>(*this);
	}

	// parenthesized so macro expansion for "max" doesn't happen
	uint16_t (max)() const { return _max; }
	void (max)(uint16_t max);

	bool hasmax() const { return _hasmax; }
	void hasmax(bool hasmax);

	void push_back(Card *c);
	void copy_back(const cardpack &rhs);
	void slurp(deck &rhs);
	void swap(deck &rhs);

	using deck_base::begin;
	using deck_base::end;
	using deck_base::size;
	using deck_base::empty;
	using deck_base::back;
	using deck_base::operator[];
private:
	bool _hasmax;
	uint16_t _max;
	Player *_owner;
	bool _is_hand;
};

#endif // #ifndef _DECK_H

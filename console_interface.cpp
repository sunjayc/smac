#include <string>
#include <Windows.h>

#include "asserts.h"
#include "interface.h"
#include "play_structures.h"

class WinConsoleInterface : public Interface
{
public:
	WinConsoleInterface(int width, int height);
private:
	unsigned long cwrite(string, int x, int y, WORD attr, int clrlength);
	void card(CardInfo *c, COORD topleft, WORD color);

	static const int CARD_WIDTH = 38;
	static const int CARD_HEIGHT = 15;

	HANDLE hScr, hOldscr;
	CONSOLE_SCREEN_BUFFER_INFO csbiInfo;
	CONSOLE_CURSOR_INFO cciInfo;
	int WIDTH, HEIGHT;
};

using std::string;

unsigned long WinConsoleInterface::cwrite(string str, int x, int y, WORD attr, int clrlength)
{
	COORD coord;
	unsigned long lnum = 0;
	int len = str.length();

	coord.X = (short)x;
	coord.Y = (short)y;
	FillConsoleOutputCharacter(hScr, ' ', clrlength, coord, &lnum);
	WriteConsoleOutputCharacterA(hScr, str.c_str(), len, coord, &lnum);
	FillConsoleOutputAttribute(hScr, attr, len, coord, &lnum);

	return lnum;
}

WinConsoleInterface::WinConsoleInterface(int width, int height) :
	WIDTH(width),
	HEIGHT(height)
{
	hOldscr = GetStdHandle(STD_OUTPUT_HANDLE);
	hScr = CreateConsoleScreenBuffer(
		GENERIC_READ |           // read/write access
		GENERIC_WRITE,
		0,                       // not shared
		NULL,                    // default security attributes
		CONSOLE_TEXTMODE_BUFFER, // must be TEXTMODE
		NULL);                   // reserved; must be NULL

	csbiInfo.dwSize.X = WIDTH;
	csbiInfo.dwSize.Y = HEIGHT;
	csbiInfo.dwCursorPosition.X = 0;
	csbiInfo.dwCursorPosition.Y = 0;
	csbiInfo.wAttributes = 7;
	csbiInfo.srWindow.Left = 0;
	csbiInfo.srWindow.Top = 0;
	csbiInfo.srWindow.Right = WIDTH - 1;
	csbiInfo.srWindow.Bottom = HEIGHT - 1;
	csbiInfo.dwMaximumWindowSize.X = WIDTH;
	csbiInfo.dwMaximumWindowSize.Y = HEIGHT;
	cciInfo.bVisible = FALSE;
	cciInfo.dwSize = HEIGHT;

	SetConsoleScreenBufferSize(hScr, csbiInfo.dwSize);
	SetConsoleCursorPosition(hScr, csbiInfo.dwCursorPosition);
	SetConsoleTextAttribute(hScr, csbiInfo.wAttributes);
	SetConsoleWindowInfo(hScr, TRUE, &csbiInfo.srWindow);
	SetConsoleCursorInfo(hScr, &cciInfo);

	SetConsoleActiveScreenBuffer(hScr);

	return;
}

// Cards are 38 x 15
void WinConsoleInterface::card(CardInfo *c, COORD topleft, WORD color)
{
	int i, j;
	CHAR_INFO ci[CARD_WIDTH * CARD_HEIGHT];
	int len, left, right;
	COORD size, pos;	//		   1		 2		   3
	SMALL_RECT sm;	   //01234567890123456789012345678901234567
	char cd[CARD_HEIGHT][CARD_WIDTH + 1] = {	"旼컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴커", // 0
												"�                               SMAC!�", // 1 
												"�                Name                �", // 2
												"�                                    �", // 3 [Health for enemies or allies or whatever would go here]
												"� 旼컴컫컴컴컴컴컴컴컴컴컴컴컴쩡컴커 �", // 4
												"� �    �         Type         �    � �", // 5
												"� �    읕컴컴컴컴컴컴컴컴컴컴켸    � �", // 6
												"� �              Desc              � �", // 7
												"� �                                � �", // 8
												"� �                                � �", // 9
												"� �                                � �", // 10
												"� 읕컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴켸 �", // 11
												"�              FlavText              �", // 12
												"�                                    �", // 13
												"읕컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴켸"};// 14
	char pad[18] = "                 ";
	char buf[133] = "";

	size.X = CARD_WIDTH;
	size.Y = CARD_HEIGHT;
	pos.X = 0;
	pos.Y = 0;
	sm.Top = topleft.Y;
	sm.Left = topleft.X;
	sm.Bottom = topleft.Y + CARD_HEIGHT - 1;
	sm.Right = topleft.X + CARD_WIDTH - 1;

	if(c == NULL)
	{
		for(i = 0; i < 15; i++)
		{
			for(j = 0; j < 38; j++)
			{
				ci[i*38 + j].Char.AsciiChar = ' ';
				ci[i*38 + j].Attributes = 7;
			}
		}
		WriteConsoleOutputA(hScr, ci, size, pos, &sm);
		return;
	}

	cd[1][32] = (c->aff & AFF::S) ? 'S' : ' ';
	cd[1][33] = (c->aff & AFF::M) ? 'M' : ' ';
	cd[1][34] = (c->aff & AFF::A) ? 'A' : ' ';
	cd[1][35] = (c->aff & AFF::C) ? 'C' : ' ';
	cd[1][36] = (c->aff & AFF::I) ? '!' : ' ';

	len = c->name.length();
	right = (34 - len) / 2;
	left = (34 - len) - right;
	sprintf_s(cd[2], 39, "� %.*s%s%.*s �", left, pad, c->name, right, pad);

	/*if(c->type & (CTYPE::ENMY | CTYPE::ALLY) && c->health != 0)
	{
		sprintf_s(buf, 133, "Health: %d", c->health);
		len = strlen(buf);
		right = (34 - len) / 2;
		left = (34 - len) - right;
		sprintf_s(cd[3], 39, "� %.*s%s%.*s �", left, pad, buf, right, pad);
		buf[0] = '\0';
	}*/

	char atypes[][10] = {"General/", "Physical/", "Verbal/", "Mental/"};
	if(c->atype & (ATYPE::GENERAL | ATYPE::PHYSICAL | ATYPE::VERBAL | ATYPE::MENTAL))
	{
		if(c->atype & ATYPE::GENERAL)
			strcat_s(buf, 133, atypes[0]);
		if(c->atype & ATYPE::PHYSICAL)
			strcat_s(buf, 133, atypes[1]);
		if(c->atype & ATYPE::VERBAL)
			strcat_s(buf, 133, atypes[2]);
		if(c->atype & ATYPE::MENTAL)
			strcat_s(buf, 133, atypes[3]);
		buf[strlen(buf) - 1] = ' ';
	}

	char types[][10] = {"Attack/", "Counter/", "Item/", "Field/", "Heal/", "Special/", "Ally/", "Enemy/", "Prank/"};
	if(c->ctype & CTYPE::ATCK)
		strcat_s(buf, 133, types[0]);
	if(c->ctype & CTYPE::CNTR)
		strcat_s(buf, 133, types[1]);
	if(c->ctype & CTYPE::ITEM)
		strcat_s(buf, 133, types[2]);
	if(c->ctype & CTYPE::FFLD)
		strcat_s(buf, 133, types[3]);
	if(c->ctype & CTYPE::HEAL)
		strcat_s(buf, 133, types[4]);
	if(c->ctype & CTYPE::SPCL)
		strcat_s(buf, 133, types[5]);
	if(c->ctype & CTYPE::ALLY)
		strcat_s(buf, 133, types[6]);
	if(c->ctype & CTYPE::ENMY)
		strcat_s(buf, 133, types[7]);
	if(c->ctype & CTYPE::PRNK)
		strcat_s(buf, 133, types[8]);
	len = strlen(buf);
	if(len > 0)
	{
		len--;
		buf[len] = '\0';
	}
	right = (22 - len) / 2;
	left = (22 - len) - right;
	sprintf_s(cd[5], 39, "� �    �%.*s%s%.*s�    � �", left, pad, buf, right, pad);

	for(i = 0; i < 4; i++)
	{
		len = c->desc[i].length();
		right = (32 - len) / 2;
		left = (32 - len) - right;
		assertTrue(len <= 32);
		sprintf_s(cd[7 + i], 39, "� �%.*s%s%.*s� �", left, pad, c->desc[i], right, pad);
	}

	for(i = 0; i < 2; i++)
	{
		len = c->flavtxt[i].length();
		right = (32 - len) / 2;
		left = (32 - len) - right;
		assertTrue(len <= 32);
		sprintf_s(cd[12 + i], 39, "�  %.*s%s%.*s  �", left, pad, c->flavtxt[i], right, pad);
	}
	
	for(i = 0; i < CARD_HEIGHT; i++)
	{
		for(j = 0; j < CARD_WIDTH; j++)
		{
			ci[i*CARD_WIDTH + j].Char.AsciiChar = cd[i][j];
			ci[i*CARD_WIDTH + j].Attributes = color;
		}
	}

	WriteConsoleOutputA(hScr, ci, size, pos, &sm);
}
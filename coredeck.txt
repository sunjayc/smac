"Air guitar", DONE
"\n3", 
"I'm using the right fingerings.", 
AFF_M, 
C_CNTR, 0, 
TYPE_M, 3, 
NULL, NULL, NULL;

"Free Chocolate Milk", DONE
"\n+4", 
"It's free, chocolate, and milk!", 
AFF_A | AFF_C | AFF_I, 
C_PLAY | C_HEAL, ID_MILK, 
0, 4, 
NULL, NULL, NULL;

"I'm a superior being", DONE
"\n3", 
"inferior non-elf...", 
AFF_A, 
C_PLAY | C_ATCK, 0, 
TYPE_M, 3, 
NULL, NULL, NULL;

"ignore'd", DONE
"\n4", 
"Whatever...", 
AFF_A | AFF_C, 
C_CNTR, 0, 
TYPE_V, 4, 
NULL, NULL, NULL;

"Standing in front of a wall", DONE
"\n4", 
"grrr...", 
AFF_S | AFF_M | AFF_A | AFF_C | AFF_I, 
C_CNTR, 0, 
TYPE_P, 4, 
NULL, NULL, NULL;

"Textbook rap", DONE
"\n4", 
"\"beatboxing\"", 
AFF_S | AFF_M, 
C_PLAY | C_ATCK, 0, 
TYPE_V, 4, 
NULL, NULL, NULL;

"Your mom", DONE
"\n2", 
"both", 
AFF_S | AFF_M | AFF_A | AFF_C, 
C_PLAY | C_ATCK, ID_YMOM, 
TYPE_V, 2, 
NULL, NULL, NULL;

"BOOM", DONE
"\n5\nApplies to all players,\nincluding the user.", 
"Pretty colors...", 
AFF_S, 
C_PLAY | C_ATCK, 0, 
TYPE_P, 5, 
NULL, BoomFunc, NULL;

"extra arm", DONE
"you can now have one extra card in your hand. when this is discarded, you may only have the normal amount of cards in your hand ", 
"that one guy has like... 4", 
AFF_I, 
C_PLAY | C_ITEM, 0, 
0, 0, 
NULL, ExtraArmFunc, NULL;

"Oops I dropped it", DONE
"\nOne of your opponents must\ndiscard an item of your choice.", 
"uh... sorry", 
AFF_S, 
C_PLAY | C_SPCL, 0, 
0, 0, 
NULL, OopsIDroppedItFunc, NULL;

"Library", DONE
"\nverbal cards cannot be played", 
"SHHHHHHHHHHHHHHH...", 
AFF_S | AFF_M | AFF_A | AFF_C | AFF_I, 
C_PLAY | C_FFLD, 0, 
0, 0, 
NULL, LibraryFunc, NULL;

"Deaf", 
"verbal damage does not effect\nyou, but you can't reply to what\nanyone says unless you pretend\nto misshear them", 
"WHAT? TRUCK BLUE?", 
AFF_S | AFF_M | AFF_A | AFF_C | AFF_I, 
C_PLAY | C_ITEM, 0, 
0, 0, 
NULL, DeafFunc, NULL;

"big fat target", still working on it
"select a person. they must be\ntargeted except on their turn.\nthis lasts for 1 round", 
"don't go near an archery range", 
AFF_A | AFF_C | AFF_I, 
C_PLAY | C_SPCL, 0, 
0, 0, 
NULL, BigFatTargetFunc, NULL;

"Ranch in Milk", 
"HAARRHRBGARBBGHRAGRLVHGRHLGHBAGRHARHAHBLGHAHRGLAGLARGARHAHAHAAHRGRALBHGARHGAHRAHLRGLARBARLHLARGHRGARHGBARARLGBAABRLARLBHALALABHA", 
"Is that a cruton?", 
AFF_S | AFF_M, 
C_PLAY | C_PRNK, 0, 
0, 0, 
NULL, RanchInMilkFunc, NULL;

"Chair Stare", DONE [but not linked with other stares]
"\n2\nAll stare cards in your hand may be played.", 
"Must... focus... on... chair...", 
AFF_M, 
C_PLAY | C_ATCK, ID_STRE, 
TYPE_M, 2, 
NULL, ChairStareFunc, NULL;

"rolling bother", DONE [but not linked with other bothers]
"all bother cards in your hand can be played at once doing 2x the number of bother cards played minus 1", 
"it isn't the bother that is annoying... it's the shoes", 
AFF_A, 
C_PLAY | C_ATCK, ID_BTHR, 
TYPE_P, 0, 
NULL, RollingBotherFunc, NULL;

"No brainer", 
"you have no mind! mental attacks do nothing to you. lose this card if you act smart.", 
"duh... mental whats?", 
AFF_S | AFF_M | AFF_A | AFF_C | AFF_I, 
C_PLAY | C_ITEM, 0, 
0, 0, 
NULL, NoBrainerFunc, NULL;

"revenge", 
"whenever someone damages you, they take the same dammage as you", 
"what did you say about my mom?!?", 
AFF_S | AFF_I, 
C_PLAY | C_ITEM, 0, 
0, 0, 
NULL, RevengeFunc, NULL;

"Caution: low ceiling", 
"max cards cannot be played, unless the are also alex cards", 
"doncha wish your girlfriend was short like me?", 
AFF_A, 
C_PLAY | C_FFLD, 0, 
0, 0, 
NULL, CautionLowCeilingFunc, NULL;

"tag", 
"pick someone to be it. when it gets to their turn, they pick someone else to be it. whoever's it must be targeted", 
"no tag backs", 
AFF_S | AFF_M | AFF_A, 
C_PLAY | C_FFLD, 0, 
0, 0, 
NULL, TagFunc, NULL;





"Agree", 
"\nAttack's damage +1", 
"\"Screw you!\" \"If you want!\"", 
AFF_S | AFF_C, 
C_CNTR, 0, 
TYPE_M, 0, 
NULL, AgreeFunc, NULL;

"Amnesia dust", 
"\n3\nThe opponent loses\ntheir next turn", 
"\"You are Santa!\"", 
AFF_A, 
C_PLAY | C_ATCK, 0, 
TYPE_M, 3, 
NULL, AmnesiaDustFunc, NULL;

"Cow hat", 
"\n4", 
"It comes with built-in\ncup holders!", 
AFF_I, 
C_CNTR, 0, 
TYPE_M, 4, 
NULL, NULL, NULL;

"Crud Comix", 
"do 2 damage per Spencer\ncard and -1 per cody\ncard in the targets hand", 
"I... HATE...\nSTICK SONIC!!!", 
AFF_C, 
C_PLAY | C_ATCK, 0, 
TYPE_M, 0, 
NULL, CrudComixFunc, NULL;

"Free Milk", 
"\n+3", 
"\"Dibs on free milk!\"", 
AFF_A | AFF_C | AFF_I, 
C_PLAY | C_HEAL, ID_MILK, 
0, 3, 
NULL, NULL, NULL;

"No your mom", 
"\n3\n(4 if it counters your mom)", 
"No your mom, no your mom...", 
AFF_S | AFF_M | AFF_A | AFF_C, 
C_CNTR, ID_YMOM, 
TYPE_V, 3, 
NULL, NoYourMomFunc, NULL;

"yeah right", 
"\n3", 
"curse you for your tallness", 
AFF_M, 
C_CNTR, 0, 
TYPE_P, 3, 
NULL, NULL, NULL;

"Your Mom", 
"\n2", 
"You mom is not cool... 7", 
AFF_S | AFF_M | AFF_A | AFF_C, 
C_PLAY | C_ATCK, ID_YMOM, 
TYPE_V, 2, 
NULL, NULL, NULL;

"You have no limbs", 
"does 2 damage\nper Cody card\nand -1 per Spencer card.", 
"\"It's only a flesh wound!\"", 
AFF_S, 
C_PLAY | C_ATCK, 0, 
TYPE_V, 0, 
NULL, NoLimbsFunc, NULL;

"Bother machine gun", 
"all bother cards in your hand, the discard pile, and everyone elses hands get played on one person of your choice", 
"long distance rolling \"bother\"", 
AFF_M, 
C_PLAY | C_ATCK, ID_BTHR, 
TYPE_P, 0, 
NULL, BotherMachineGunFunc, NULL;

"Chocolate Cow", 
"Heals 2 every turn of the user.\nStarts when this card is used.\nLasts 3 heals.", 
"My grandma claims to have one.", 
AFF_A | AFF_C | AFF_I, 
C_PLAY | C_HEAL, 0, 
0, 2, 
NULL, ChocolateCowFunc, NULL;

"condesending", 
"3\n4 if opponent has 3 or more Alex cards", 
"\"high five!\"", 
AFF_M, 
C_PLAY | C_ATCK, 0, 
0, 3, 
NULL, CondesendingFunc, NULL;

"dumping area", 
"physical cards can't be played", 
"_dumping_ area! you smell like a crapping area!", 
AFF_S | AFF_M | AFF_C, 
C_PLAY | C_FFLD, 0, 
0, 0, 
NULL, DumpingAreaFunc, NULL;

"Folder maze", 
"Deals 1 uncounterable mental damage to any opponent on your turn.", 
"The folder lied to you.", 
AFF_M | AFF_A | AFF_C, 
C_PLAY | C_ITEM, 0, 
0, 0, 
NULL, FolderMazeFunc, NULL;

"Free milk steal", 
"If someone else plays a heal play this card and all effects go to you.", 
"Wait... you can just do that?", 
AFF_I, 
C_SPCL, 0, 
0, 0, 
FreeMilkStealDraw, NULL, NULL;

#include "coordinator.h"

#include <iostream>
#include <sstream>
#include "asserts.h"
#include "card.h"
#include "engine.h"

using std::cin;
using std::cout;
using std::endl;
using std::getline;
using std::make_shared;
using std::shared_ptr;
using std::string;
using std::vector;

template <typename T>
static bool get(std::istream& in, T& val) {
	string line;
	getline(in, line);
	int num;
	bool res = !!(std::stringstream(line) >> num);
	val = static_cast<T>(num);
	return res;
}

uint8_t BasicCoordinator::_retrieve_players(vector<shared_ptr<Player>>& ps, Playfield *f) {
	cout << "How many players?" << endl;
	uint8_t nump;
	get(cin, nump);
	for (uint8_t i = 0; i < nump; i++) {
		ps.push_back(make_shared<Player>(f, this));
	}

	int i = 1;
	for (auto& p : ps) {
		std::stringstream name;
		name << "Player " << i;
		p->name = name.str();
		i++;
	}

	return nump;
}

void BasicCoordinator::turn_start(const Player *curp) {
	cout << endl;
	for (auto& p : _ps) {
		cout << p->name << ": ";
		if (p->dead())
			cout << "dead";
		else
			cout << p->health() << " health";
		cout << endl;
	}
	cout << endl;
	cout << curp->name << "'s turn!" << endl;
	cout << endl;
}

void BasicCoordinator::drew_card(const Player *p, const Card *c) {
	(void)p;
	(void)c;
}

void BasicCoordinator::card_moved(const Player *from, const Player *to, const Card *cfrom, const Card *cto, Cardloc where) {
	(void)from;
	(void)to;
	(void)cfrom;
	(void)cto;
	(void)where;
}

void BasicCoordinator::card_played(const Player *from, const Player *to, const Card* c) {
	cout << from->name << " played " << c->ci.name << " on " << to->name << endl;
}

uint16_t BasicCoordinator::geti(size_t bigmax, bool passable) {
	assertLessThanEqual(bigmax, UINT16_MAX);
	uint16_t max = static_cast<uint16_t>(bigmax);
	uint16_t choice;
	do {
		std::string input;
		getline(cin, input);
		assertTrue(cin.good());
		if (!(std::stringstream(input) >> choice)) {
			choice = max; // force-fail the while condition because
			continue; // jumps to the while condition
		}
		if (passable && choice == 0)
			return max;
		choice -= 1;
	} while (choice >= max);

	return choice;
}

vector<uint16_t> BasicCoordinator::get_multi(size_t bigmax, bool passable) {
	assertLessThanEqual(bigmax, UINT16_MAX);
	uint16_t max = static_cast<uint16_t>(bigmax);
	std::string input;
	getline(cin, input);
	uint16_t choice;
	std::stringstream ss(input);
	vector<uint16_t> choices;
	while (ss >> choice) {
		choice -= 1;
		if (choice < max)
			choices.push_back(choice);
	}
	if (choices.empty() && !passable) {
		// no numbers, but we need at least one since we're not marked passable
		return get_multi(bigmax, passable);
	}
	return choices;
}

Card* BasicCoordinator::pass_priority(const query& q) {
	if (q.passable)
		cout << "0. Pass" << endl;
	uint16_t i = 1;
	for (auto& pc : q.additional_cards) {
		Player *p = pc.first;
		Card *cc = pc.second;
		cout << i << ". " << p->name << "'s " << cc->ci.name << endl;
		i++;
	}
	uint16_t max = i - 1;
	uint16_t choice = geti(max, q.passable);
	if (q.passable && choice == max)
		return nullptr;
	else
		return std::next(q.additional_cards.begin(), choice)->second;
}

Card* BasicCoordinator::select_card(const query& q) {
	cout << q.p->name << ", please select a " <<
		(q.type == query::COUNTER ? "counter:" : "card:") << endl;
	if (q.passable)
		cout << "0. Pass" << endl;
	uint16_t i = 1;
	for (auto& c : q.cards) {
		cout << i << ". " << c->ci.name << endl;
		i++;
	}
	for (auto& pc : q.additional_cards) {
		Player *p = pc.first;
		Card *cc = pc.second;
		cout << i << ". " << p->name << "'s " << cc->ci.name << endl;
		i++;
	}
	uint16_t max = i - 1;
	uint16_t choice = geti(max, q.passable);
	if (q.passable && choice == max)
		return nullptr;
	if (choice < q.cards.size())
		return q.cards[choice];
	else
		return std::next(q.additional_cards.begin(), choice - q.cards.size())->second;
}

cardpack BasicCoordinator::select_multi_cards(const query& q) {
	cout << q.p->name << ", please select one or more of the following:" << endl;
	int16_t i = 1;
	for (auto& c : q.cards) {
		cout << i << ". " << c->ci.name << endl;
		i++;
	}
	vector<uint16_t> choices = get_multi(q.cards.size(), q.passable);
	cardpack ccards;
	for (auto choice : choices) {
		ccards.push_back(q.cards[choice]);
	}
	return ccards;
}

void BasicCoordinator::wait(uint64_t id) {
	query q = pop_query(id);
	switch (q.type) {
	case query::CARD:
	case query::COUNTER:
		q.fc->val = make_shared<Card*>(select_card(q));
		q.fc->ready = true;
		break;
	case query::MULTI_CARDS:
		q.fcc->val = make_shared<cardpack>(select_multi_cards(q));
		q.fcc->ready = true;
		break;
	case query::PLAYER:
		q.fp->val = make_shared<Player*>(select_player(q));
		q.fp->ready = true;
		break;
	case query::TIMER:
		// wait five seconds
		for (const auto& p : q.players) {
			cout << p->name << ":" << endl;
		}
		cout << "5..4..3..2..1..0" << endl;
		q.fc->val = make_shared<Card*>(pass_priority(q));
		q.fc->ready = true;
		break;
	case query::ATCKTYPE:
		q.fa->val = make_shared<ATYPE>(select_atype(q));
		q.fa->ready = true;
		break;
	}
}

Player *BasicCoordinator::select_player(const query& q) {
	cout << q.p->name << ", please select a player:" << endl;
	if (q.passable)
		cout << "0. Pass" << endl;
	int16_t i = 1;
	for (auto& p : q.players) {
		cout << i << ". " << p->name << endl;
		i++;
	}
	uint16_t choice = geti(q.players.size(), q.passable);
	if (q.passable && choice == q.cards.size())
		return nullptr;

	auto iter = q.players.begin();
	std::advance(iter, choice);
	return *iter;
}

ATYPE BasicCoordinator::select_atype(const query& q) {
	cout << q.p->name << ", select an annoyance:" << endl;
	if (q.passable)
		cout << "0. Pass" << endl;
	cout << "1. Physical" << endl;
	cout << "2. Verbal" << endl;
	cout << "3. Mental" << endl;
	uint16_t choice = geti(3, q.passable);

	switch (choice) {
	case 0:
		return ATYPE::PHYSICAL;
	case 1:
		return ATYPE::VERBAL;
	case 2:
		return ATYPE::MENTAL;
	}

	assertTrue(q.passable && choice == 3);
	return ATYPE::NONE;
}

void BasicCoordinator::player_hit(const Player *p, int16_t hit) {
	cout << p->name << " was damaged: " << hit << endl;
}

void BasicCoordinator::player_healed(const Player *p, int16_t hit) {
	cout << p->name << " was healed: " << hit << endl;
}

void BasicCoordinator::player_dead(const Player *p) {
	cout << p->name << " is now dead." << endl;
}

void BasicCoordinator::game_end() {
	cout << "Game has ended!" << endl;
}

#ifndef _CARD_FLAGS_H
#define _CARD_FLAGS_H

#include "flagset.h"

// Affiliations
enum class AFF : uint8_t {
	S, M, A, C, I, AJ,
	MAX_VAL
};
FLAGSET(AFF, aff_t);

// Card types
enum class CTYPE : uint8_t {
	ATCK, // Attack card
	CNTR, // Counter card
	ITEM, // Item card
	FFLD, // Field card
	HEAL, // Heal card
	SPCL, // Special card
	ALLY, // Ally card
	ENMY, // Enemy card
	PRNK, // Prank card
	MAX_VAL
};
FLAGSET(CTYPE, ctype_t);

// Attack types
enum class ATYPE : uint8_t {
	NONE,
	GENERAL,
	PHYSICAL,
	VERBAL,
	MENTAL,
	MAX_VAL
};
FLAGSET(ATYPE, atype_t);

enum class Cardloc : uint8_t {
	HAND,
	ITEM,
	FIELD,
	PRANK,
	OTHER,
};

#endif // #ifndef _CARD_FLAGS_H

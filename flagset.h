#ifndef _FLAGSET_H
#define _FLAGSET_H

#include <cinttypes>
#include <bitset>
#include <type_traits>

template<typename E>
class flagset
{
	typedef typename std::underlying_type<E>::type B;
public:
	flagset() { }
	flagset(const E e) { set(e); }

	void set(const E e) { bits.set(val(e)); }

	bool operator&(const E rhs) const {
		return bits.test(val(rhs));
	}
	flagset<E> operator&(const flagset<E>& rhs) const {
		flagset<E> fs (this->bits);
		fs.bits &= rhs.bits;
		return fs;
	}

	flagset<E> operator|(const E rhs) const {
		flagset<E> fs (this->bits);
		fs.bits.set(val(rhs));
		return fs;
	}
	flagset<E> operator|(const flagset<E>& rhs) const {
		flagset<E> fs (this->bits);
		fs.bits |= rhs.bits;
		return fs;
	}

	operator bool() const {
		return bits.any();
	}

	flagset<E> operator~() const {
		flagset<E> fs (this->bits);
		fs.bits.flip();
		return fs;
	}
private:
	flagset(const std::bitset<static_cast<B>(E::MAX_VAL)>& bits) : bits(bits) { }
	B val(const E e) const { return static_cast<B>(e); }

	std::bitset<static_cast<B>(E::MAX_VAL)> bits;
};

#define FLAGSET(ENUM, FLAGSETNAME) \
	flagset<ENUM> inline operator|(const ENUM lhs, const ENUM rhs) { \
		flagset<ENUM> flags = lhs; \
		flags.set(rhs); \
		return flags; \
	} \
	typedef flagset<ENUM> FLAGSETNAME

void test_flagset();

#endif // #ifndef _FLAGSET_H

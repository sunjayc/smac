#include "asserts.h"
#include "network.h"

const uint32_t len = UINT32_MAX / sizeof(uint32_t) / 4;

void test_server() {
	ServerSocket* server = new OSServerSocket("9949");
	assertFalse(server->invalid());

	Socket* c = server->acceptClient();
	assertTrue(c);

	uint32_t* buf = new uint32_t[len];
	for (uint32_t i = 0; i < len; i++) {
		buf[i] = i;
	}
	bool written = c->write(buf, len * sizeof(uint32_t));
	assertTrue(written);

	delete [] buf;
	delete c;
}

void test_client() {
	Socket* client = new OSSocket();
	assertTrue(client->connectServer("127.0.0.1", 9949));

	uint32_t* buf = new uint32_t[len];
	bool read = client->read(buf, len * sizeof(uint32_t));
	assertTrue(read);

	for (uint32_t i = 0; i < len; i++) {
		assertEqual(buf[i], i);
	}

	delete [] buf;
}

#include <netdb.h>
#include <string.h>
#include <sys/socket.h>
#include <sys/types.h>
#include <unistd.h>

#include "network.h"

using std::string;

BerkeleyServerSocket::~BerkeleyServerSocket() {
	if (!invalid()) {
		close(_sockfd);
	}
}

BerkeleyServerSocket::BerkeleyServerSocket(string port) {
	struct addrinfo hints {0};
	struct addrinfo *results, *rp;
	int res, optval;

	hints.ai_family = AF_UNSPEC;
	hints.ai_socktype = SOCK_STREAM;
	hints.ai_flags = AI_PASSIVE;
	hints.ai_protocol = IPPROTO_TCP;

	res = getaddrinfo(NULL, port.c_str(), &hints, &results);
	if (res != 0) {
		return;
	}

	for (rp = results; rp != NULL; rp = rp->ai_next) {
		_sockfd = socket(rp->ai_family, rp->ai_socktype, rp->ai_protocol);
		if (_sockfd == -1)
			continue;

		optval = 1;
		setsockopt(_sockfd, SOL_SOCKET, SO_REUSEADDR, &optval, sizeof(optval));

		res = bind(_sockfd, rp->ai_addr, rp->ai_addrlen);
		if (res == 0) // bind was ok, break out of for loop
			break;

		close(_sockfd);
	}

	freeaddrinfo(results);

	res = listen(_sockfd, SOMAXCONN);
	if (res != 0) {
		close(_sockfd);
		return;
	}

	_invalid = false;
}

Socket* BerkeleyServerSocket::acceptClient() {
	if (invalid())
		return nullptr;
	struct sockaddr_storage caddr;
	socklen_t caddr_len = sizeof(caddr);
	Socket *client_sock = nullptr;
	do {
		int clientfd = accept(_sockfd, (struct sockaddr *)&caddr, &caddr_len);
		if (clientfd < 0) {
			if (errno == EAGAIN || errno == EINTR)
				continue;
			break;
		}
		client_sock = new BerkeleySocket(clientfd);
	} while (!client_sock);

	return client_sock;
}


BerkeleySocket::~BerkeleySocket() {
	if (!invalid()) {
		close(_sockfd);
	}
}

bool BerkeleySocket::read(void* buf, uint32_t n) {
	int res;
	uint32_t readden = 0;
	while (readden < n) {
		res = ::read(_sockfd, (char*)buf + readden, n - readden);
		if (res == -1) {
			if (errno == EINTR)
				continue;
			close(_sockfd);
			_connected = false;
			return false;
		}
		readden += res;
	}
	return true;
}

bool BerkeleySocket::write(const void* buf, uint32_t n) {
	int res;
	uint32_t written = 0;
	while (written < n) {
		res = ::write(_sockfd, (char*)buf + written, n - written);
		if (res == -1) {
			if (errno == EINTR)
				continue;
			close(_sockfd);
			_connected = false;
			return false;
		}
		written += res;
	}
	return true;
}

bool BerkeleySocket::connectServer(string hostname, unsigned short port) {
	int res;
	struct addrinfo *result = nullptr, *ptr = nullptr, hints {0};
	hints.ai_family = AF_UNSPEC;
	hints.ai_socktype = SOCK_STREAM;
	hints.ai_protocol = IPPROTO_TCP;

	char portstr[10];
	snprintf(portstr, 10, "%u", port);
	res = getaddrinfo(hostname.c_str(), portstr, &hints, &result);
	if (res != 0)
		return false;

	_sockfd = -1;

	ptr = result;
	do {
		_sockfd = socket(ptr->ai_family, ptr->ai_socktype, ptr->ai_protocol);
		if (_sockfd == -1) {
			freeaddrinfo(result);
			return false;
		}

		res = connect(_sockfd, ptr->ai_addr, ptr->ai_addrlen);
		if (res == -1) {
			close(_sockfd);
			ptr = result->ai_next;
		}
	} while (res == -1);
	freeaddrinfo(result);

	_invalid = false;
	_connected = true;
	return true;
}

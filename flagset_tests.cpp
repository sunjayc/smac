#include "asserts.h"
#include "flagset.h"

enum class TEST {
	A, B, C, D,
	MAX_VAL
};
FLAGSET(TEST, test_t);

void test_flagset() {
	test_t a;
	assertFalse(a & TEST::A);
	assertFalse(a & TEST::B);
	assertFalse(a & TEST::C);
	assertFalse(a & TEST::D);

	test_t b = TEST::B;
	assertFalse(b & TEST::A);
	assertTrue(b & TEST::B);
	assertFalse(b & TEST::C);
	assertFalse(b & TEST::D);

	test_t c = TEST::B | TEST::C;
	assertFalse(c & TEST::A);
	assertTrue(c & TEST::B);
	assertTrue(c & TEST::C);
	assertFalse(c & TEST::D);

	test_t d = TEST::A | TEST::B | TEST::D;
	assertTrue(d & TEST::A);
	assertTrue(d & TEST::B);
	assertFalse(d & TEST::C);
	assertTrue(d & TEST::D);

	test_t e = TEST::C;

	assertTrue(d & c);
	assertFalse(e & d);

	test_t f = ~a;
	assertTrue(f & TEST::A);
	assertTrue(f & TEST::B);
	assertTrue(f & TEST::C);
	assertTrue(f & TEST::D);

	f = ~b;
	assertTrue(f & TEST::A);
	assertFalse(f & TEST::B);
	assertTrue(f & TEST::C);
	assertTrue(f & TEST::D);
}

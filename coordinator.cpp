#include "coordinator.h"

#include <memory>

using std::list;
using std::make_shared;

Coordinator::future<Card*> Coordinator::select_card(const Player *p, const cardpack& cards, bool passable) {
	uint32_t id = next_id();
	// Make an entry id -> select_card
	// Make a corresponding future and return it
	// Need to save p and cards for later use
	query q;
	q.type = query::CARD;
	q.p = p;
	q.cards = cards;
	q.fc = make_shared<future_state<Card*>>();
	q.fc->id = id;
	q.passable = passable;
	_queries[id] = q;
	select_card(q);
	return future<Card*>(this, q.fc);
}

Coordinator::future<cardpack> Coordinator::select_multi_cards(const Player *p, const cardpack& ccards) {
	uint32_t id = next_id();
	// Make an entry id -> select_card
	// Make a corresponding future and return it
	// Need to save p and cards for later use
	query q;
	q.type = query::MULTI_CARDS;
	q.p = p;
	q.cards = ccards;
	q.fcc = make_shared<future_state<cardpack>>();
	q.fcc->id = id;
	q.passable = true;
	_queries[id] = q;
	select_multi_cards(q);
	return future<cardpack>(this, q.fcc);
}

Coordinator::future<Card*> Coordinator::select_counter(const Player *p, const cardpack& cards) {
	uint32_t id = next_id();
	// Make an entry id -> select_counter
	// Make a corresponding future and return it
	// Need to save p and cards for later use
	query q;
	q.type = query::COUNTER;
	q.p = p;
	q.cards = cards;
	q.fc = make_shared<future_state<Card*>>();
	q.fc->id = id;
	q.passable = true;
	_queries[id] = q;
	select_card(q);
	return future<Card*>(this, q.fc);
}

Coordinator::future<Player*> Coordinator::select_player(const Player *p, const list<Player*>& players) {
	uint32_t id = next_id();
	// Make an entry id -> select_player
	// Make a corresponding future and return it
	// Need to save p and players for later use
	query q;
	q.type = query::PLAYER;
	q.p = p;
	q.players = players;
	q.fp = make_shared<future_state<Player*>>();
	q.fp->id = id;
	q.passable = false;
	_queries[id] = q;
	select_player(q);
	return future<Player*>(this, q.fp);
}

Coordinator::future<ATYPE> Coordinator::select_atype(const Player *p) {
	uint32_t id = next_id();
	query q;
	q.type = query::ATCKTYPE;
	q.p = p;
	q.fa = make_shared<future_state<ATYPE>>();
	q.fa->id = id;
	q.passable = false;
	_queries[id] = q;
	select_atype(q);
	return future<ATYPE>(this, q.fa);
}

Coordinator::future<Card*> Coordinator::five_second_timer(const std::list<Player*>& players, const playercards& additional_cards) {
	uint32_t id = next_id();
	query q;
	q.type = query::TIMER;
	q.players = players;
	q.additional_cards = additional_cards;
	q.fc = make_shared<future_state<Card*>>();
	q.fc->id = id;
	q.passable = true;
	_queries[id] = q;
	timer(q);
	return future<Card*>(this, q.fc);
}
